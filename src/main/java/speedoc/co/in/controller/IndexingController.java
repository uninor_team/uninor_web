package speedoc.co.in.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.IndexingService;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.IndexingVo;
import speedoc.co.in.vo.ScrutinyCaptureVo;
import speedoc.co.in.vo.ScrutinyRecordJSONResponseVo;

@Controller
@RequestMapping("/indexing")
public class IndexingController {

	private static Logger log = Logger.getLogger(IndexingController.class.getName());
	
	@Autowired
	private IndexingService indexingService;
	
	@RequestMapping(value = "/saveindexdata", method = RequestMethod.POST)
	public @ResponseBody String  saveIndexData(ModelMap modelMap , @RequestBody String postData) {
		try{
			ObjectMapper mapper = new ObjectMapper();
			IndexingVo indexingVo = mapper.readValue(postData.trim(), IndexingVo.class);
			log.info(" input from c#  saveindexdata :: " + indexingVo.toString());
			Integer resultCode=null;
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
			Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
			if(indexingVo != null){
				
				String dobText = StringUtils.trimToNull(indexingVo.getDobText());
				if(dobText != null){
					java.util.Date dates = format.parse(dobText);
					indexingVo.setDob(new Timestamp(dates.getTime()));
				}
				String poiIssueDateText = StringUtils.trimToNull(indexingVo.getPoiIssueDateText());
				log.info("indexingController saveIndexData :: OLD poiIssueDateText :: "+poiIssueDateText);
				
				// added by Kisshor :: 25-08-2015
				String newPoiIssueDateText = null;
				if(poiIssueDateText != null){
					String newPoiIssudeDt[] = poiIssueDateText.split("/");
					log.info("inside if :: indexingController :: saveIndexData :: poiIssueDateText != null ");
					
					// if condition added :: by Kisshor :: 25-08-2015
					
					if(newPoiIssudeDt[2].substring(0,2).contains("00")){
						log.info("inside if :: indexingController :: saveIndexData :: if year = "+newPoiIssudeDt[2]);
						newPoiIssueDateText = 	newPoiIssudeDt[0]+"/"+newPoiIssudeDt[1]+"/"+
												newPoiIssudeDt[2].replace(newPoiIssudeDt[2],Integer.toString(localCalendar.get(Calendar.YEAR)));
						
						java.util.Date dates = format.parse(newPoiIssueDateText);
						Timestamp poiIssueDate = new Timestamp(dates.getTime());
						log.info("saveIndexData :: indexingVo.getPoiIssueDateText() :: "+indexingVo.getPoiIssueDateText());
						indexingVo.setPoiIssueDate(poiIssueDate);
					}else{		
						log.info("inside else :: indexingController :: saveIndexData :: e.g. if poiIssue year !=0012");
						java.util.Date dates = format.parse(poiIssueDateText);
						Timestamp poiIssueDate = new Timestamp(dates.getTime());
						log.info("saveIndexData :: indexingVo.getPoiIssueDateText() :: "+indexingVo.getPoiIssueDateText());
						indexingVo.setPoiIssueDate(poiIssueDate);
					}
				}
				String poaIssueDateText = StringUtils.trimToNull(indexingVo.getPoaIssueDateText());
				log.info("indexingController saveIndexData :: poaIssueDateText :: "+poaIssueDateText);
				
				String newPoaIssueDateText = null;
				
				if(poaIssueDateText != null){
					log.info("inside if :: indexingController :: saveIndexData :: poaIssueDateText != null");
					String newPoaIssudeDt[] = poaIssueDateText.split("/");

					// if condition added :: by Kisshor :: 25-08-2015
					if(newPoaIssudeDt[2].substring(0,2).contains("00")){
						log.info("inside if :: indexingController :: saveIndexData :: if year = "+newPoaIssudeDt[2]);
						
						newPoaIssueDateText = 	newPoaIssudeDt[0]+"/"+newPoaIssudeDt[1]+"/"+
												newPoaIssudeDt[2].replace(newPoaIssudeDt[2],Integer.toString(localCalendar.get(Calendar.YEAR)));
						
						java.util.Date dates = format.parse(newPoaIssueDateText);
						indexingVo.setPoaIssueDate(new Timestamp(dates.getTime()));
						log.info("saveIndexData :: indexingVo.getPoiIssueDateText() :: "+indexingVo.getPoaIssueDateText());
					}else{
						log.info("inside else :: indexingController :: saveIndexData :: e.g. if poaIssue year !=0012");
						java.util.Date dates = format.parse(poaIssueDateText);
						indexingVo.setPoaIssueDate(new Timestamp(dates.getTime()));
						log.info("saveIndexData :: indexingVo.getPoiIssueDateText() :: "+indexingVo.getPoaIssueDateText());
					}
				}
				String visaText = StringUtils.trimToNull(indexingVo.getVisaValidTillText());
				if(visaText != null){
					java.util.Date dates = format.parse(visaText);
					indexingVo.setVisaValidTill(new Timestamp(dates.getTime()));
				}
				resultCode = indexingService.saveIndexData(indexingVo);
				log.info("response sending :: resultCode :: " + resultCode);
			}
			return resultCode+"";
		}catch (Exception e) {
			log.error(e);
			return "2";
		}
	}
	
	@RequestMapping(value = "/setindexingcompleted", method = RequestMethod.POST)
	public @ResponseBody String  setIndexingCompleted(ModelMap modelMap , @RequestBody String postData) {
		log.info("inside setindexingcompleted();");
		try{
			ObjectMapper mapper = new ObjectMapper();
			ScrutinyCaptureVo scrutinyCaptureVO = mapper.readValue(postData, ScrutinyCaptureVo.class);
			log.info("scrutinyCaptureVO for set  index_completed status :: "+ scrutinyCaptureVO );
			String resultCode = indexingService.setIndexingCompleted(scrutinyCaptureVO);
			// 0 = success    1 = dup     2 = Error in server     3 = unable to connect
			return resultCode;
		}catch (Exception e) {
			log.error(e);
			return "failure";
		}
	}
	
	@RequestMapping(value = "/getscrutinyrecords", method = RequestMethod.GET)
	public @ResponseBody String  getScrutinyRecords(ModelMap modelMap) {
		log.info("inside getscrutinyrecords.");
		List<ScrutinyCaptureVo> list=indexingService.getScrutinyCompletedRecords();
		log.info(" scrutinylist size :: " + list.size());
		ScrutinyRecordJSONResponseVo scrutinyRecordJSONResponseVO = new ScrutinyRecordJSONResponseVo();	
		if(list.size() > 0){
			ArrayList<String> summaryIdUsername = indexingService.getUserToAllot();
			scrutinyRecordJSONResponseVO.setScrutinyRecords(list);
			if(summaryIdUsername != null){
				scrutinyRecordJSONResponseVO.setSummaryId(summaryIdUsername.get(0));
				scrutinyRecordJSONResponseVO.setUsername(summaryIdUsername.get(1));
			}else{
				log.info(" summaryIdUsername == null ");
			}
		}
	    ObjectMapper mapper = new ObjectMapper();
	    String writeValueAsString = null;
		try {
			writeValueAsString = mapper.writeValueAsString(scrutinyRecordJSONResponseVO);
			log.info("sending scrutiny records json :: " + writeValueAsString);
		}catch (Exception e) {
			log.error("Error in getScrutinyRecords :: "+e.getMessage(),e);
		}
		return writeValueAsString;
	}
	
	// voter id chitti request
	@RequestMapping(value="/getelectrodata", method=RequestMethod.GET)
	public @ResponseBody String getElectroData(ModelMap modelMap){
		IndexingVo indexingVo=null;
		try {
		 indexingVo = indexingService.getElectroData();		
		} catch (Exception e) {
			log.error("Error in getElectroData :: IndexingController :: "+e.getMessage(),e);
		}
		return indexingVo.getPoiref();
	}
	
	@RequestMapping(value="/saveelectrodata", method=RequestMethod.POST)
	public @ResponseBody String saveElectroData(ModelMap modelMap, @RequestBody IndexingDataVO indexingDataVO){
		try {
			log.info("controller indexingDataVO :: "+indexingDataVO);
			Integer result = indexingService.saveElectroData(indexingDataVO);
		
			return result+"";
		} catch (Exception e) {
			log.error("Eror in saveElectroData :: "+e.getMessage(),e);
			e.printStackTrace();
			return "2";
		}
	}
}
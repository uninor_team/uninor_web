package speedoc.co.in.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IStatusService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.vo.JasperReportVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/misController")
public class MisController {
	
	@Autowired
	IStatusService statusService;
	
	@Autowired
	private IUserService userService;

	@Autowired
	private IFlcService flcService;
	
	@RequestMapping(value="/getreportpage", method=RequestMethod.GET)
	public String getReportPage(ModelMap modelMap){
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
//		flcService.updateCafTempPool(userVo) ;
		modelMap.addAttribute("jasperReportVo", new JasperReportVo());
		return "mis/report";
	}	
}

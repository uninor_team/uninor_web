package speedoc.co.in.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.JqgridResponse;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/userController")
public class UserController {
	
	private static Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IFlcService flcService;
	
	@RequestMapping(value="/getuserform", method=RequestMethod.GET)
	public String getUserForm(ModelMap modelMap){
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info("user.getUsername() :: " + user.getUsername());
//		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
//		flcService.updateCafTempPool(userVo) ;
		modelMap.addAttribute("userVo",new UserVo());
		logger.info("Inside getting userForm...");
		return "user/userSaveForm";
	}
	
	@RequestMapping(value="/getusergrid", method=RequestMethod.GET)
	public String getUserGrid(ModelMap modelMap){
		return "user/userGrid";
	}
		
	@RequestMapping(value = "/saveuser", method = RequestMethod.POST)
	public String saveOrUpdate(@RequestParam String userType, UserVo userVo, ModelMap modelMap,HttpServletRequest request) {
		try {
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String userName = user.getUsername();	
			logger.info("username :: "+userName +" prevOption :: "+userType);
			logger.info(userType);
			userService.saveOrUpdate(userVo,userType);	
		} catch (Exception e) {
			logger.error("saveOrUpdate :: "+e.getMessage(),e);
		}	
		return "user/userGrid";
	}
	
	@RequestMapping(value = "/checkUser", method = RequestMethod.POST)
	public @ResponseBody String checkUser(ModelMap modelMap, String username){		
		if(userService.getExistingUserName(username)){
			return "success";
		}else{
			return "failure"; 
		}
	}
	
	@RequestMapping(value = "/getuserlist", produces = "application/json")
	@ResponseBody JqgridResponse<UserVo> getUserList(
			@RequestParam(value = "_search", required = false) Boolean search,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx,
			@RequestParam(value = "sord", required = false) String sord) {
		ServiceResponse<GridDataVo<List<UserVo>>> serviceResponse = null; 
		try {			
			FilterVo<UserVo> filterVo = new FilterVo<UserVo>() ;
			filterVo.setPageNumber(page);
			filterVo.setRowsPerPage(rows) ;
			serviceResponse = userService.getUserList(filterVo);		
		} catch (Exception e) {
			logger.error("Error in getuserlist of userController::" +e.getMessage());
		}		
		JqgridResponse<UserVo> jqgridResponse = JQGridJSONObject(serviceResponse, page, rows);
		return jqgridResponse;		
	}
	
	private JqgridResponse<UserVo> JQGridJSONObject(ServiceResponse<GridDataVo<List<UserVo>>> serviceResponse,Integer page, Integer rows) {
		JqgridResponse<UserVo> response = new JqgridResponse<UserVo>();
		response.setRows(serviceResponse.getResponse().getResult());
		if (serviceResponse.getResponse().getMaxPageNumber() != null) {
			response.setTotal(serviceResponse.getResponse().getMaxPageNumber().toString());
		} else {
			response.setTotal("1");
		}
		response.setPage(Integer.valueOf(page).toString());
		response.setRecords(serviceResponse.getResponse().getRecords());
		return response;
	}
	
	@RequestMapping(value = "/getedituserpage", method = RequestMethod.GET)
	public String getUserUpdatePage(@RequestParam Long id,ModelMap modelMap) {
		ServiceResponse<UserVo> serviceResponse = userService.findById(id) ;
		modelMap.addAttribute("userVo",serviceResponse.getResponse());
		return "user/editUserPage";
	}
}

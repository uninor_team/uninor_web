package speedoc.co.in.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.ICafImageService;
import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.vo.RotateVO;

@Controller
@RequestMapping("/imageController")
public class ImageController {
	private Logger log = Logger.getLogger(ImageController.class);
	
	@Autowired
	private ICafImageService cafImageService;	
	
	@Autowired
	private IFlcService flcService;
	
	//rotate image
	@RequestMapping(value = "rotateImage", method = RequestMethod.POST)    // added on 15-01-2015 
	public @ResponseBody
	ServiceResponse<RotateVO> getRotatePdf(@RequestBody RotateVO rotateVO) {
		ServiceResponse<RotateVO> response = new ServiceResponse<RotateVO>();
		try {
			String fileName1 = null;
			log.info("before rotation rotateVO = "+rotateVO.toString());
			String fileName = rotateVO.getFileName();
//			String fileName = "103500004.pdf";
			int pageNo = rotateVO.getPageNo();			
			fileName1 = cafImageService.getImageRotatePdf(fileName, pageNo);

			log.info("fileName after rotation = "+ fileName1 );
			if (fileName1 != null) {
				rotateVO.setFileName(fileName1);
				response.setResult(rotateVO);
				response.setSuccessful(true);
			}
		} catch (Exception e) {
			log.error("error while rotate image :: " + e.getMessage(),e);
		}
		return response;
	}
	
	@RequestMapping(value = "getcafimagenames", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> getCafImageNames(@RequestParam String cafFileLocation) {
		log.debug("In getcafimagenames");
		if (cafFileLocation != null) {
			log.info("Caf File Location = "+ cafFileLocation);
			return flcService.getCafImageNames(cafFileLocation);
		} else {
			ServiceResponse<String> response = new ServiceResponse<String>();
			response.setSuccessful(false);
			return response;
		}
	}
	
}

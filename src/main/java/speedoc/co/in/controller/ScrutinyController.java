package speedoc.co.in.controller;

import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IScrutinyService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/slc")
public class ScrutinyController {
	private Logger log = Logger.getLogger(ScrutinyController.class);

	@Autowired
	private IScrutinyService scrutinyService;

	@Autowired
	private IUserService userService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private IFlcService flcService;
	
	@RequestMapping(value = "/getslcpage", method = RequestMethod.GET)
	public String getScrutinyPage(ModelMap modelMap) {
//		 UserVo userVo=getUserLogin();
//		 flcService.updateCafTempPool(userVo) ;
		return "slc/scrutiny-page-new";
	}

	@RequestMapping(value = "getslcdata", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<CafScrutinyVo> getSlcData(ModelMap modelMap) {
		log.info("In getSlcData");
		ServiceResponse<CafScrutinyVo> response = null;
		//UserVo userVo=getUserLogin();
		response = scrutinyService.getDECompletedCafs(sessionService.getUserVo());
		return response;
	}

	@RequestMapping(value = "getcafimagenames", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> getCafImageNames(CafScrutinyVo cafScrutinyVo) {
		log.debug("In getcafimagenames");
		if (cafScrutinyVo != null) {
			log.info("Caf File Location = "+ cafScrutinyVo.getCafFileLocation());
			return flcService.getCafImageNames(cafScrutinyVo.getCafFileLocation());
		} else {
			ServiceResponse<String> response = new ServiceResponse<String>();
			response.setSuccessful(false);
			return response;
		}
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> save(CafScrutinyVo cafScrutinyVo, ModelMap modelMap) {
		log.info("cafScrutinyVo = " + cafScrutinyVo);
		ServiceResponse<String> response = null;
		UserVo userVo = getUserLogin();
		response = scrutinyService.save(cafScrutinyVo, userVo);
		return response;
	}

	@RequestMapping(value = "/renamereject", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> rejectSlc(@RequestParam String slcremak, CafScrutinyVo safScrutinyVo) {
		log.info("ScrutinyService renameReject :: safScrutinyVo= " + safScrutinyVo);
		log.info("slcremak=" + slcremak);
		UserVo userVo = getUserLogin();
		ServiceResponse<String> rs = scrutinyService.rejectSlc(slcremak,safScrutinyVo, userVo);
		return rs;
	}

	@RequestMapping(value = "getcaoimage", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<CaoVo> getCaoImage(@RequestParam String caoCode, ModelMap modelMap) {
		log.info("In getCaoImage caoCode = " + caoCode);
		ServiceResponse<CaoVo> response = null;
		response = scrutinyService.getCaoImage(caoCode);
		return response;
	}

	@RequestMapping(value = "getcaoimage", method = RequestMethod.GET)
	public void getCafFile(@RequestParam String fileName, @RequestParam String timestamp, OutputStream out) {
		scrutinyService.getCafFile(fileName, out);
	}

	private UserVo getUserLogin() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		log.info("user.getUsername() :: " + user.getUsername());
		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
		log.info("userVo :: " + userVo);
		return userVo;
	}
	
	@RequestMapping(value = "rejectreason", method=RequestMethod.POST)
	public @ResponseBody ServiceResponse<List<RemarkVo>> getRejectReason(ModelMap modelMap, @RequestParam String header,CafScrutinyVo cafScrutinyVo){
		log.info("getRejectReason header :: "+header);
		ServiceResponse<List<RemarkVo>> sr = scrutinyService.getRejectReasons(header);	
		modelMap.addAttribute("cafScrutiny", cafScrutinyVo);
		modelMap.addAttribute("remarkVoList", sr.getResult());
		return sr;
	}
	
	@RequestMapping(value = "getrejectreason", method=RequestMethod.POST)
	public String getRejReason(ModelMap modelMap, @RequestParam String header){
		ServiceResponse<List<RemarkVo>> sr = scrutinyService.getRejectReasons(header);	
		modelMap.addAttribute("remarkVoList", sr.getResult());
		return "slc/reject-reason-page";
	}
	
	@RequestMapping(value = "alreadyexistspeedoc", method = RequestMethod.POST)
	public @ResponseBody  boolean alreadyexistspeedoc(String speedocId, ModelMap modelMap) {
		log.info("speedocId = " + speedocId);
		boolean b = scrutinyService.findSpeedocIdExists(speedocId);
		if(b == true){
			return b;
		}else{
			return b;
		}
	}
	
	@RequestMapping(value = "/getslcsearchpage", method = RequestMethod.GET)
	public String getSearchPage(ModelMap modelMap) {
		return "slcsearch/slcSearchPage";
	}
	
	@RequestMapping(value = "/getslcsearchdata", method = RequestMethod.POST)
	@ResponseBody ServiceResponse<List<SearchResultVo>> getSearchData(ModelMap modelMap, @RequestParam String searchTxt, @RequestParam String searchOption, HttpServletResponse httpServletResponse) {
		log.info(" searchTxt = " + searchTxt+" searchOption = "+searchOption);
		ServiceResponse<List<SearchResultVo>> response = new ServiceResponse<List<SearchResultVo>>();
		List<SearchResultVo> searchResultVos=null;
		UserVo userVo =  getUserLogin();
		if(searchOption.equalsIgnoreCase("Mobile Number")){
			 searchResultVos = scrutinyService.getSlcSearchDataByMobileNumber(searchTxt, userVo);
			log.info("Mobile Number searchResultVos size :: " + searchResultVos.size());
		}else if(searchOption.equalsIgnoreCase("Speedoc Id")){
			searchResultVos=scrutinyService.getSlcSearchDataBySpeedocId(searchTxt,userVo);
			log.info("Speedoc Id searchResultVos size :: " + searchResultVos.size());
		}else if(searchOption.equalsIgnoreCase("Caf Number")){
			searchResultVos=scrutinyService.getSlcSearchDataByCafNumber(searchTxt,userVo);
			log.info("Speedoc Id searchResultVos size :: " + searchResultVos.size());
		}
		if (searchResultVos.size() > 0) {
			response.setResult(searchResultVos);
			response.setSuccessful(true);
		} else {
			response.setSuccessful(false);
			response.setMessage("No Record found.");
		}
		return response;
	}
	
	// slc search for edit page
	@RequestMapping(value="slcSearchEditPage",method=RequestMethod.GET )
	public String slcSearchEditPage(@RequestParam String cafId , ModelMap modelMap){
		CafScrutinyVo cafScrutinyVo = null;
		UserVo userVo=getUserLogin();
		cafScrutinyVo = scrutinyService.getSlcSearchCaf(userVo,cafId);
		if(cafScrutinyVo != null){
			scrutinyService.updateCafStatusLatestToOldSearch(false, cafScrutinyVo.getId(),userVo.getId());
		}
		modelMap.addAttribute("cafScrutinyVo", cafScrutinyVo);
		return "slcsearch/slcSearchEditPage";
	}
	
	
	// slc search for preview page
	@RequestMapping(value="slcSearchPreviewPage",method=RequestMethod.GET )
	public String slcSearchPreviewPage(@RequestParam String cafId,ModelMap modelMap){
		CafScrutinyVo cafScrutinyVo = null;
		UserVo userVo=getUserLogin();
		cafScrutinyVo = scrutinyService.getSlcSearchCaf(userVo,cafId);
		modelMap.addAttribute("cafScrutinyVo", cafScrutinyVo);
		return "slcsearch/slcSearchPreviewPage";
	}
	
	//slc cancel 
	@RequestMapping(value="/cancelCaf", method=RequestMethod.GET)
	public String cancelCaf(@RequestParam String cafId , ModelMap modelMap)
	{
		try{
			boolean bool = scrutinyService.cancelCaf(cafId ,null);
//			return "success";
		}
		catch (Exception e) {
			log.error( e.getMessage());
//			return "fail";
		}
		return "success";
	}
	
	// status page
	@RequestMapping(value = "getstatuspage", method = RequestMethod.GET)
	public String getStatusPage(ModelMap map,String cafId){
		log.info("getStatusPage:::cafId::"+cafId);
		
		map.addAttribute("cafId", cafId);
		return "slcsearch/cafstatus";
	}
	
	//show status 
	@RequestMapping(value = "getstatus", method = RequestMethod.POST)
	@ResponseBody ServiceResponse<List<StatusResultVo>> getstatus(String cafId){
		log.info(" cafId ::"+cafId);
		StatusResultVo statusResultVo = new StatusResultVo();
		statusResultVo.setCafTblId(Long.valueOf(cafId));
		
		ServiceResponse<List<StatusResultVo>> response = new ServiceResponse<List<StatusResultVo>>();
		
		List<StatusResultVo> statusResultVos =  scrutinyService.getStatusList(statusResultVo);
		
		if( statusResultVos.size() > 0 ){
			response.setResult(statusResultVos);
			response.setSuccessful(true);
		}else{
			response.setSuccessful(false);
			response.setMessage("No Status Available.");
		}
		log.info("End of cafStatusVOs.size()"+statusResultVos.size());
		return response;
	}
	
	// slc search save 
	@RequestMapping(value = "searchSave", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> searchSave(CafScrutinyVo cafScrutinyVo, ModelMap modelMap) {
		log.info("cafScrutinyVo = " + cafScrutinyVo);
		ServiceResponse<String> response = null;
		UserVo userVo = getUserLogin();
		response = scrutinyService.searchSave(cafScrutinyVo, userVo);
		return response;
	}
	// slc search reject 
	@RequestMapping(value = "/searchrenamereject", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> rejectSearchSlc(@RequestParam String slcremak, CafScrutinyVo safScrutinyVo) {
		log.info("ScrutinyService renameReject :: safScrutinyVo= " + safScrutinyVo);
		log.info("slcremak=" + slcremak);
		UserVo userVo = getUserLogin();
		ServiceResponse<String> rs = scrutinyService.rejectSearchSlc(slcremak,safScrutinyVo, userVo);
		return rs;
	}
	
	
}

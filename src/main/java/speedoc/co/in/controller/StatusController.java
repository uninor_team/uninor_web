package speedoc.co.in.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import speedoc.co.in.service.IStatusService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.vo.StatusViewerVo;

@Controller
@RequestMapping("/statusController")
public class StatusController {

	@Autowired
	IStatusService statusService;

	private static Logger logger = Logger.getLogger(StatusController.class);
	
	@RequestMapping(value = "/getstatuspage", method = RequestMethod.GET)
	public String adminStatus(ModelMap modelMap) {		
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 0);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String date = sdf.format(cal.getTime());
			Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(date);
			Timestamp dateNow=new Timestamp(date2.getTime());			
			logger.info("dateNow :: "+dateNow+ " date :: "+date);	
			modelMap.addAttribute("fromDate", dateNow);
			modelMap.addAttribute("toDate", dateNow);			
		} catch (ParseException e) {
			logger.error("adminStatus :: "+e.getMessage(),e);
		}		
		return "status/adminStatusViewer";
	}
	
	@RequestMapping(value = "/searchStatusViewer", method = RequestMethod.POST)
	public String searchStatusViewer(@RequestParam String fromDate, @RequestParam String toDate, ModelMap modelMap){
		ServiceResponse<List<StatusViewerVo>> response=null;
		List<StatusViewerVo> list=null;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date dateStr = formatter.parse(fromDate);
					
			String endDate = toDate+" 23:59:00";
			SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			Date dateEnd = formatter1.parse(endDate);
			
			Timestamp timestamp = new Timestamp(dateStr.getTime());
			Timestamp timestamp2 = new Timestamp(dateEnd.getTime());
						
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			logger.info("fromDate :: "+fromDate+" toDate :: "+toDate+" username :: "+username);
			response = statusService.getCountbyAdmin(fromDate, toDate, username);
			
			list = response.getResult();
			logger.info("list :: "+list.size());
					
			modelMap.addAttribute("fromDate", timestamp);
			modelMap.addAttribute("toDate", timestamp2);	
			modelMap.addAttribute("lists", list);
		}catch (Exception e) {
			logger.error("searchStatusViewer :: "+e.getMessage(),e);
		}		
		return "status/result";
	}	
}

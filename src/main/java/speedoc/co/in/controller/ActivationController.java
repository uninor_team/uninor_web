package speedoc.co.in.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.service.IActivationOfficerService;
import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.JqgridResponse;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;

@Controller
@RequestMapping("/activationController")
public class ActivationController {

	private static Logger logger = Logger.getLogger(ActivationController.class);

	@Value("${ACTIVATION_OFFICER_FILE_PATH}")
	private String activationOfficerFilePath;

	@Value("${TEMP_DATA_PATH}")
	private String tempFilePath;

	@Autowired
	ICafStatusDao cafStatusDao;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IFlcService flcService;

	@Autowired
	IActivationOfficerService activationOfficerService;
	
	@RequestMapping(value = "/getactivationform", method = RequestMethod.GET)
	public String getActivationPage(ModelMap modelMap) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info("user.getUsername() :: " + user.getUsername());
//		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
//		flcService.updateCafTempPool(userVo) ;
		
		modelMap.addAttribute("signatureFile", new CaoVo());
		
		return "activation/activationOfficerFile";
	}

	@RequestMapping(value = "/getactivationgrid", method = RequestMethod.GET)
	public String getGridPage(ModelMap modelMap) {
		return "activation/activationOfficerGrid";
	}

	@RequestMapping(value = "/saveform", method = RequestMethod.POST)
	public String saveActivationForm(MultipartHttpServletRequest signatureFile,
			@RequestParam String caoName, @RequestParam String caoCode,@RequestParam Long id,
			ModelMap modelMap, HttpServletRequest request) {
		Iterator<String> itr = signatureFile.getFileNames();
		MultipartFile files = signatureFile.getFile(itr.next());
		logger.info("signatureFile ::  come in controller"+ signatureFile);
		if (null != files) {
			try {
				CaoVo activationOfficerVo = new CaoVo();
				String renameFile = caoCode + ".jpg";
				File signature = new File(activationOfficerFilePath+ File.separator + renameFile);
				if(id== null){
					Boolean isExistCaoCode = activationOfficerService.isCaoCodeAlreadyExist(caoCode);
					if (isExistCaoCode != true) {
						activationOfficerVo.setCaoName(caoName);
						activationOfficerVo.setCaoCode(caoCode);
						activationOfficerVo.setImageName(renameFile);
						files.transferTo(signature);
						logger.info(" signature .getName :: " + signature.getName());
						activationOfficerService.saveActivationOfficer(activationOfficerVo);
						modelMap.addAttribute("msg", "SUCCESS");
					}
				}else{
					activationOfficerVo.setCaoName(caoName);
					activationOfficerVo.setCaoCode(caoCode);
					activationOfficerVo.setImageName(renameFile);
					activationOfficerVo.setId(id);
					files.transferTo(signature);
					logger.info(" signature .getName :: " + signature.getName());
					activationOfficerService.saveActivationOfficer(activationOfficerVo);
					modelMap.addAttribute("msg", "SUCCESS");
				}				
			} catch (Exception e) {
				logger.error("saveActivationForm :: " + e.getMessage(), e);
			}
		}
		return "activation/activationOfficerGrid";
	}

	@RequestMapping(value = "/getactivationmonthwiselist", produces = "application/json")
	@ResponseBody JqgridResponse<CaoVo> getActivationList(
	@RequestParam(value = "_search", required = false) Boolean search,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx,
			@RequestParam(value = "sord", required = false) String sord) {
		ServiceResponse<GridDataVo<List<CaoVo>>> serviceResponse = null;
		try {
			FilterVo<CaoVo> filterVo = new FilterVo<CaoVo>();
			filterVo.setPageNumber(page);
			filterVo.setRowsPerPage(rows);
			serviceResponse = activationOfficerService.getActivationList(filterVo);
		} catch (Exception e) {
			logger.error("Error in getavammonthwiselist of AvamController::"+ e.getMessage());
		}
		JqgridResponse<CaoVo> jqgridResponse = JQGridJSONObject(serviceResponse, page, rows);
		return jqgridResponse;
	}

	private JqgridResponse<CaoVo> JQGridJSONObject(ServiceResponse<GridDataVo<List<CaoVo>>> serviceResponse,
			Integer page, Integer rows) {
		JqgridResponse<CaoVo> response = new JqgridResponse<CaoVo>();	
		response.setRows(serviceResponse.getResponse().getResult());
		if (serviceResponse.getResponse().getMaxPageNumber() != null) {
			response.setTotal(serviceResponse.getResponse().getMaxPageNumber().toString());
		} else {
			response.setTotal("1");
		}
		response.setPage(Integer.valueOf(page).toString());
		response.setRecords(serviceResponse.getResponse().getRecords());
		return response;
	}

	@RequestMapping(value = "/imageValidation", produces = "application/json")
	@ResponseBody public String imageValidation(MultipartHttpServletRequest signatureFile,@RequestParam String caoCode) {
		Iterator<String> itr = signatureFile.getFileNames();
		MultipartFile files = signatureFile.getFile(itr.next());
		String renameFile = caoCode + ".jpg";
		File tempSignPath = new File(tempFilePath + File.separator + renameFile);
		logger.info(" files.getOriginalFilename() :: "+ files.getOriginalFilename() + " caoCode =" + caoCode);
		BufferedImage bimg;
		String msg = null;
		try {
			Boolean isExistCaoCode = activationOfficerService.isCaoCodeAlreadyExist(caoCode);
			if (isExistCaoCode != true) {
				files.transferTo(tempSignPath);
				bimg = ImageIO.read(tempSignPath);
				double kilobytes = files.getSize() / 1024.0;
				String type = files.getContentType();
				if (bimg != null) {
					int width = bimg.getWidth();
					int height = bimg.getHeight();
					if (kilobytes < 2 || kilobytes > 50) {
						msg = "SIZE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (width != 420 && height != 120) {
						msg = "RESOLUTION_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (!type.equals("image/jpeg") && !type.equals("image/jpg")) {
						msg = "TYPE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else {
						msg = "SUCCESS";
						return msg;
					}
				} else {
					if (kilobytes < 2 && kilobytes > 50) {
						msg = "SIZE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (!type.equals("image/jpeg") && !type.equals("image/jpg")) {
						msg = "TYPE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					}
				}
			} else {
				msg = "DUPLICATE_CODE_ERROR";
				FileUtils.deleteQuietly(tempSignPath);
				return msg;
			}
		} catch (IOException e) {
			logger.error("imageValidation :: " + e.getMessage(), e);
		}
		return null;
	}

	@RequestMapping(value = "/geteditsignaturepage", method = RequestMethod.GET)
	public String getEditSignaturePage(@RequestParam Long id, ModelMap modelMap) {
		ServiceResponse<CaoVo> serviceResponse = activationOfficerService.getSignatureById(id);
		CaoVo caoVo = new CaoVo();
		caoVo.setImageName(activationOfficerFilePath);
		modelMap.addAttribute("caoVo", serviceResponse.getResponse());
		return "activation/editActivationPage";
	}
	
	@RequestMapping(value = "/changecaostatus", method = RequestMethod.GET)
	public @ResponseBody String changeCaoStatus(@RequestParam Long id,@RequestParam int status, ModelMap modelMap) {
		boolean updated = activationOfficerService.changeCaoStatus(id,status);
		
		if(updated == true)
			return "success";
		else
			return "fail";
	}
	
	@RequestMapping(value = "/imageFileValidation", produces = "application/json")
	@ResponseBody public String imageFileValidation(MultipartHttpServletRequest signatureFile,@RequestParam String caoCode) {
		Iterator<String> itr = signatureFile.getFileNames();
		MultipartFile files = signatureFile.getFile(itr.next());
		String renameFile = caoCode + ".jpg";
		File tempSignPath = new File(tempFilePath + File.separator + renameFile);
		logger.info(" files.getOriginalFilename() :: "+ files.getOriginalFilename() + " caoCode =" + caoCode);
		BufferedImage bimg;
		String msg = null;
		try {
			Boolean isExistCaoCode = activationOfficerService.isCaoCodeAlreadyExist(caoCode);
			if (isExistCaoCode == true) {
				files.transferTo(tempSignPath);
				bimg = ImageIO.read(tempSignPath);
				double kilobytes = files.getSize() / 1024.0;
				String type = files.getContentType();
				if (bimg != null) {
					int width = bimg.getWidth();
					int height = bimg.getHeight();
					if (kilobytes < 2 || kilobytes > 50) {
						msg = "SIZE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (width != 420 && height != 120) {
						msg = "RESOLUTION_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (!type.equals("image/jpeg") && !type.equals("image/jpg")) {
						msg = "TYPE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else {
						msg = "SUCCESS";
						return msg;
					}
				} else {
					if (kilobytes < 2 && kilobytes > 50) {
						msg = "SIZE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					} else if (!type.equals("image/jpeg") && !type.equals("image/jpg")) {
						msg = "TYPE_ERROR";
						FileUtils.deleteQuietly(tempSignPath);
						return msg;
					}
				}
			} 
		} catch (IOException e) {
			logger.error("imageFileValidation :: " + e.getMessage(), e);
		}
		return null;
	}
}

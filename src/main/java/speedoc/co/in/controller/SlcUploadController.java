package speedoc.co.in.controller;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.ISlcUploadService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.JqgridResponse;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcImportVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/slcupload")
public class SlcUploadController {
	
	private static Logger logger = Logger.getLogger(SlcUploadController.class);
	
	@Autowired
	private ISlcUploadService slcUploadService;
	
	@Autowired
	private IUserService userService;

	@Autowired
	private IFlcService flcService;
	
	@Value("${SLC_INPROCESS_FILE_PATH}")
	private String slcInprocessFilePath;
	
	@RequestMapping(value = "/getslcuploadpage", method = RequestMethod.GET)
	public String getSlcUploadPage(ModelMap modelMap) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
//		flcService.updateCafTempPool(userVo) ;
		return "slcupload/slc-file-upload-page";
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String save(MultipartHttpServletRequest uploadForm, ModelMap modelMap , HttpServletRequest request) {
		Iterator<String> itr =  uploadForm.getFileNames();
		MultipartFile files = uploadForm.getFile(itr.next());
		if (null != files) {
			try {
				String fileName = files.getOriginalFilename();
				Date date = new Date();
				String slcFilePath = slcInprocessFilePath + File.separator
						+ FilenameUtils.getBaseName(fileName) + "_"
						+ date.getHours() + "_" + date.getMinutes() + "_"
						+ date.getSeconds() + "."
						+ FilenameUtils.getExtension(fileName);
				files.transferTo(new File(slcFilePath));				
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		return "slcupload/slc-page";
	}
	
	@RequestMapping(value = "/getgridpage" ,method = RequestMethod.GET)
	public String getGridPage(ModelMap modelMap){	
		return "slcupload/slcUploadGrid";
	}
	
	@RequestMapping(value="/getslcpage", method=RequestMethod.GET)
	public String getAvamPage(ModelMap modelMap){		
		return "slcupload/slc-page";
	}
	
	@RequestMapping(value = "/getslclist", produces = "application/json")
	@ResponseBody JqgridResponse<SlcImportVo> getActivationList(@RequestParam(value = "_search", required = false) Boolean search,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx,
			@RequestParam(value = "sord", required = false) String sord) {
		ServiceResponse<GridDataVo<List<SlcImportVo>>> serviceResponse = null;
		try {
			FilterVo<SlcImportVo> filterVo = new FilterVo<SlcImportVo>();
			filterVo.setPageNumber(page);
			filterVo.setRowsPerPage(rows);
			serviceResponse = slcUploadService.getSlcList(filterVo);
		} catch (Exception e) {
			logger.error("Error in getavammonthwiselist of AvamController::"+ e.getMessage());
		}
		JqgridResponse<SlcImportVo> jqgridResponse = null;
		if(serviceResponse != null){
			 jqgridResponse = JQGridJSONObject1(serviceResponse, page, rows);
		}
		return jqgridResponse;
	}

	private JqgridResponse<SlcImportVo> JQGridJSONObject1(ServiceResponse<GridDataVo<List<SlcImportVo>>> serviceResponse, Integer page, Integer rows) {
		JqgridResponse<SlcImportVo> response = new JqgridResponse<SlcImportVo>();	
		response.setRows(serviceResponse.getResponse().getResult());
		if (serviceResponse.getResponse().getMaxPageNumber() != null) {
			response.setTotal(serviceResponse.getResponse().getMaxPageNumber().toString());
		} else {
			response.setTotal("1");
		}
		response.setPage(Integer.valueOf(page).toString());
		response.setRecords(serviceResponse.getResponse().getRecords());
		return response;
	}
}

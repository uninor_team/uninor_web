package speedoc.co.in.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import speedoc.co.in.vo.UserVo;

@Service
public class MailMail {
	
	@SuppressWarnings("unused")
	private JavaMailSender mailSender;
	@SuppressWarnings("unused")
	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendEmail(UserVo userVo, String url) throws Exception {

		 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("testmailvista@gmail.com", "testmail");
			}
		  });
 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("testmailvista@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(userVo.getEmailAddress()));
			message.setSubject("Uninor Scanning Application");
			message.setContent(msgBody(userVo, url),"text/html");
			Transport.send(message);
 
	
	}
	
	public String msgBody(UserVo userVo, String url){
		String text = "Dear " + userVo.getFirstName() + " " + userVo.getLastName()  + ", <br>" +  
					  userVo.getCity() + "<br><br>" +
					  
					  "Welcome to uninor-speedoc CAF management process.Please download speedoc scanning " +
					  "application from the link below.<br><br>" + 
		    
		              "<a href='" + url + "'>" + url + "</a><br><br>" +
		              
		              "Username :- " + userVo.getUsername() + " <br>" +
		              "Password :- " + userVo.getPwd() + " <br><br>" +
		              
		              "Regards<br>" + 
		              "Datamatic - Support";
		return text;
	}

}

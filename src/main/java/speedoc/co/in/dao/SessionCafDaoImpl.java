package speedoc.co.in.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import speedoc.co.in.vo.SessionCafVo;

public class SessionCafDaoImpl implements SessionCafDao{
	private static Logger log  = Logger.getLogger(SessionCafDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected JdbcTemplate jdbcTemplate; 
	
	@Autowired
	public void setDataSource(DataSource dataSource) 
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertSessionCaf(final SessionCafVo sessionCafVo) {
		System.out.println("Inside insertSessionCaf " + sessionCafVo.toString() );
		try{
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
					String sql = "INSERT INTO USER_SESSION_CAF_TBL (SESSION_ID, CAF_TBL_ID, MOBILE_NUMBER, USER_ID, STATUS_ID, CREATED_DATE) VALUES (?,?,?,?,?,?)";
					PreparedStatement pstmt = con.prepareStatement(sql);
					pstmt.setString(1, sessionCafVo.getSessionId());
					pstmt.setLong(2, sessionCafVo.getCafTblId());
					pstmt.setString(3, sessionCafVo.getMobileNumber());
					pstmt.setLong(4, sessionCafVo.getUserId());
					pstmt.setLong(5, sessionCafVo.getStatusId());
					pstmt.setTimestamp(6, sessionCafVo.getCreatedDate());

					System.out.println("The insert prepared statement ::"+pstmt);
					log.info("SessionCafDaoImpl :: insertSessionCaf :: Row inserted in USER_SESSION_CAF_TBL :: CAF_TBL_ID = "+ sessionCafVo.getCafTblId());
					
					getSessionCafVoByCafId(sessionCafVo.getCafTblId());
					
					return pstmt;
				}
			}, keyHolder);
		}catch(Exception e){
			log.error("SessionCafDaoImpl :: insertSessionCaf :: "+ e.getMessage(),e);
		}
	}

	@Override
	public boolean getSessionCafVoByCafId(Long cafTblId) {
		SessionCafVo sessionCafVo = null;
		 try {
				String sql = "select * from USER_SESSION_CAF_TBL where CAF_TBL_ID = "+cafTblId+"";
				sessionCafVo = jdbcTemplate.queryForObject(sql, SessionCafVo.class); 
				    if (sessionCafVo == null) {
				        return false;
				    } else {
				    	System.out.println(sessionCafVo.toString());
				        return true;
				    }
			} catch (EmptyResultDataAccessException e) {
				log.error("Exception Occured :: getSessionCafVoByCafId", e); 
				return false;
			}catch (DataAccessException e) {
				log.error("Exception Occured :: getSessionCafVoByCafId", e); 
				return false;
			}catch (Exception e) {
				log.error("Exception Occured :: getSessionCafVoByCafId", e); 
				return false;
			}

//		 try {
//			String sql = "select * from USER_SESSION_CAF_TBL where CAF_TBL_ID = "+cafTblId+"";
//			  List<SessionCafVo> certs = jdbcTemplate.queryForList(sql, SessionCafVo.class); 
//			    if (certs.isEmpty()) {
//			        return false;
//			    } else {
//			    	System.out.println(certs.get(0));
//			        return true;
//			    }
//		} catch (EmptyResultDataAccessException e) {
//			log.error("Exception Occured :: getSessionCafVoByCafId", e); 
//			return false;
//		}catch (DataAccessException e) {
//			log.error("Exception Occured :: getSessionCafVoByCafId", e); 
//			return false;
//		}catch (Exception e) {
//			log.error("Exception Occured :: getSessionCafVoByCafId", e); 
//			return false;
//		}
		
	}

	public void deleteSessionCaf(Long cafTblId) {
		try {
			String delete = "DELETE FROM USER_SESSION_CAF_TBL WHERE CAF_TBL_ID = " + cafTblId + "";
			jdbcTemplate.execute(delete);
			log.info("SessionCafDaoImpl :: deleteSessionCaf :: Session Row Deleted :: where CAF_TBL_ID = "+ cafTblId);
		} catch (Exception e) {
			log.error("SessionCafDaoImpl :: deleteSessionCaf :: "+ e.getMessage(),e);
		}
	}
	}
	

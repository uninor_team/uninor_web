
package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.RoleVo;


public interface IRolesDao 
{

	List<RoleVo> getAllRoles();
	
	String[] getRolesByUserId(int userId);

	String[] getRolesNameByUserId(int userId);

	RoleVo getRoleByRoleType(RoleVo rolesVO) throws Exception;
	
	public RoleVo getAllRolesNameByUserId(Long userId) ;


}

package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.vo.SearchResultVo;

@Repository
public class SearchDao implements ISearchDao {

	private static Logger logger = Logger.getLogger(SearchDao.class);

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchResultVo> getSearchDataByMobileNumber(String mobileNumber) {
		String sql = "select ct.speedoc_id, ct.mobile_number, ct.caf_number, "
				+ "ct.first_name, ct.last_name, st.status_name, "
				+ "to_char(cst.created_date, 'dd/Mon/yyyy hh:mm'), ut.username from caf_tbl as ct "
				+ "JOIN caf_status_tbl as cst on ct.id=cst.caf_tbl_id "
				+ "JOIN status_tbl as st on cst.status_id=st.status_id "
				+ "JOIN user_tbl as ut on ut.id=cst.created_by "
				+ "WHERE ct.mobile_number='" + mobileNumber + "' ";
		try {
			List<Object> list = sessionFactory.getCurrentSession()
					.createSQLQuery(sql).list();

			return convertToSearchResultVoList(list);
		} catch (Exception e) {
			logger.error("getSearchDataByMobileNumber :: " + e.getMessage(), e);
		}
		return null;
	}

	private List<SearchResultVo> convertToSearchResultVoList(List<Object> list)
			throws Exception {
		Iterator<Object> it = list.iterator();
		List<SearchResultVo> searchResultVos = new ArrayList<SearchResultVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			SearchResultVo searchResultVo = new SearchResultVo();
			searchResultVo.setSpeedocId((String) object[0]);
			searchResultVo.setMobileNumber((String) object[1]);
			searchResultVo.setCafNumber((String) object[2]);
			searchResultVo.setFirstName((String) object[3]);
			searchResultVo.setLastName((String) object[4]);
			searchResultVo.setStatusName((String) object[5]);
			searchResultVo.setCreatedDate((String) object[6]);
			searchResultVo.setUsername((String) object[7]);
			searchResultVos.add(searchResultVo);
		}
		return searchResultVos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchResultVo> getSearchDataBySpeedocId(String speedocId) {
		String sql = "select ct.speedoc_id, ct.mobile_number, ct.caf_number, "
				+ "ct.first_name, ct.last_name, st.status_name, "
				+ "to_char(cst.created_date, 'dd/Mon/yyyy hh:mm'), ut.username from caf_tbl as ct "
				+ "JOIN caf_status_tbl as cst on ct.id=cst.caf_tbl_id "
				+ "JOIN status_tbl as st on cst.status_id=st.status_id "
				+ "JOIN user_tbl as ut on ut.id=cst.created_by "
				+ "WHERE ct.speedoc_id='" + speedocId + "' ";
		try {
			List<Object> list = sessionFactory.getCurrentSession()
					.createSQLQuery(sql).list();
			return convertToSearchResultVoList(list);
		} catch (Exception e) {
			logger.error("getSearchDataBySpeedocId :: " + e.getMessage(), e);
		}
		return null;
	}
}

package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.SearchResultVo;

public interface ISearchDao {
	List<SearchResultVo> getSearchDataByMobileNumber(String mobileNumber);
	List<SearchResultVo> getSearchDataBySpeedocId(String speedocId);
}

package speedoc.co.in.dao;


import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FlcAcceptVo;
import speedoc.co.in.vo.PosCposVo;
import speedoc.co.in.vo.UserVo;

public interface ICafDao {

	boolean findSpeedocIdExists(String speedocId);
	CafVo saveCaf(CafVo cafVo) throws Exception;
	public int updateSpeedocIdInCafById(String speedocId,Long  id, UserVo userVo) throws Exception;
	public CafVo findCafById(Long id) throws Exception  ;
	void updateCafFromIndexing(CafVo cafVO);
	CafVo findSpeedocIdAlreadyExists(String speedocId);
	Boolean updateOnhold(Long id, Integer onhold);
	CafVo findOnhold(Long cafId);
	CafVo getOnholdByCafId(Long cafId);
	boolean findSpeedocIdAndStatusExists(String speedocId);
	PosCposVo getPosDetail(String speedocId);
	public FlcAcceptVo checkOnholdAndSpeedocIdExist(String speedocId, Long cafId, Long id);
//	public CafVo getDeCompletedCaf(Long cafTblId) throws Exception;
	public CafScrutinyVo getDeCompletedCaf(UserVo userVo) throws Exception; 
	
}

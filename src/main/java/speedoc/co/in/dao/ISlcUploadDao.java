package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDataVo;
import speedoc.co.in.vo.SlcImportVo;

public interface ISlcUploadDao {

	Long getSlcCount();

	List<SlcImportVo> getSlcList(FilterVo<SlcImportVo> filterVo);

	Boolean isSlcAlreadyDone(Long long1);

	CafStatusVo getAllStatusVo(String speedocId);

	SlcImportVo addSlcImportRecord(SlcImportVo slcImportVo);

	CafStatusVo getAllCafStatusData(String speedocId);

	Boolean updateDEStatus(Long id);

	Boolean updateSlcImportByImportId(Integer recordUpdated,
			Integer duplicateRecord, Long slcImportId);

	SlcDataVo addNewSlcDataNumber(SlcDataVo slcDataVo);

	Long getSlcDataCountBySpeedocId(String speedocId);

	Integer uploadSlcFile();

	void addSlcDataFile(String slcFilePath);

	Boolean updateSlcImortId(Long slcImportId);

}

/**
 * 
 */
package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.SqlTimestampConverter;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.domain.Role;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.RoleVo;

@Repository
@Transactional
public class RolesDao implements IRolesDao
{
	private Logger log = Logger.getLogger(RolesDao.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	

	public RoleVo saveRole(final RoleVo rolesVO) {

		Role role = new Role() ;
		try {
			
			java.sql.Timestamp defaultValue = null;
			SqlTimestampConverter converter = new SqlTimestampConverter(defaultValue);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register(converter,java.sql.Timestamp.class);
			
			BeanUtils.copyProperties(role, rolesVO);
			sessionFactory.getCurrentSession().save(role);
			BeanUtils.copyProperties(rolesVO,role);
		} catch (Exception e) {
			log.error("Error in saveRole of class RolesDaoImpl:: ",e);
		}
		return rolesVO;
	}

	 @SuppressWarnings("unchecked")
	public List<RoleVo> getAllRoles() {
		List<RoleVo> roleVos = new ArrayList<RoleVo>() ;
		try{
		List<Role> roleList = sessionFactory.getCurrentSession().createCriteria(Role.class).list() ;
		
		if(roleList != null){
			roleVos = convertToRoleVoList(roleList);
		}
		}catch(Exception e){
			log.error("Error in getAllRoles of class RolesDaoImpl:: ",e);
		}
		return roleVos;
	}

	 @SuppressWarnings("unchecked")
	public String[] getRolesByUserId(int userId) {
		
		String hql = "select r FROM UserRole ur,Role as r where ur.roleId = r.id" +
	    		" and ur.userId =?";
				
		    List<RoleVo> roleVos = new ArrayList<RoleVo>() ;
		    try{
		    	List<Role> roleList = sessionFactory.getCurrentSession().createQuery(hql).setInteger(0,userId).list();
		    	if(roleList != null){
					roleVos = convertToRoleVoList(roleList);
					String[] userSelectedRoles =  (String[]) roleVos.toArray(new String[0]);
			    	return userSelectedRoles;
				}
		    }catch(Exception e){
		    	log.error("Error in getRolesByUserId of class RolesDaoImpl:: ",e);
		    }
		    return null ;
	}

	 @SuppressWarnings("unchecked")
	public String[] getRolesNameByUserId(int userId) 
	{
		String hql = "select r FROM UserRole ur,Role as r where ur.roleId = r.id" +
	    		" and ur.userId =?";
	    List<RoleVo> roleVos = new ArrayList<RoleVo>() ;
	    try{
	    	List<Role> roleList = sessionFactory.getCurrentSession().createQuery(hql).setInteger(0,userId).list();
	    	if(roleList != null){
				roleVos = convertToRoleVoList(roleList);
				String[] userSelectedRoles =  (String[]) roleVos.toArray(new String[0]);
				System.out.println("INSIDE RolesDaoImpl userSelectedRoles:"+userSelectedRoles);
		    	return userSelectedRoles;
			}
	    }catch(Exception e){
	    	log.error("Error in getRolesByUserId of class RolesDaoImpl:: ",e);
	    }
	    return null ;
	}

	public RoleVo getAllRolesNameByUserId(Long userId) 
	{
		String hql = "select r FROM UserRole ur,Role as r where ur.roleId = r.id" +
	    		" and ur.userId =?";
	    RoleVo roleVo = new RoleVo();
	    try{
	    	Role role = (Role) sessionFactory.getCurrentSession().createQuery(hql).setLong(0,userId).uniqueResult();
	    	if(role != null){
	    		roleVo = convertToRoleVo(role);
		    	return roleVo;
			}
	    }catch(Exception e){
	    	log.error("Error in getRolesByUserId of class RolesDaoImpl:: ",e);
	    }
	    return null ;
	}
	
	@Override
	public RoleVo getRoleByRoleType(RoleVo rolesVO) throws Exception {
		
			Role role = (Role) sessionFactory.getCurrentSession()
			            .createCriteria(Role.class)
			            .add(Restrictions.eq("roleType", rolesVO.getRoleType())).uniqueResult();
			
			if(role !=  null){
				RoleVo roleVo = convertToRoleVo(role);
				return roleVo ;
			}
		
		return null ;
	}
	
	 public static RoleVo convertToRoleVo(Role role) throws Exception {
		 RoleVo roleVo = new RoleVo();
		 Utils.copyProperties(roleVo, role);
		 return roleVo;
	}
	
	 public static  List<RoleVo> convertToRoleVoList(List<Role> roleList) throws Exception {
			ArrayList<RoleVo> roleVos = new ArrayList<RoleVo>();
			for (Role role : roleList) {
				RoleVo roleVo = convertToRoleVo(role);
				roleVos.add(roleVo);
			}
			return roleVos;
		}

}

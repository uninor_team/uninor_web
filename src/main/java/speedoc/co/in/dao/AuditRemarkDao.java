package speedoc.co.in.dao;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.AuditRemark;
import speedoc.co.in.vo.AuditRemarkVo;

@Repository
public class AuditRemarkDao implements IAuditRemarkDao{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	private static Logger logger=Logger.getLogger(AuditRemarkDao.class);
	
	@Override
	public Long save(AuditRemarkVo auditRemarkVo) {
		try{
			AuditRemark auditRemark = new AuditRemark() ;
			BeanUtils.copyProperties(auditRemark, auditRemarkVo) ;
			Session session = sessionFactory.getCurrentSession();
			session.save(auditRemark) ;
//			session.flush() ;
			return auditRemark.getId() ;
		}catch (Exception e) {
			logger.error("Error in save :: "+e.getMessage(),e);
		}
		return null;
	}

}

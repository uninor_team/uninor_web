package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Remark;
import speedoc.co.in.vo.RemarkVo;

@Repository
public class RemarkDao implements IRemarkDao{
	
	private static Logger log = Logger.getLogger(RemarkDao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
  public Long save(String rejectReason) throws Exception{
		  Remark remark = new Remark();
		  remark.setRejectReason(rejectReason);
		  Session session = sessionFactory.getCurrentSession() ;
		  session.save(remark) ;
		  session.flush() ;
		  return remark.getId() ;
  }
  
  @SuppressWarnings("unchecked")
public List<RemarkVo> getFlcRejectReasons() {
	  try{
		  List<Remark> remarkList = sessionFactory.getCurrentSession().createCriteria(Remark.class)
				  .add(Restrictions.eq("rejectType", "FLC")).addOrder(Order.asc("id")).list() ;
		  if(remarkList != null && remarkList.size() > 0){
			  return convertToRemarkVoList(remarkList);
		  }
	  }catch(Exception e){
		  log.error("Error in getFlcRejectReasons method ::"+e.getMessage(),e);
		  return null ;
	  }
	  return null ;
  }
	
@SuppressWarnings("unchecked")
public List<RemarkVo> getSlcRejectReasons() {
	  try{
		  List<Remark> remarkList = sessionFactory.getCurrentSession().createCriteria(Remark.class)
				  .add(Restrictions.eq("rejectType","SLC")).addOrder(Order.asc("id")).list() ;
		  if(remarkList != null && remarkList.size() > 0){
			  return convertToRemarkVoList(remarkList);
		  }
	  }catch(Exception e){
		  log.error("Error in getSlcRejectReasons method ::"+e.getMessage(),e);
		  return null ;
	  }
	  return null ;
  }
  
  public List<RemarkVo> convertToRemarkVoList(List<Remark> remarkList) throws Exception{
	  List<RemarkVo> remarkVoList = new ArrayList<RemarkVo>();
	  for(Remark remark : remarkList){
		  RemarkVo remarkVo = new RemarkVo() ;
		  BeanUtils.copyProperties(remarkVo, remark);
		  remarkVoList.add(remarkVo);
	  }
	  return remarkVoList ;
  }

@SuppressWarnings("unchecked")
@Override
public List<RemarkVo> getRejectReasons(String header) {
	 try{
		 String sql= "select rt.id, rt.reject_reason as rejectReason,rt.reject_type as rejectType from remark_tbl rt " +
		 		"join header_remark_tbl hrt on hrt.remark_id = rt.id " +
		 		"join header_tbl ht on ht.id = header_id " +
		 		"where ht.header_name = '"+header+"'";
		
		 List<RemarkVo> remarkList = sessionFactory.getCurrentSession().createSQLQuery(sql)
		 .addScalar("id", StandardBasicTypes.LONG)
		 .addScalar("rejectReason", StandardBasicTypes.STRING)
		 .addScalar("rejectType", StandardBasicTypes.STRING)
		 .setResultTransformer(Transformers.aliasToBean(RemarkVo.class)).list();
		
		 if(remarkList != null && remarkList.size() > 0){
			  log.info("remarkList.size() :: "+remarkList.size());
			  return remarkList;
		  }
	  }catch(Exception e){
		  log.error("Error in getRejectReasons method ::"+e.getMessage(),e);
		  return null ;
	  }
	  return null ;
}

@SuppressWarnings("unchecked")
@Override
public List<RemarkVo> getAuditRejectReasons() {
	 try{
		  List<Remark> remarkList = sessionFactory.getCurrentSession().createCriteria(Remark.class)
				  .add(Restrictions.eq("rejectType","SLC")).addOrder(Order.asc("id")).list() ;
		  if(remarkList != null && remarkList.size() > 0){
			  return convertToRemarkVoList(remarkList);
		  }
	  }catch(Exception e){
		  log.error("Error in getAuditRejectReasons method ::"+e.getMessage(),e);
		  return null ;
	  }
	  return null ;
}
  
}














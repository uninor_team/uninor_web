package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.RemarkVo;

public interface IRemarkDao {
	  public Long save(String remarks) throws Exception ;
	  public List<RemarkVo> getSlcRejectReasons() ;
	  public List<RemarkVo> getFlcRejectReasons() ;
	  public List<RemarkVo> getRejectReasons(String header);
	public List<RemarkVo> getAuditRejectReasons();
}

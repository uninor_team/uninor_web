package speedoc.co.in.dao;

import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.UserVo;

public interface ICafStatusDao {

	public CafStatusVo saveCafStatus(CafStatusVo cafStatusVo) throws Exception ;
	public Boolean updateCafStatusLatestToOld(Boolean latestind, Long id) throws Exception;
	public CafStatusVo getLatestCafStatusVoByCafTblId(Long cafTblId);
//	public Integer updateCafStatus(CafFlcVo cafFlcVo) throws Exception;
	public Integer updateCafStatusAudit(CafAuditVo cafAuditVo);
	public boolean alreadyScrutinyComplete(CafStatusVo cafStatusVo);
	public boolean alreadyFLCComplete(CafStatusVo cafStatusVo);
	public Integer updateCafStatus1(CafScrutinyVo cafScrutinyVo)throws Exception;
//	public Integer updateCafStatus(TempPoolVo tempFlcPoolVo);
	public boolean updateCafStatus(String ids) throws Exception;
	public Boolean updateCafStatus(UserVo userVo);
	public boolean updateCurrentAccessUserId(Long cafId, Integer statusCode);
	public boolean updateCurrentAccessUserId1(Long cafId, int statusCode);
	public Boolean updateCafStatusLatestToOldSearch(boolean latestInd, Long cafTblId, Long userId);
	public boolean updateCurrentUserId(Long id);
}

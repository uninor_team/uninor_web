package speedoc.co.in.dao;

import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.UserVo;

public interface IFlcPoolDao {

	public CafStatusVo getUnknownCaf(UserVo userVo);
//	public FlcPoolVo save(FlcPoolVo flcPoolVo ) throws Exception;
	public int updateFlcPool(CafFlcVo cafFlcVo) throws Exception ;
	public boolean delete(Long id) throws Exception  ;
	public boolean deleteFlcPool();
	public Boolean findSpeedocIdExistInAvam(String speedocId);
//	public TempPoolVo saveTempPool(TempPoolVo tempPoolVo);
//	public List<TempPoolVo> getTempPool(UserVo userVo);
//	public boolean deleteTempPool(Long flcPoolId);
//	public boolean deleteRecordTempPool(Long id);
//	public String getTempPool(UserVo userVo);
//	public void deleteTempPool();
}

package speedoc.co.in.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.vo.FtpReportVo;
import speedoc.co.in.vo.StatusViewerVo;

@Repository
public class StatusDao implements IStatusDao {

	@Autowired
	SessionFactory sessionFactory;

	private static Logger logger = Logger.getLogger(StatusDao.class);

	// status_viewer
	@SuppressWarnings("unchecked")
	@Override
	public List<StatusViewerVo> getCountbyAdmin(String fromdate, String todate,
			String username) {
		logger.info("fromdate :: "+fromdate+" todate :: "+todate);
		String condition = "";
//AND date(caf_status_tbl.created_date) between '2014-12-10' AND '2014-12-11'
		String fromDateArray[]= fromdate.split("/");
		String toDateArray[]= todate.split("/");
		
		if (!fromdate.equals("") && !todate.equals("")) {
			//condition = " AND to_char(caf_status_tbl.created_date, 'dd/MM/yyyy') BETWEEN '"+ fromdate + "' AND '" + todate + "'";
			condition="AND date(caf_status_tbl.created_date) between '"+fromDateArray[2].trim()+"-"+fromDateArray[1].trim()+"-"+fromDateArray[0].trim()+"' AND '" +toDateArray[2].trim()+"-"+toDateArray[1].trim()+"-"+toDateArray[0].trim()+"'";
			logger.info("if condition :: "+condition);
		} else {
			condition = " AND to_char(caf_status_tbl.created_date, 'dd/MM/yyyy' ) = to_char(CURRENT_DATE,'dd/MM/yyyy') ";
			logger.info("else condition :: "+condition);
		}
		String getcountByUser = "select statustable.username, "
				+ "sum(CASE statustable.status_id WHEN 4001 "
				+ "THEN statustable.count ELSE 0 END) AS deCompleteCount, "
				+ "sum(CASE statustable.status_id WHEN 3001 "
				+ "THEN statustable.count ELSE 0 END) AS flcAcceptCount, "
				+ "sum(CASE statustable.status_id WHEN 3002 "
				+ "THEN statustable.count ELSE 0 END) AS flcRejectCount, "
				+ "sum(CASE statustable.status_id WHEN 5001 "
				+ "THEN statustable.count ELSE 0 END) AS scrutinyCompleteCount, "
				+ "sum(CASE statustable.status_id WHEN 5002 "
				+ "THEN statustable.count ELSE 0 END) AS scrutinyRejectCount "
				+ "FROM (SELECT caf_status_tbl.status_id, user_tbl.username, "
				+ "COUNT(*) AS count FROM caf_status_tbl JOIN user_tbl "
				+ "ON user_tbl.id=caf_status_tbl.created_by WHERE caf_status_tbl.status_id "
				+ "IN (4001,3001,3002,5001,5002) "
				+ condition
				+ "GROUP BY caf_status_tbl.status_id,user_tbl.username) as statustable "
				+ "GROUP BY statustable.username ORDER BY statustable.username asc";
		try {
			Session session = sessionFactory.getCurrentSession();
			List<Object> list = session.createSQLQuery(getcountByUser).list();
			logger.info("list :: "+list.size());			
			return convertToStatusViewerVoList(list);
		} catch (Exception e) {
			logger.error("StatusDao :: getCountbyAdmin :: " + e.getMessage());
		}
		return null;
	}

	private List<StatusViewerVo> convertToStatusViewerVoList(List<Object> list) {
		Iterator<Object> it = list.iterator();
		List<StatusViewerVo> statusViewerVos = new ArrayList<StatusViewerVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			StatusViewerVo statusViewerVo = new StatusViewerVo();
			statusViewerVo.setUsername((String)object[0]);
			statusViewerVo.setDeCompleteCount((BigDecimal) object[1]);
			statusViewerVo.setFlcAcceptCount((BigDecimal) object[2]);
			statusViewerVo.setFlcRejectCount((BigDecimal) object[3]);
			statusViewerVo.setScrutinyCompleteCount( (BigDecimal) object[4]);
			statusViewerVo.setScrutinyRejectCount((BigDecimal) object[5]);
			statusViewerVos.add(statusViewerVo);
			logger.info("statusViewerVos :: "+statusViewerVos);
		}
		return statusViewerVos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FtpReportVo> getFtpReport(Timestamp fromDate, Timestamp toDate) {
		List<FtpReportVo> ftpReportList=null;
		String sql = "SELECT ct.speedoc_id AS speedocId, ct.mobile_number AS mobileNumber, ct.caf_number AS cafNumber, " +
				"ct.unknown_number AS unknownNumber, ct.dtr_code AS dtrCode, ct.scanned_date AS scannedDate, " +
				"ct.caf_file_location AS cafFileLocation, " +
				"ct.cao_code AS caoCode, user_tbl.username, " +
				"cst.created_date AS createdDate FROM caf_tbl AS ct " +
				"JOIN caf_status_tbl AS cst ON ct.id = cst.caf_tbl_id " +
				"JOIN user_tbl ON user_tbl.id=cst.created_by " +
				"WHERE cst.status_id = "+StatusCodeEnum.AVAM_SUCCESS.getStatusCode()+" AND ct.mobile_number IS NOT NULL " +
				"AND cst.created_date BETWEEN '"+fromDate+"' AND '"+toDate+"' " +
				"ORDER BY cst.created_date";
		try {
			ftpReportList = sessionFactory.getCurrentSession().createSQLQuery(sql)
			.addScalar("speedocId", StandardBasicTypes.STRING)
			.addScalar("mobileNumber", StandardBasicTypes.STRING)
			.addScalar("cafNumber", StandardBasicTypes.STRING)
			.addScalar("unknownNumber", StandardBasicTypes.STRING)
			.addScalar("dtrCode", StandardBasicTypes.STRING)
			.addScalar("scannedDate", StandardBasicTypes.TIMESTAMP)
			.addScalar("caoCode", StandardBasicTypes.STRING)
			.addScalar("username", StandardBasicTypes.STRING)
			.addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
			.addScalar("cafFileLocation", StandardBasicTypes.STRING)
			.setResultTransformer(Transformers.aliasToBean(FtpReportVo.class)).list();
			logger.info("ftpReportList.size() :: "+ftpReportList.size());
		} catch (Exception e) {
			logger.error("Error in getFtpReport in StatusDao :: "+e.getMessage(),e);
		}
		return ftpReportList;
	}

	@Override
	public void getIndexingData(Timestamp timestamp, Timestamp timestamp2, String fileName) {
		try{
			String sql = "COPY(SELECT mobile_number, first_name, last_name, caf_number, created_date, caf_tracking_status,imsi_number, upc_code, " +
					"previous_service_provider_name, status_of_subscriber, existing_number, middle_name, father_first_name,father_middle_name, " +
					"father_last_name, gender, dob, nationality, pan_gir_number, uid_number, local_address, local_address_landmark, " +
					"local_city, local_district, local_state, local_pincode, permanent_address,permanent_address_landmark, permanent_city, permanent_district, " +
					"permanent_state, permanent_pincode, existing_connection, tarrif_plan_applied, email_address, value_added_service_applied, alternate_mobile_number, " +
					"subscriber_profession, poi, poiref, poi_issue_date, poi_issue_place, poi_issuing_authority, poa, poaref, poa_issue_date, poa_issue_place, " +
					"poa_issuing_authority, passport_number, visa_valid_till, type_of_visa, visa_number, local_reference_name, local_reference_address, local_reference_city, " +
					"local_reference_pincode, local_reference_contact_number, calling_party_number, friends_and_family, gprs, wap, mms, caller_back_ring_tone, status, " +
					"search_count FROM indexing_tbl " +
					"WHERE created_date BETWEEN '"+timestamp+"' AND '"+timestamp2+"' " +
					"ORDER BY created_date) TO '"+fileName+"' WITH DELIMITER ',' CSV HEADER";
			
			int executeUpdate = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			logger.info("getIndexingData executeUpdate :: "+executeUpdate);
		}catch (Exception e) {
			logger.error("Error in getIndexingData :: "+e.getMessage(),e);
		}
	}
	
	@Override
	public void getScruitinyRejectData(Timestamp timestamp, Timestamp timestamp2, String fileName) {
		try{
			String sql = "COPY(	SELECT ct.speedoc_id, min(ct.mobile_number) as mobile_number, min(ct.caf_number) as caf_number, "
					+ "min(ct.dtr_code) as dtr_code,min(ct.cao_code) as cao_code, min(ct.scanned_date) as scanned_date, "
					+ "to_char(min(cst.created_date),'dd-Mon-yy') as received_date, min(cst.status_id) as status_id, "
					+ "min(ut.username) as username,array_to_string(array_agg(remark_tbl.reject_reason), ',') as reject_reason, "
					+ "array_to_string(array_agg(ht.header_name), ',') as header_name "
					+ "FROM caf_tbl ct "
					+ "JOIN caf_status_tbl cst ON ct.id = cst.caf_tbl_id "
					+ "JOIN user_tbl ut ON cst.created_by = ut.id  "
					+ "JOIN slc_remark_tbl srt ON cst.id = srt.caf_status_id "
					+ "JOIN remark_tbl ON srt.remark_id = remark_tbl.id "
					+ "JOIN header_remark_tbl hrt ON remark_tbl.id = hrt.remark_id "
					+ "JOIN header_tbl ht ON hrt.header_id = ht.id "
					+ "WHERE cst.status_id = 5002 and cst.created_date between '"+timestamp+"' AND '"+timestamp2+"' "
					+ "GROUP BY  ct.speedoc_id, cst.created_date "
					+ "ORDER BY cst.created_date) "
					+ "TO '"+fileName+"' WITH DELIMITER ',' CSV HEADER";
			
			int executeUpdate = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			logger.info("getIndexingData executeUpdate :: "+executeUpdate);
		}catch (Exception e) {
			logger.error("Error in getIndexingData :: "+e.getMessage(),e);
		}
	}
}

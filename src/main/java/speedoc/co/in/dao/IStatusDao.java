package speedoc.co.in.dao;

import java.sql.Timestamp;
import java.util.List;

import speedoc.co.in.vo.FtpReportVo;
import speedoc.co.in.vo.StatusViewerVo;

public interface IStatusDao {

	List<StatusViewerVo> getCountbyAdmin(String fromdate, String todate,
			String username);

	List<FtpReportVo> getFtpReport(Timestamp fromDate, Timestamp toDate);

	void getIndexingData(Timestamp timestamp, Timestamp timestamp2, String indexingPath);

	void getScruitinyRejectData(Timestamp timestamp, Timestamp timestamp2,String rejectScruitnyPath);

}

package speedoc.co.in.dao;

import speedoc.co.in.vo.FlcRemarkVo;

public interface IFlcRemarkDao {
	public Long save(FlcRemarkVo flcRemarkVo ) throws Exception;
}

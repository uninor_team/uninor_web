package speedoc.co.in.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Caf;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FlcAcceptVo;
import speedoc.co.in.vo.PosCposVo;
import speedoc.co.in.vo.UserVo;

@Repository
public class CafDao implements ICafDao {

	private static Logger log = Logger.getLogger(CafDao.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean findSpeedocIdExists(String speedocId) {
		long count = 0;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Caf.class)
					.add(Restrictions.eq("speedocId", speedocId))
					.setProjection(Projections.rowCount()).uniqueResult();
			log.info("findSpeedocIdExists :: getCountBySpeedocId :: count ::"
					+ count);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Erron in findSpeedocIdExists method ", e);
			return false;
		}
	}

	@Override
	public CafVo saveCaf(CafVo cafVo) throws Exception {

		Caf caf = new Caf();
		CafVo savedCafVo = null;

		Utils.copyProperties(caf, cafVo);
		Session session = sessionFactory.getCurrentSession();
		session.save(caf);
		if (caf != null) {
			savedCafVo = new CafVo();
			Utils.copyProperties(savedCafVo, caf);
		}
		return savedCafVo;

	}

	@Override
	public int updateSpeedocIdInCafById(String speedocId, Long id, UserVo userVo)
			throws Exception {
		log.info("updateSpeedocIdInCafById id=" + id + " For speedocId ="
				+ speedocId);
		String hql = "UPDATE Caf SET speedocId = :speedocId, modifiedBy=:modifiedBy, modifiedDate=:modifiedDate where id = :id";
		int result = sessionFactory
				.getCurrentSession()
				.createQuery(hql)
				.setParameter("speedocId", speedocId)
				.setParameter("modifiedBy", userVo.getId())
				.setParameter("modifiedDate",new Timestamp(new Date().getTime()))
				.setParameter("id", id).executeUpdate();
		return result;
	}

	@Override
	public CafVo findCafById(Long id) throws Exception {
		CafVo cafVo = null;
		Caf caf = (Caf) sessionFactory.getCurrentSession().load(Caf.class, id);
		if (caf != null) {
			cafVo = new CafVo();
			Utils.copyProperties(cafVo, caf);
		}
		return cafVo;
	}

	@Override
	public void updateCafFromIndexing(CafVo cafVO) {

		String hql = "UPDATE Caf SET  simNumber =:simNumber, "
				+ "photograph =:photograph, category=:category, gender=:gender, firstName=:firstName, "
				+ "middleName=:middleName, lastName=:lastName,dob=:dob,nationality=:nationality, "
				+ "fatherFirstName=:fatherFirstName, fatherMiddleName=:fatherMiddleName, fatherLastName=:fatherLastName, "
				+ "localAddress=:localAddress,localAddressLandmark=:localAddressLandmark,localCity=:localCity, "
				+ "localPincode=:localPincode, localDistrict=:localDistrict, localState=:localState, "
				+ "permanentAddress=:permanentAddress, permanentAddressLandmark=:permanentAddressLandmark, "
				+ "permanentCity=:permanentCity,permanentPincode=:permanentPincode, "
				+ "permanentDistrict=:permanentDistrict,permanentState=:permanentState, "
				+ "localReferenceName=:localReferenceName, localReferenceAddress=:localReferenceAddress, "
				+ "localReferenceCity=:localReferenceCity,localReferencePincode=:localReferencePincode, "
				+ "localReferenceContactNumber=:localReferenceContactNumber, "
				+ "alternateMobileNumber=:alternateMobileNumber, form6061=:form6061, poi=:poi, poiref=:poiref, "
				+ "poa=:poa, poaref=:poaref,languageId=:languageId, modifiedDate=:modifiedDate, "
				+ "panGirNumber=:panGirNumber, mnp=:mnp, passportNumber=:passportNumber,typeOfVisa=:typeOfVisa, "
				+ "visaValidTill=:visaValidTill, visaNumber=:visaNumber, foreignNational=:foreignNational, "
				+ "outstation=:outstation, modifiedBy=:modifiedBy,existingConnection=:existingConnection,"
				+ "callerBackRingTone=:callerBackRingTone, friendsAndFamily=:friendsAndFamily,"
				+ "gprs=:gprs, wap=:wap, mms=:mms, coverageStrength=:coverageStrength, "
				+ "attractiveCallRates=:attractiveCallRates, poiIssueDate =:poiIssueDate,poiIssuePlace=:poiIssuePlace,poiIssuingAuthority=:poiIssuingAuthority, "
				+ " poaIssueDate=:poaIssueDate,poaIssuePlace=:poaIssuePlace,poaIssuingAuthority=:poaIssuingAuthority,upcCode=:upcCode,"
				+ "statusOfSubscriber=:statusOfSubscriber,valueAddedServiceApplied=:valueAddedServiceApplied,imsiNumber=:imsiNumber,subscriberProfession=:subscriberProfession,"
				+ "uidNumber=:uidNumber,tarrifPlanApplied=:tarrifPlanApplied,email=:email,existingNumber=:existingNumber,"
				+ "previousServiceProviderName=:previousServiceProviderName, cafTrackingStatus=:cafTrackingStatus "
				+ "WHERE id=:id";

		org.hibernate.Query query = sessionFactory.getCurrentSession().createQuery(hql);

		query.setParameter("simNumber", cafVO.getSimNumber());
		query.setParameter("photograph", cafVO.getPhotograph());
		query.setParameter("category", cafVO.getCategory());
		query.setParameter("gender", cafVO.getGender());
		query.setParameter("firstName", cafVO.getFirstName().trim());
		if(cafVO.getMiddleName() == null){
			query.setParameter("middleName", cafVO.getMiddleName());
		}else{
			query.setParameter("middleName", cafVO.getMiddleName().trim());
		}
		query.setParameter("lastName", cafVO.getLastName().trim());
		query.setParameter("dob", cafVO.getDob());
		query.setParameter("nationality", cafVO.getNationality());
		query.setParameter("fatherFirstName", cafVO.getFatherFirstName());
		query.setParameter("fatherMiddleName", cafVO.getFatherMiddleName());
		query.setParameter("fatherLastName", cafVO.getFatherLastName());
		query.setParameter("localAddress", cafVO.getLocalAddress());
		query.setParameter("localAddressLandmark",
				cafVO.getLocalAddressLandmark());
		query.setParameter("localCity", cafVO.getLocalCity());
		query.setParameter("localPincode", cafVO.getLocalPincode());
		query.setParameter("localDistrict", cafVO.getLocalDistrict());
		query.setParameter("localState", cafVO.getLocalState());
		query.setParameter("permanentAddress", cafVO.getPermanentAddress());
		query.setParameter("permanentAddressLandmark",
				cafVO.getPermanentAddressLandmark());
		query.setParameter("permanentCity", cafVO.getPermanentCity());
		query.setParameter("permanentPincode", cafVO.getPermanentPincode());
		query.setParameter("permanentDistrict", cafVO.getPermanentDistrict());
		query.setParameter("permanentState", cafVO.getPermanentState());
		query.setParameter("localReferenceName", cafVO.getLocalReferenceName());
		query.setParameter("localReferenceAddress",
				cafVO.getLocalReferenceAddress());
		query.setParameter("localReferenceCity", cafVO.getLocalReferenceCity());
		query.setParameter("localReferencePincode",
				cafVO.getLocalReferencePincode());
		query.setParameter("localReferenceContactNumber",
				cafVO.getLocalReferenceContactNumber());
		query.setParameter("alternateMobileNumber",
				cafVO.getAlternateMobileNumber());
		query.setParameter("form6061", cafVO.getForm6061());
		query.setParameter("poi", cafVO.getPoi());
		query.setParameter("poiref", cafVO.getPoiref());
		query.setParameter("poa", cafVO.getPoa());
		query.setParameter("poaref", cafVO.getPoaref());
		query.setParameter("languageId", 1);
		query.setParameter("panGirNumber", cafVO.getPanGirNumber());
		query.setParameter("mnp", cafVO.getMnp());
		query.setParameter("passportNumber", cafVO.getPassportNumber());
		query.setParameter("typeOfVisa", cafVO.getTypeOfVisa());
		query.setParameter("visaValidTill", cafVO.getVisaValidTill());
		query.setParameter("visaNumber", cafVO.getVisaNumber());

		query.setParameter("modifiedDate", new Timestamp(new Date().getTime()));
		

		if (cafVO.getForeignNational() != null) {
			query.setParameter("foreignNational", "Y");
		} else {
			query.setParameter("foreignNational", "N");
		}

		if (cafVO.getOutstation() != null) {
			query.setParameter("outstation", "Y");
		} else {
			query.setParameter("outstation", "N");
		}
		query.setParameter("modifiedBy", cafVO.getModifiedBy());
		query.setParameter("existingConnection", cafVO.getExistingConnection());
		query.setParameter("callerBackRingTone", cafVO.getCallerBackRingTone());
		query.setParameter("friendsAndFamily", cafVO.getFriendsAndFamily());
		query.setParameter("gprs", cafVO.getGprs());
		query.setParameter("wap", cafVO.getWap());
		query.setParameter("mms", cafVO.getMms());
		query.setParameter("coverageStrength", cafVO.getCoverageStrength());
		query.setParameter("attractiveCallRates",
				cafVO.getAttractiveCallRates());
		query.setParameter("poiIssueDate", cafVO.getPoiIssueDate());
		query.setParameter("poiIssuePlace", cafVO.getPoiIssuePlace());
		query.setParameter("poiIssuingAuthority",
				cafVO.getPoiIssuingAuthority());
		query.setParameter("poaIssueDate", cafVO.getPoaIssueDate());
		query.setParameter("poaIssuePlace", cafVO.getPoaIssuePlace());
		query.setParameter("poaIssuingAuthority",
				cafVO.getPoaIssuingAuthority());
		query.setParameter("upcCode", cafVO.getUpcCode());
		query.setParameter("statusOfSubscriber", cafVO.getStatusOfSubscriber());
		query.setParameter("valueAddedServiceApplied",
				cafVO.getValueAddedServiceApplied());
		query.setParameter("imsiNumber", cafVO.getImsiNumber());
		query.setParameter("category", cafVO.getCategory());
		query.setParameter("subscriberProfession",
				cafVO.getSubscriberProfession());
		query.setParameter("uidNumber", cafVO.getUidNumber());
		query.setParameter("tarrifPlanApplied", cafVO.getTarrifPlanApplied());
		query.setParameter("email", cafVO.getEmail());
		query.setParameter("previousServiceProviderName",
				cafVO.getPreviousServiceProviderName());
		query.setParameter("existingNumber", cafVO.getExistingNumber());
		query.setParameter("previousServiceProviderName",
				cafVO.getPreviousServiceProviderName());
		query.setParameter("cafTrackingStatus", cafVO.getCafTrackingStatus());
		
		query.setParameter("id", cafVO.getId());

		int result = query.executeUpdate();
		log.info("updateCafFromIndexing :: result :: " + result);

	}

	@Override
	public CafVo findSpeedocIdAlreadyExists(String speedocId) {
		CafVo cafVo = null;
		String sql = "SELECT speedoc_id as speedocId FROM caf_tbl WHERE speedoc_id ILIKE '"
				+ speedocId + "%' ORDER BY speedoc_id DESC limit 1";
		try {
			cafVo = (CafVo) sessionFactory.getCurrentSession().createSQLQuery(sql)
					.addScalar("speedocId", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(CafVo.class)).uniqueResult();
			log.info("findSpeedocIdAlreadyExists :: List of cafVo :: " + cafVo);
			return cafVo;
		} catch (Exception e) {
			log.error("Error in findSpeedocIdAlreadyExists :: " + e.getMessage(),e);
		}
		return null;
	}

	public Boolean updateOnhold(Long id, Integer onhold) {
		String hql = "UPDATE Caf set onhold =:onhold WHERE id=:id";
		org.hibernate.Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("onhold", onhold);
		query.setParameter("id", id);
		int result = query.executeUpdate();
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public CafVo findOnhold(Long cafId) {
		CafVo cafVo = null;
		try {
			Caf caf = (Caf) sessionFactory.getCurrentSession().createCriteria(Caf.class).add(Restrictions.eq("id", cafId)).uniqueResult();
			if (caf != null) {
				cafVo = new CafVo();
				Utils.copyProperties(cafVo, caf);
			}
		} catch (Exception e) {
			log.error("Error in findOnhold :: " + e.getMessage(), e);
		}
		return cafVo;
	}

	@Override
	public CafVo getOnholdByCafId(Long cafId) {
		CafVo cafVo = null;
		try {
			Caf caf = (Caf) sessionFactory.getCurrentSession().createCriteria(Caf.class).add(Restrictions.eq("id", cafId)).uniqueResult();
			if (caf != null) {
				cafVo = new CafVo();
				Utils.copyProperties(cafVo, caf);
				return cafVo;
			}
		} catch (Exception e) {
			log.error("Error in getOnholdByCafId" + e.getMessage(), e);
		}
		return null;
	}

	@Override
	public boolean findSpeedocIdAndStatusExists(String speedocId) {
		BigInteger count = null;
		long count1 = 0;
		try {
			String sql = "select count(*) from caf_status_tbl cst "
					+ "join caf_tbl ct on ct.id = cst.caf_tbl_id "
					+ "where ct.speedoc_id = '" + speedocId + "' and "
					+ "cst.status_id IN ('"+ StatusCodeEnum.SCRUTINY_COMPLETE.getStatusCode() + "','"
					+ StatusCodeEnum.SCRUTINY_REJECT.getStatusCode() + "')";
			count = (BigInteger) sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult();
			log.info("findSpeedocIdAndStatusExists :: count ::" + count);
			count1 = count.longValue();
			if (count1 > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Erron in findSpeedocIdAndStatusExists method "+ e.getMessage(), e);
			return false;
		}
	}

	@Override
	public PosCposVo getPosDetail(String speedocId) {
		try {
			String sql = "SELECT rtr_code as rtrCode,rtr_name as rtrName FROM pos_cpos_tbl where speedoc_id = '"
					+ speedocId + "'";
			PosCposVo posCposVo = (PosCposVo) sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("rtrCode", StandardBasicTypes.STRING)
					.addScalar("rtrName", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(PosCposVo.class)).uniqueResult();
			return posCposVo;
		
		} catch (Exception e) {
			log.error("Error in getPosDetail :: "+e.getMessage(),e);
			return null;
		}
	}
	@Override
	public FlcAcceptVo checkOnholdAndSpeedocIdExist(String speedocId, Long cafId, Long id) {
		try {
			log.info("checkOnholdAndSpeedocIdExist of user =" + id + " For speedocId =" + speedocId + " and cafId =" +cafId);
			
			String sql = "SELECT updated_result as updatedResult FROM sp_flc_accept('" + speedocId + "'," + cafId + "," + id + ")";
			FlcAcceptVo flcAcceptVo = (FlcAcceptVo) sessionFactory.getCurrentSession().createSQLQuery(sql)
					.addScalar("updatedResult", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(FlcAcceptVo.class)).uniqueResult();
			
			System.out.println(" CafDao :: checkOnholdAndSpeedocIdExist ::updatedResult :: "+flcAcceptVo.getUpdatedResult());
			if(flcAcceptVo !=null){
				return flcAcceptVo;
			}
		} catch (HibernateException e) {
			log.error("exception in checkOnholdAndSpeedocIdExist"+e.getMessage(),e);
		}
		return null;
	}

	@Override
	public synchronized CafScrutinyVo getDeCompletedCaf(UserVo userVo) throws Exception{
		CafScrutinyVo cafScrutinyVo;
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			log.info("inside getDeCompletedCaf :: userVo.id ::"+userVo.getId());
			cafScrutinyVo = null;
			//String sql = "select * from sp_get_de_completed_caf("+cafTblId+")"; // NEW QUERY ADDED ON 28-08-2015
			String sql = "select id as id ,speedoc_id as speedocId ,file_id as fileId ,mobile_number as mobileNumber, sim_number as simNumber ,caf_number as cafNumber, unknown_number as unknownNumber,"
					+ "report as report ,photograph as photograph,mnp as mnp ,existing_number as existingNumber,connection_type as connectionType ,foreign_national as foreignNational,"
					+ "outstation as outstation,first_name as firstName,middle_name as middleName,last_name as lastName,gender as gender , email as email,service as service,dob as dob ,nationality as nationality ,"
					+ "father_first_name as fatherFirstName,father_middle_name as fatherMiddleName,father_last_name as fatherLastName ,local_address  as localAddress,"
					+ "local_address_landmark  as localAddressLandmark,local_city  as localCity,local_pincode as localPincode ,local_district as localDistrict, local_state as localState,"
					+ "alternate_mobile_number as alternateMobileNumber,permanent_address as permanentAddress, permanent_address_landmark as permanentAddressLandmark ,permanent_city as permanentCity,"
					+ "permanent_pincode as permanentPincode ,permanent_district as permanentDistrict,permanent_state as permanentState,pan_gir_number as panGirNumber ,poi as poi ,poiref as poiref,poa as poa, "
					+ "poaref as poaref,passport_number as passportNumber,visa_valid_till as visaValidTill ,type_of_visa as typeOfVisa,visa_number as visaNumber,local_reference_name as localReferenceName ,"
					+ "local_reference_address as localReferenceAddress,local_reference_city as localReferenceCity ,local_reference_pincode as localReferencePincode,"
					+ "local_reference_contact_number as localReferenceContactNumber,category as category,caf_date as cafDate,form_66 as form6061 ,language_id as languageId ,caf_type as cafType,"
					+ "caf_file_location as cafFileLocation ,pages as pages,parent_id as parentId ,activation_date as activationDate,monthly_expenditure as monthlyExpenditure ,"
					+ "existing_connection as existingConnection,connection_opting_for as connectionOptingFor,caller_back_ring_tone as callerBackRingTone,friends_and_family as friendsAndFamily,"
					+ "gprs as gprs ,wap as wap ,mms as mms,coverage_strength as coverageStrength, billing_transparency as billingTransparency,quality_of_customer_service as qualityOfCustomerService ,"
					+ "more_retail_outlets as moreRetailOutlets,attractive_call_rates as attractiveCallRates , calling_party_number as callingPartyNumber,poi_issue_date as poiIssueDate ,"
					+ "poi_issue_place as poiIssuePlace,poi_issuing_authority as poiIssuingAuthority,poa_issue_date as poaIssueDate ,poa_issue_place as poaIssuePlace,poa_issuing_authority as poaIssuingAuthority,"
					+ "upc_code as upcCode ,previous_operator_circle as previousOperatorCircle,point_of_sale_code as pointOfSaleCode,pos_address as posAddress,pos_state as posState,"
					+ "pos_city as posCity,pos_pin_code as posPinCode,pos_code as posCode,activation_officer_name as activationOfficerName,activation_officer_designation as activationOfficerDesignation ,"
					+ "status_of_subscriber as statusOfSubscriber,value_added_service_applied as valueAddedServiceApplied ,imsi_number as imsiNumber,area_type as areaType ,"
					+ "subscriber_profession as subscriberProfession,uid_number as uidNumber,tarrif_plan_applied as tarrifPlanApplied ,previous_service_provider_name as previousServiceProviderName ,"
					+ "dtr_code as dtrCode,cp_id as cpId ,created_date as createdDate ,modified_date as modifiedDate,created_by as createdBy,modified_by as modifiedBy ,"
					+ "scanned_date as scannedDate,cao_code as caoCode,onhold as onhold ,rtr_code as rtrCode ,rtr_name as rtrName,"
					+ "caf_tracking_status as cafTrackingStatus,polling_station as pollingStation,voter_id as voterId,voter_name as voterName,caf_status_id as cafStatusId from sp_get_de_completed_caf_from_slc_pool('"
					+ StatusCodeEnum.DE_COMPLETE.getStatusCode() + "','"
					+ userVo.getId() + "')";
					
			cafScrutinyVo = (CafScrutinyVo) session.createSQLQuery(sql)
					.addScalar("id", StandardBasicTypes.LONG).addScalar("speedocId",StandardBasicTypes.STRING).addScalar("fileId", StandardBasicTypes.STRING)
					.addScalar("mobileNumber",StandardBasicTypes.STRING).addScalar("simNumber", StandardBasicTypes.STRING).addScalar("cafNumber",StandardBasicTypes.STRING)
					.addScalar("unknownNumber", StandardBasicTypes.STRING).addScalar("report",StandardBasicTypes.STRING).addScalar("photograph", StandardBasicTypes.STRING)
					.addScalar("mnp",StandardBasicTypes.STRING).addScalar("existingNumber", StandardBasicTypes.STRING).addScalar("connectionType",StandardBasicTypes.STRING)
					.addScalar("foreignNational", StandardBasicTypes.STRING).addScalar("outstation",StandardBasicTypes.STRING).addScalar("firstName", StandardBasicTypes.STRING)
					.addScalar("middleName",StandardBasicTypes.STRING).addScalar("lastName", StandardBasicTypes.STRING).addScalar("gender",StandardBasicTypes.STRING)
					.addScalar("email", StandardBasicTypes.STRING).addScalar("service",StandardBasicTypes.STRING).addScalar("dob", StandardBasicTypes.TIMESTAMP)
					.addScalar("nationality",StandardBasicTypes.STRING).addScalar("fatherFirstName", StandardBasicTypes.STRING).addScalar("fatherMiddleName",StandardBasicTypes.STRING)
					.addScalar("fatherLastName", StandardBasicTypes.STRING).addScalar("localAddress",StandardBasicTypes.STRING).addScalar("localAddressLandmark", StandardBasicTypes.STRING)
					.addScalar("localCity",StandardBasicTypes.STRING).addScalar("localPincode", StandardBasicTypes.STRING).addScalar("localDistrict",StandardBasicTypes.STRING)
					.addScalar("localState", StandardBasicTypes.STRING).addScalar("alternateMobileNumber",StandardBasicTypes.STRING).addScalar("permanentAddress", StandardBasicTypes.STRING)
					.addScalar("permanentAddressLandmark",StandardBasicTypes.STRING).addScalar("permanentCity", StandardBasicTypes.STRING).addScalar("permanentPincode",StandardBasicTypes.STRING)
					.addScalar("permanentDistrict", StandardBasicTypes.STRING).addScalar("permanentState",StandardBasicTypes.STRING).addScalar("panGirNumber", StandardBasicTypes.STRING)
					.addScalar("poi",StandardBasicTypes.STRING).addScalar("poiref", StandardBasicTypes.STRING).addScalar("poa",StandardBasicTypes.STRING)
					.addScalar("poaref", StandardBasicTypes.STRING).addScalar("passportNumber",StandardBasicTypes.STRING).addScalar("visaValidTill", StandardBasicTypes.TIMESTAMP)
					.addScalar("typeOfVisa",StandardBasicTypes.STRING).addScalar("visaNumber", StandardBasicTypes.STRING).addScalar("localReferenceName",StandardBasicTypes.STRING)
					.addScalar("localReferenceAddress", StandardBasicTypes.STRING).addScalar("localReferenceCity",StandardBasicTypes.STRING).addScalar("localReferencePincode", StandardBasicTypes.STRING)
					.addScalar("localReferenceContactNumber",StandardBasicTypes.STRING).addScalar("category", StandardBasicTypes.STRING).addScalar("cafDate",StandardBasicTypes.TIMESTAMP)
					.addScalar("form6061", StandardBasicTypes.INTEGER).addScalar("languageId",StandardBasicTypes.INTEGER).addScalar("cafType", StandardBasicTypes.INTEGER)
					.addScalar("cafFileLocation",StandardBasicTypes.STRING).addScalar("pages", StandardBasicTypes.INTEGER).addScalar("parentId",StandardBasicTypes.STRING)
					.addScalar("activationDate", StandardBasicTypes.TIMESTAMP).addScalar("monthlyExpenditure",StandardBasicTypes.INTEGER).addScalar("existingConnection", StandardBasicTypes.STRING)
					.addScalar("connectionOptingFor",StandardBasicTypes.STRING).addScalar("callerBackRingTone", StandardBasicTypes.STRING).addScalar("friendsAndFamily",StandardBasicTypes.STRING)
					.addScalar("gprs", StandardBasicTypes.STRING).addScalar("wap",StandardBasicTypes.STRING).addScalar("mms", StandardBasicTypes.STRING)
					.addScalar("coverageStrength",StandardBasicTypes.STRING).addScalar("billingTransparency", StandardBasicTypes.STRING).addScalar("qualityOfCustomerService",StandardBasicTypes.STRING)
					.addScalar("moreRetailOutlets", StandardBasicTypes.STRING).addScalar("attractiveCallRates",StandardBasicTypes.STRING).addScalar("callingPartyNumber", StandardBasicTypes.STRING)
					.addScalar("poiIssueDate",StandardBasicTypes.TIMESTAMP).addScalar("poiIssuePlace", StandardBasicTypes.STRING).addScalar("poiIssuingAuthority",StandardBasicTypes.STRING)
					.addScalar("poaIssueDate", StandardBasicTypes.TIMESTAMP).addScalar("poaIssuePlace",StandardBasicTypes.STRING).addScalar("poaIssuingAuthority", StandardBasicTypes.STRING)
					.addScalar("upcCode",StandardBasicTypes.STRING).addScalar("previousOperatorCircle", StandardBasicTypes.STRING).addScalar("pointOfSaleCode",StandardBasicTypes.STRING)
					.addScalar("posAddress", StandardBasicTypes.STRING).addScalar("posState",StandardBasicTypes.STRING).addScalar("posCity", StandardBasicTypes.STRING)
					.addScalar("posPinCode",StandardBasicTypes.STRING).addScalar("posCode", StandardBasicTypes.STRING).addScalar("activationOfficerName",StandardBasicTypes.STRING)
					.addScalar("activationOfficerDesignation", StandardBasicTypes.STRING).addScalar("statusOfSubscriber",StandardBasicTypes.STRING).addScalar("valueAddedServiceApplied", StandardBasicTypes.STRING)
					.addScalar("imsiNumber",StandardBasicTypes.STRING).addScalar("areaType", StandardBasicTypes.STRING).addScalar("subscriberProfession",StandardBasicTypes.STRING)
					.addScalar("uidNumber", StandardBasicTypes.STRING).addScalar("tarrifPlanApplied",StandardBasicTypes.STRING).addScalar("previousServiceProviderName", StandardBasicTypes.STRING)
					.addScalar("dtrCode",StandardBasicTypes.STRING).addScalar("cpId", StandardBasicTypes.LONG).addScalar("createdDate",StandardBasicTypes.TIMESTAMP)
					.addScalar("modifiedDate", StandardBasicTypes.TIMESTAMP).addScalar("createdBy",StandardBasicTypes.LONG).addScalar("modifiedBy", StandardBasicTypes.LONG)
					.addScalar("scannedDate", StandardBasicTypes.TIMESTAMP).addScalar("caoCode",StandardBasicTypes.STRING).addScalar("onhold", StandardBasicTypes.INTEGER)
					.addScalar("rtrCode", StandardBasicTypes.STRING).addScalar("rtrName",StandardBasicTypes.STRING)
					.addScalar("cafTrackingStatus", StandardBasicTypes.STRING).addScalar("pollingStation",StandardBasicTypes.STRING)
					.addScalar("voterId", StandardBasicTypes.STRING).addScalar("voterName",StandardBasicTypes.STRING).addScalar("cafStatusId",StandardBasicTypes.LONG)
					.setResultTransformer(Transformers.aliasToBean(CafScrutinyVo.class)).uniqueResult();
		
			if (cafScrutinyVo != null) {
				log.info("exit from getDeCompletedCaf :: CafDao");	
				return cafScrutinyVo;
			}
		} catch (Exception e) {
			tx.rollback();
			log.info("Exception in :: CafDao :: getDeCompletedCaf "+e.getMessage(),e);	
		}finally{
			tx.commit();
			session.close();
		}
		return null;
	}
}

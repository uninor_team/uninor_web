package speedoc.co.in.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.DataException;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Avam;
import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.domain.FlcPool;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.UserVo;

@Repository
public class FlcPoolDao implements IFlcPoolDao {

	private Logger log = Logger.getLogger(FlcPoolDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public CafStatusVo getUnknownCaf(UserVo userVo) {
		try {
			// String sql =
			// "select cst.* from caf_status_tbl as cst left join "+
			// "flc_pool_tbl as fpt on fpt.caf_tbl_id = cst.caf_tbl_id where cst.end_date is null "
			// +
			// "and cst.status_id IN ("+StatusCodeEnum.UNKNOWN.getStatusCode()+","+StatusCodeEnum.FLC_REJECT.getStatusCode()+","+StatusCodeEnum.FLC_SKIP.getStatusCode()+") "+
			// "and cst.latest_ind is true and fpt.caf_tbl_id is null limit 1";

			String sql = "select * from sp_get_caf_from_flc_pool('"
					+ StatusCodeEnum.UNKNOWN.getStatusCode() + ","
					+ StatusCodeEnum.FLC_SKIP.getStatusCode() + "','"+userVo.getId() + "')"; // NEW
																		// QUERY
																		// ADDED
																		// ON
																		// 10-11-2014

			CafStatus cafStatus = (CafStatus) sessionFactory
					.getCurrentSession().createSQLQuery(sql)
					.addEntity(CafStatus.class).uniqueResult();
			log.debug("cafStatus=" + cafStatus);
			if (cafStatus != null) {
				return convertToCafStatusVo(cafStatus);
			}
		} catch (DataException e) {
			return getUnknownCaf(userVo);
		} catch (GenericJDBCException e) {
			return getUnknownCaf(userVo);
		} catch (Exception e) {
			log.error("Error in getUnknownCaf :: " + e.getMessage(), e);
			return null;
		}
		return null;
	}

//	public FlcPoolVo save(FlcPoolVo flcPoolVo) throws Exception {
//		FlcPool flcPool = new FlcPool();
//		Utils.copyProperties(flcPool, flcPoolVo);
//		Session session = sessionFactory.getCurrentSession();
//		session.save(flcPool);
//		// session.flush() ;
//		Utils.copyProperties(flcPoolVo, flcPool);
//		return flcPoolVo;
//	}

	public List<CafStatusVo> convertToFlcPoolVoList(
			List<CafStatus> cafStatusList) throws Exception {
		List<CafStatusVo> cafStatusVos = new ArrayList<CafStatusVo>();
		for (CafStatus cafStatus : cafStatusList) {
			cafStatusVos.add(convertToCafStatusVo(cafStatus));
		}
		return cafStatusVos;
	}

	public CafStatusVo convertToCafStatusVo(CafStatus cafStatus)
			throws Exception {
		CafStatusVo cafStatusVo = new CafStatusVo();
		Utils.copyProperties(cafStatusVo, cafStatus);
		return cafStatusVo;
	}

	public int updateFlcPool(CafFlcVo cafFlcVo) throws Exception {
		String hql = "UPDATE FlcPool SET endDate = :endDate where id = :id";
		int result = sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("endDate", new Timestamp(new Date().getTime()))
				.setParameter("id", cafFlcVo.getFlcPoolId()).executeUpdate();
		return result;
	}

	public boolean delete(Long id) throws Exception {
		log.info("Deleting flc_pool_tbl by id = " + id);
		Session session = sessionFactory.getCurrentSession();
		FlcPool flcPool = (FlcPool) session.get(FlcPool.class, id);
		if (flcPool != null) {
			session.delete(flcPool);
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	public boolean deleteFlcPool() {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.createSQLQuery(
					"DELETE FROM flc_pool_tbl WHERE start_date < (NOW() - INTERVAL '5 minute')")
					.addScalar("id", Hibernate.BIG_INTEGER)
					.addScalar("caf_tbl_id", Hibernate.BIG_INTEGER)
					.addScalar("start_date", Hibernate.TIMESTAMP)
					.addScalar("end_date", Hibernate.TIMESTAMP).executeUpdate();
			return true;
		} catch (Exception e) {
			log.error("Error in deleteFlcPool() " + e.getMessage(), e);
			return false;
		}
	}

	@Override
	public Boolean findSpeedocIdExistInAvam(String speedocId) {
		long count = 0;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Avam.class)
					.add(Restrictions.eq("speedocId", speedocId))
					.setProjection(Projections.rowCount()).uniqueResult();
			log.info("findSpeedocIdExistInAvam :: findSpeedocIdExistInAvam :: count ::"
					+ count);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Error in findSpeedocIdExistInAvam :: " + e.getMessage(),
					e);
		}
		return null;
	}

//	@Override
//	public TempPoolVo saveTempPool(TempPoolVo tempPoolVo) {
//		try {
//			TempPool tempFlcPool = new TempPool();
//			Utils.copyProperties(tempFlcPool, tempPoolVo);
//			Session session = sessionFactory.getCurrentSession();
//			session.save(tempFlcPool);
//			// session.flush() ;
//			Utils.copyProperties(tempPoolVo, tempFlcPool);
//			if (tempPoolVo != null) {
//				return tempPoolVo;
//			} else {
//				return null;
//			}
//		} catch (Exception e) {
//			log.error("Error in saveTempPool :: " + e.getMessage(), e);
//			return null;
//		}
//	}

	// @SuppressWarnings("unchecked")
	// @Override
	// public List<TempPoolVo> getTempPool(UserVo userVo) {
	// try {
	// List<TempPoolVo> tempFlcPoolVos = null;
	// List<TempPool> tempPools =
	// sessionFactory.getCurrentSession().createCriteria(TempPool.class).list();
	// tempFlcPoolVos = convertToTempPoolVoList(tempPools);
	// log.info("tempFlcPoolVos.size() :: "+tempFlcPoolVos.size());
	// if(tempFlcPoolVos.size() > 0 ){
	// return tempFlcPoolVos;
	// }else{
	// return tempFlcPoolVos;
	// }
	// } catch (Exception e) {
	// log.error("Error in getTempFlcPool :: "+e.getMessage(),e);
	// return null;
	// }
	// }

//	public List<TempPoolVo> convertToTempPoolVoList(List<TempPool> tempPools)
//			throws Exception {
//		List<TempPoolVo> tempPoolVos = new ArrayList<TempPoolVo>();
//		for (TempPool tempPool : tempPools) {
//			tempPoolVos.add(convertToTempPoolVo(tempPool));
//		}
//		return tempPoolVos;
//	}
//
//	private TempPoolVo convertToTempPoolVo(TempPool tempPool) throws Exception {
//		TempPoolVo tempPoolVo2 = new TempPoolVo();
//
//		Utils.copyProperties(tempPoolVo2, tempPool);
//
//		return tempPoolVo2;
//	}

//	@Override
//	public boolean deleteTempPool(Long id) {
//		log.info("Deleting flc temp_pool_tbl by id = " + id);
//		Session session = sessionFactory.getCurrentSession();
//		TempPool tempFlcPool = (TempPool) session.get(TempPool.class, id);
//		if (tempFlcPool != null) {
//			session.delete(tempFlcPool);
//			// session.flush();
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	@Override
//	public boolean deleteRecordTempPool(Long cafTblId) {
//		log.info("Deleting flc temp_pool_tbl by cafTblId = " + cafTblId);
//		try {
//			String hql = "DELETE FROM TempPool WHERE cafTblId = :cafTblId";
//			Session session = sessionFactory.getCurrentSession();
//			Query query = session.createQuery(hql);
//			query.setParameter("cafTblId", cafTblId);
//			int result = query.executeUpdate();
//			// session.flush();
//			log.info("result :: " + result);
//			return true;
//		} catch (Exception e) {
//			log.error("deleteRecordTempPool :: " + e.getMessage(), e);
//			return false;
//		}
//	}
//
//	@Override
//	public String getTempPool(UserVo userVo) {
//		try {
//			String sql = "SELECT string_agg(cast(caf_status_id AS character varying),',') from temp_pool_tbl ";
//			String ids = (String) sessionFactory.getCurrentSession()
//					.createSQLQuery(sql).uniqueResult();
//			return ids;
//		} catch (Exception e) {
//			log.error("Error in getTempFlcPool :: " + e.getMessage(), e);
//			return null;
//		}
//	}
//
//	@Override
//	public void deleteTempPool() {
//		log.info("Deleting temp_flc_pool_tbl");
//		String sql = "Delete From temp_pool_tbl";
//		int result = sessionFactory.getCurrentSession().createSQLQuery(sql)
//				.executeUpdate();
//		log.info("Deleting temp_flc_pool_tbl result :: " + result);
//	}
}

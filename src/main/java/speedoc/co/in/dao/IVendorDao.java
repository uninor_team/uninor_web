package speedoc.co.in.dao;

import speedoc.co.in.vo.VendorScanWorkstationVo;
import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

public interface IVendorDao {
		public VendorVo addVendor(VendorVo vendorVo);
		public VendorUserVo addVendorUser(VendorUserVo vendorUserVo) throws Exception;
		public VendorVo getVendorByAdminUserId(Long adminId);
		public VendorUserVo getVendorUserByUserId(Integer userId);
		public VendorScanWorkstationVo getVendorScanWorkStationByVendorId(Long vendorId);
		public VendorScanWorkstationVo getVendorScanWorkStationByRegKey(String regKey);
		public VendorScanWorkstationVo updateVendorScanWorkStationByRegKey(String macAddress,String regKey);
		public VendorVo getVendorDataByUserId(Long id) throws Exception;
		public VendorScanWorkstationVo addVendorScanWorkStation(VendorScanWorkstationVo vendorScanWorkstationVo) throws Exception;
}

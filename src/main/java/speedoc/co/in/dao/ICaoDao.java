package speedoc.co.in.dao;

import speedoc.co.in.vo.CaoVo;

public interface ICaoDao {
	public CaoVo getCaoImageByCaoCode(String caoCode);
}

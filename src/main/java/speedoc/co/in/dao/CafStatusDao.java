package speedoc.co.in.dao;

import java.math.BigInteger;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.UserVo;

@Repository
public class CafStatusDao implements ICafStatusDao {

	private static Logger log = Logger.getLogger(CafStatusDao.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CafStatusVo saveCafStatus(CafStatusVo cafStatusVo) throws Exception {
		CafStatus cafStatus = new CafStatus();
		CafStatusVo savedCafStatusVo = null;

		Utils.copyProperties(cafStatus, cafStatusVo);
		Session session = sessionFactory.getCurrentSession();
		session.save(cafStatus);

		if (cafStatus != null) {
			savedCafStatusVo = new CafStatusVo();
			Utils.copyProperties(savedCafStatusVo, cafStatus);
		}
		return savedCafStatusVo;
	}

	@Override
	public Boolean updateCafStatusLatestToOld(Boolean latestind, Long id)
			throws Exception {
		log.info("inside updateCafStatusLatestToOld id=" + id);
		String hql = "update CafStatus set latestInd = :latestInd , endDate = :endDate"
				+ ", currentlyAccessingUserId = :currentlyAccessingUserId WHERE id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("latestInd", latestind);
		query.setTimestamp("endDate", new Date());
		query.setParameter("currentlyAccessingUserId", null);
		query.setParameter("id", id);
		int result = query.executeUpdate();
		log.info("updateCafStatusLatestToOld Rows affected: " + result);
		if (result > 0) {
			return true;
		}	
		return false;
	}	

	public Integer updateCafStatus(CafFlcVo cafFlcVo) throws Exception {
		String hql = "UPDATE CafStatus SET endDate = :endDate,latestInd = :latestInd where id = :id";
		int result = sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("endDate", null).setParameter("latestInd", true)
				.setParameter("id", cafFlcVo.getCafStatusId()).executeUpdate();
		return result;
	}

	public Integer updateCafStatus1(CafScrutinyVo cafScrutinyVo)
			throws Exception {
		String hql = "UPDATE CafStatus SET endDate = :endDate,latestInd = :latestInd where id = :id";
		int result = sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("endDate", null).setParameter("latestInd", true)
				.setParameter("id", cafScrutinyVo.getCafStatusId())
				.executeUpdate();
		return result;
	}

	@Override
	public CafStatusVo getLatestCafStatusVoByCafTblId(Long cafTblId) {
		CafStatusVo cafStatusVo = null;
		try {
			CafStatus cafStatus = (CafStatus) sessionFactory
					.getCurrentSession().createCriteria(CafStatus.class)
					.add(Restrictions.isNull("endDate"))
					.add(Restrictions.eq("latestInd", Boolean.TRUE))
					.add(Restrictions.eq("cafTblId", cafTblId)).uniqueResult();
			if (cafStatus != null) {
				cafStatusVo = new CafStatusVo();
				Utils.copyProperties(cafStatusVo, cafStatus);
			}
		} catch (Exception e) {
			log.error("getLatestCafStatusVoByCafTblId :: " + e.getMessage(), e);
		}
		return cafStatusVo;
	}

	@Override
	public Integer updateCafStatusAudit(CafAuditVo cafAuditVo) {
		String hql = "UPDATE CafStatus SET endDate = :endDate,latestInd = :latestInd where id = :id";
		int result = sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("endDate", null).setParameter("latestInd", true)
				.setParameter("id", cafAuditVo.getCafStatusId())
				.executeUpdate();
		return result;
	}

	@Override
	public boolean alreadyScrutinyComplete(CafStatusVo cafStatusVo) {
		BigInteger count;
		String sql = "SELECT count(*) FROM caf_status_tbl WHERE status_id IN (5001, 5002) AND caf_tbl_id = "
				+ cafStatusVo.getCafTblId();
		try {
			Query query = sessionFactory.getCurrentSession()
					.createSQLQuery(sql);
			count = (BigInteger) query.uniqueResult();
			Long count1 = count.longValue();
			log.info("slc count1 = " + count1);
			if (count1 > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Error in alreadyScrutinyComplete :: " + e.getMessage(),e);
			return false;
		}
	}

	@Override
	public boolean alreadyFLCComplete(CafStatusVo cafStatusVo) {
		BigInteger count;
		String sql = "SELECT count(*) FROM caf_status_tbl WHERE status_id IN (3001) AND caf_tbl_id = "
				+ cafStatusVo.getCafTblId();
		try {
			Query query = sessionFactory.getCurrentSession()
					.createSQLQuery(sql);
			count = (BigInteger) query.uniqueResult();
			Long count1 = count.longValue();
			log.info("flc count1 = " + count1);
			if (count1 > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Error in alreadyFLCComplete :: " + e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean updateCafStatus(String ids) throws Exception {
		String sql = "UPDATE caf_status_tbl SET end_date = " + null
				+ ",latest_ind = " + true + " where id IN (" + ids + ")";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql)
					.executeUpdate();
			log.info("updateCafStatus Rows affected: " + result);
			if (result > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("Error in updateCafStatus(String ids) :: " + e.getMessage(), e);
			return false;
		}
	}

	public Boolean updateCafStatus(UserVo userVo) {
		String sql = "UPDATE caf_status_tbl SET end_date = null,latest_ind = true, currently_accessing_user_id = null "
				+ "where currently_accessing_user_id = '" + userVo.getId() + "'";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			log.info("updateCafStatus Rows affected: " + result);
			if (result > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("Error in updateCafStatus(UserVo userVo) :: "+e.getMessage(),e);
			return false;
		}
	}

	@Override
	public boolean updateCurrentAccessUserId(Long cafId,Integer statusCode) {
		String sql = "UPDATE caf_status_tbl SET currently_accessing_user_id = null where caf_tbl_id='"+cafId+"' and status_id="+statusCode+"";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			log.info("updateCurrentAccessUserId Rows affected: " + result);
			if (result > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("Error in updateCurrentAccessUserId :: "+e.getMessage(),e);
			return false;
		}
	}

	@Override
	public boolean updateCurrentAccessUserId1(Long cafId, int statusCode) {
		String sql = "UPDATE caf_status_tbl SET currently_accessing_user_id = null,latest_ind=true, end_date= null where caf_tbl_id="+cafId+" and status_id="+statusCode+"";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			log.info("updateCurrentAccessUserId Rows affected: " + result);
			if (result > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("Error in updateCurrentAccessUserId :: "+e.getMessage(),e);
			return false;
		}
	}

	@Override
	public Boolean updateCafStatusLatestToOldSearch(boolean latestInd, Long cafTblId, Long userId) {
		log.info("inside updateCafStatusLatestToOldSearch id=" + cafTblId);
		String sql="UPDATE caf_status_tbl SET currently_accessing_user_id = "+userId+",latest_ind="+latestInd+", end_date= '"+new Date()+"' "
				+ "where caf_tbl_id='"+cafTblId+"' and latest_ind=true and end_date is null";
		Session session = sessionFactory.getCurrentSession();
		int result = session.createSQLQuery(sql).executeUpdate();
		log.info("updateCafStatusLatestToOldSearch Rows affected: " + result);
		if (result > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCurrentUserId(Long cafId) {
		String sql = "UPDATE caf_status_tbl SET currently_accessing_user_id = null where caf_tbl_id='"+cafId+"' and currently_accessing_user_id is not null";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			log.info("updateCurrentUserId Rows affected: " + result);
			if (result > 0) {
				return true;
			}
			return false;

		} catch (Exception e) {
			log.error("Error in updateCurrentUserId :: "+e.getMessage(),e);
			return false;
		}
	}

}

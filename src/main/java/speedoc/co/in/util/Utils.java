package speedoc.co.in.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.log4j.Logger;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;

public class Utils {

	private static Logger logger = Logger.getLogger(Utils.class);
	public static final Integer barcodeLength = 12;
	public static final Integer mobileLength = 10;
	public static final Integer cafLength = 12;

	public static CafStatusVo getCafStatusVoForAvamStatus(Integer statusCode,CafVo cafVo) {
		CafStatusVo cafStatusVo = new CafStatusVo();
		cafStatusVo.setCafTblId(cafVo.getId());
		cafStatusVo.setCreatedBy(cafVo.getCreatedBy());
		cafStatusVo.setLatestInd(Boolean.TRUE);
		cafStatusVo.setStatusId(statusCode);
		cafStatusVo.setEndDate(null);
		return cafStatusVo;
	}

	public static String cryptToMD5(String str) {
		if (str == null || str.length() == 0) {
			throw new IllegalArgumentException(
					"String to encrypt cannot be null or zero length");
		}
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] hash = md.digest();

			for (int i = 0; i < hash.length; i++) {
				if ((0xff & hash[i]) < 0x10) {
					hexString.append("0"
							+ Integer.toHexString((0xFF & hash[i])));
				} else {
					hexString.append(Integer.toHexString(0xFF & hash[i]));
				}
			}
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		}
		return hexString.toString();
	}

	public static void copyProperties(java.lang.Object dest,java.lang.Object orig) throws java.lang.IllegalAccessException,java.lang.reflect.InvocationTargetException {
		BeanUtilsBean.getInstance().getConvertUtils().register(false, true, -1);
		BeanUtils.copyProperties(dest, orig);
	}

	@SuppressWarnings("static-access")
	public static void customOptionPane(String header, String msg) {
		JDialog.setDefaultLookAndFeelDecorated(true);
		UIManager UI = new UIManager();
		UI.put("OptionPane.background", new ColorUIResource(220, 0, 0));
		UI.put("Panel.background", new ColorUIResource(220, 0, 0));
		JOptionPane.showMessageDialog(null, new JLabel(msg), header,JOptionPane.INFORMATION_MESSAGE);
	}
}
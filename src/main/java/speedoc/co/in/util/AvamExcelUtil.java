package speedoc.co.in.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.DateValidator;
import org.apache.commons.validator.routines.DoubleValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.apache.commons.validator.routines.LongValidator;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import speedoc.co.in.vo.AvamVo;

public class AvamExcelUtil {

	protected static Logger logger = Logger.getLogger(AvamExcelUtil.class);

	public static XSSFSheet getSheetFromExcel(String filePath)
			throws IOException {

		FileInputStream excelInputStream = new FileInputStream(filePath);
		try {
			XSSFWorkbook myWorkBook = new XSSFWorkbook(excelInputStream);
			XSSFSheet sheet = myWorkBook.getSheetAt(0);
			excelInputStream.close();
			return sheet;
		} catch (Exception e) {
			logger.error("getSheetFromExcel ::" + e.getMessage(), e);
			return null;
		}
	}

	public static AvamVo readSheetRowWise(XSSFRow xssfRow) {
		logger.info("Reading ROw Number :: " + xssfRow.getRowNum());
		AvamVo avamVo = new AvamVo();

		int numberOfColumns = xssfRow.getPhysicalNumberOfCells();
		logger.info("numberOfColumns :: " + numberOfColumns);
		for (int i = 0; i < numberOfColumns; i++) {
			String cellValue = null;
			XSSFCell cell = xssfRow.getCell(i);
			cell.setCellFormula(null);

			switch (i) {

			case 0:
				// uninor number or mobile number
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())
						&& longValidater(cell.getStringCellValue())
						&& cell.getStringCellValue().length() == Utils.mobileLength) {
					avamVo.setMobileNumber(cell.getStringCellValue());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell :: uninor number:: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;

			case 1:
				// CAF number validation
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())
						&& longValidater(cell.getStringCellValue())
						&& cell.getStringCellValue().length() == Utils.cafLength) {
					avamVo.setCafNumber(cell.getStringCellValue());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell ::  CAF number :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;

			case 2:
				// unique id barcode
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if ((nullValidater(cell.getStringCellValue()) && cell
						.getStringCellValue().length() == 9)
						|| (nullValidater(cell.getStringCellValue()) && cell
								.getStringCellValue().length() == 12)
						|| (nullValidater(cell.getStringCellValue()) && cell
								.getStringCellValue().length() == 13)) {
					avamVo.setSpeedocId(cell.getStringCellValue());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell ::  SpeedocId :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;

			case 3:
				// distributor Name
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setDistributorName(cell.getStringCellValue());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell ::  distributor Name :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;

			case 4:
				// Distributor code
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setDtrCode(cell.getStringCellValue().trim());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell :: Distributor code :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;
			case 5:
				// cao Name

				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setCaoName(cell.getStringCellValue());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell ::  Cao Name :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;
			case 6:
				// Cao code
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setCaoCode(cell.getStringCellValue().trim());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell :: Cao code :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;
			case 7:
				// activation_date
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				if (DateUtil.isCellDateFormatted(cell)) {
					try {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"dd/MM/yyyy");
						cellValue = sdf.format(cell.getDateCellValue());
						if (nullValidater(cellValue)) {
							Date dateStr = sdf.parse(cellValue);
							Timestamp timestamp = new Timestamp(
									dateStr.getTime());
							avamVo.setActivationDate(timestamp);
						} else {
							logger.error("Row Added To Error File :: Fail at cell :: Activation Date :: "
									+ cell);
							avamVo = null;
							return avamVo;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;
			case 8:
				// pos code
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setPosCode(cell.getStringCellValue().trim());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell :: pos code :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;
			case 9:
				// pos name
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setPosName(cell.getStringCellValue().trim());
				} else {
					logger.error("Row Added To ERROR File :: Fail at  cell :: pos name :: "
							+ cell);
					avamVo = null;
					return avamVo;
				}
				break;

			case 10:
				// cafTrackingStatus
				cell.setCellType(Cell.CELL_TYPE_STRING);
				if (nullValidater(cell.getStringCellValue())) {
					avamVo.setCafTrackingStatus(cell.getStringCellValue()
							.trim());
				}else {
					logger.error("Row Added To ERROR File :: Fail at  cell ::  cafTrackingStatus :: "
							+ cell);
					avamVo = null;
					return avamVo;					
				}
			default:
				break;
			}
		}
		return avamVo;
	}

	public static String createErrorFile(String errorFilePath, XSSFRow headerRow) {
		logger.info("creating error file....");
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet errorSheet = workbook.createSheet("error records");

			FileOutputStream fileOut = new FileOutputStream(errorFilePath);
			XSSFRow errorExcelheaderRow = errorSheet.createRow(0);

			for (int i = 0; i < headerRow.getPhysicalNumberOfCells(); i++) {
				XSSFCell cell = errorExcelheaderRow.createCell(i);
				cell.setCellValue(headerRow.getCell(i).toString());
			}
			workbook.write(fileOut);
			fileOut.close();
			logger.info("creating error file and adding header end....");
			return errorFilePath;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	public static void addRoWToErrorFile(XSSFRow avamRow,
			String excelErrorFilePath) throws Exception {

		FileInputStream file = null;

		XSSFWorkbook workbook = null;
		try {
			file = new FileInputStream(new File(excelErrorFilePath));
			workbook = new XSSFWorkbook(file);

			XSSFSheet errorSheet = workbook.getSheetAt(0);
			int totalRowsInErrorFile = errorSheet.getPhysicalNumberOfRows();
			XSSFRow errorExcelheaderRow = errorSheet
					.createRow(totalRowsInErrorFile);

			for (int i = 0; i < avamRow.getPhysicalNumberOfCells(); i++) {
				XSSFCell cell = errorExcelheaderRow.createCell(i);
				cell.setCellFormula(null);
				cell.setCellValue(avamRow.getCell(i).toString());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			file.close();
			FileOutputStream outFile = new FileOutputStream(new File(
					excelErrorFilePath));
			workbook.write(outFile);
			outFile.flush();
			outFile.close();
		}
	}

	public static boolean integerValidater(String cellValue) {
		IntegerValidator validator = IntegerValidator.getInstance();
		boolean isValid = false;
		if (validator.validate(cellValue) == null) {
			isValid = false;
		} else {
			isValid = true;
		}
		return isValid;
	}

	public static boolean doubleValidater(String cellValue) {
		DoubleValidator validator = DoubleValidator.getInstance();
		boolean isValid = false;
		if (validator.validate(cellValue) == null) {
			isValid = false;
		} else {
			isValid = true;
		}
		return isValid;
	}

	public static boolean longValidater(String cellValue) {
		LongValidator validator = LongValidator.getInstance();
		boolean isValid = false;
		if (validator.validate(cellValue) == null) {
			isValid = false;
		} else {
			isValid = true;
		}
		return isValid;
	}

	public static boolean dateValidater(String cellValue) {
		logger.info("cellValue :: " + cellValue);
		DateValidator validator = DateValidator.getInstance();
		boolean isValid = false;
		if (validator.validate(cellValue, "dd/MMM/yyyy") == null) {
			isValid = false;
		} else {
			isValid = true;
		}
		return isValid;
	}

	public static boolean nullValidater(String cellValue) {
		boolean isValid = false;
		if (StringUtils.trimToNull(cellValue) == null) {
			isValid = false;
		} else {
			isValid = true;
		}
		return isValid;
	}

	public static Integer getNumOfDataRowsWithoutHeader(String pn33ExcelPath) {
		int physicalNumberOfRows = 0;
		try {
			XSSFSheet sheet0 = getSheetFromExcel(pn33ExcelPath);
			physicalNumberOfRows = sheet0.getPhysicalNumberOfRows() - 1;
		} catch (Exception e) {
			logger.error(e);
		}
		return physicalNumberOfRows;
	}

	public static Timestamp parseStringToTimestamp(String date)
			throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date date1 = sdf.parse(date.trim() + " 00:00:00");
		Timestamp timestamp = new Timestamp(date1.getTime());
		return timestamp;
	}

	public static Timestamp parseStringDateToTimestamp(String date)
			throws Exception {
		if (date.length() < 11) {
			date = date.trim() + " 00:00:00";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date date1 = sdf.parse(date.trim());
		Timestamp timestamp = new Timestamp(date1.getTime());
		return timestamp;
	}

	public int diffInDays(Timestamp date1, Timestamp date2) {
		int diffDateInDays = (int) ((date1.getTime() - date2.getTime()) / (1000 * 60 * 60 * 24));

		diffDateInDays = Math.abs(diffDateInDays);
		diffDateInDays = diffDateInDays + 1;
		logger.debug("diffDateInDays :: " + diffDateInDays);
		return diffDateInDays;
	}

	public static String getFormatedCurrentDateTimeAsString() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public static String getFormatedCurrentDateTime() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

}

package speedoc.co.in.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;


public class ImageUtils {
	
	protected static Logger log = Logger.getLogger(ImageUtils.class
			.getName());
	
	// write image 
	public static void writeImage(BufferedImage img, String fileLocation,
			String extension) {
		ImageWriter imageWriter=null;
		try {
			BufferedImage bi = img;
			File outputfile = new File(fileLocation);
			java.util.Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName("jpg");
			if(it.hasNext()){
				imageWriter=it.next();
			}
			ImageOutputStream imageOutputStream=ImageIO.createImageOutputStream(outputfile);
			imageWriter.setOutput(imageOutputStream);
			ImageWriteParam param=imageWriter.getDefaultWriteParam();
			imageWriter.write(null, new IIOImage(bi, null, null), param);
			imageOutputStream.flush();
			imageWriter.dispose();
			imageOutputStream.close();
		} catch (IOException e) {
			log.error("DataentryUtil :: writeImage ::  " + e.getMessage());
		}
	}
	
	// read image file
	public static BufferedImage readImage(String fileLocation) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(fileLocation));
		} catch (IOException e) {
			log.error("DataentryUtil :: readImage ::  " + e.getMessage());
		}
		return img;
	}
	
	// not use
	public static BufferedImage rotateMyImage(BufferedImage img, double angle) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(w, h, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.rotate(Math.toRadians(angle), w/2 , h/2);

		g.drawImage(img, null, 0, 0);
		return dimg;
	}
	
	//its using for rotating images
	public static BufferedImage rotateImage(BufferedImage img, double angle) {
        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = img.getWidth();
        int h = img.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotatedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g2d = rotatedImage.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2, (newHeight - h) / 2);

        int x = w / 2;
        int y = h / 2;

        at.rotate(Math.toRadians(angle), x, y);
        g2d.setTransform(at);
        g2d.drawImage(img, null, 0, 0);
        g2d.drawRect(0, 0, newWidth - 1, newHeight - 1);
        g2d.dispose();	
        return rotatedImage;
    }	
}

package speedoc.co.in.util;

public enum CafAttributeEnum {
	
	 	MOBILE_NUMBER("mobile_number"),
	    SIM_NUMBER("sim_number"),
	    UNKNOWN_NUMBER("unknown_number"),
	    SPEEDOC_ID("speedoc_id"),
	    CAF_NUMBER("caf_number"),
	    FILE_ID("file_id"), 
	    CREATED_BY("created_by"),
	    OU_ID("ou_id"),
	    AUTHOR("www.speedoc.co.in, www.speedoc.in"),
	    CAF_REPORT("report"),
	    TITLE("Uninor Datamatic-CAF"),
	    CAF_CONNECTION_TYPE("connection_type");
	 	
	  
	 	private final String stringText;

	    private CafAttributeEnum(String stringText) {
	        this.stringText = stringText;
	    }

	    public static CafAttributeEnum fromString(String text) {
	        if (text != null) {
	            for (CafAttributeEnum b : CafAttributeEnum.values()) {
	                if (text.equalsIgnoreCase(b.stringText)) {
	                    return b;
	                }
	            }
	        }
	        return null;
	    }

	    @Override
	    public String toString() {
	        return this.stringText;
	    }

}

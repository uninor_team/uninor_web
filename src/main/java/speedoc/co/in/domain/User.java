package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "user_tbl")
public class User {

	public User(){
		super();
		id = null;
	}
		
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_tbl_seq")
	@SequenceGenerator(name = "user_tbl_seq", sequenceName = "user_tbl_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "middle_name")
	private String middleName ;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "pwd")
	private String pwd;
	
	@Column(name = "mobile_number")
	private String mobileNumber ;
	
	@Column(name = "address")
	private String address ;
	
	@Column(name = "enabled")
	private Boolean enabled;
	
	@Column(name = "account_expired")
	private Boolean accountExpired ;
	
	@Column(name = "account_locked")
	private Boolean accountLocked ;
	
	@Column(name = "password_changed_date",updatable = false, insertable = false)
	private Timestamp passwordChangedDate ;
	
	@Column(name = "username", unique = true, updatable = false)
	private String username ;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "description")
	private String description;
		
	@Column(name = "created_date", updatable = false, insertable = false)
	private Timestamp createdDate ;
	
	@Column(name = "modified_date", updatable = false, insertable = false)
	private Timestamp modifiedDate;
	
	@Column(name = "active")
	private Integer active;
	
	@Column(name = "created_by")
	private Long createdBy ;
	
	@Column(name="email_address")
	private String emailAddress;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}


	public Timestamp getPasswordChangedDate() {
		return passwordChangedDate;
	}

	public void setPasswordChangedDate(Timestamp passwordChangedDate) {
		this.passwordChangedDate = passwordChangedDate;
	}

	public Boolean getAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public Boolean getAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(Boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", pwd=" + pwd
				+ ", mobileNumber=" + mobileNumber + ", address=" + address
				+ ", enabled=" + enabled + ", accountExpired=" + accountExpired
				+ ", accountLocked=" + accountLocked + ", passwordChangedDate="
				+ passwordChangedDate + ", username=" + username + ", city="
				+ city + ", state=" + state + ", status=" + status
				+ ", description=" + description + ", createdDate="
				+ createdDate + ", modifiedDate=" + modifiedDate + ", active="
				+ active + ", createdBy=" + createdBy + ", emailAddress="
				+ emailAddress + "]";
	}

	
}





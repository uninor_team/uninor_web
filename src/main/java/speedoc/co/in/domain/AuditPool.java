package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "audit_pool_tbl")
public class AuditPool {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "audit_pool_tbl_seq")
	@SequenceGenerator(name = "audit_pool_tbl_seq" ,sequenceName ="audit_pool_tbl_seq" ,allocationSize = 1 )
	private Long id;

	@Column(name = "caf_tbl_id")
	private Long cafTblId;

	@Column(name = "start_date")
	private Timestamp startDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "AuditPool [id=" + id + ", cafTblId=" + cafTblId
				+ ", startDate=" + startDate + "]";
	}

	
}

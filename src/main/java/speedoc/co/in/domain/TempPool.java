package speedoc.co.in.domain;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "temp_pool_tbl")
public class TempPool {
	
	@Id
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private Long id;
	
	@Column(name="caf_tbl_id")
	private Long cafTblId;
	
	@Column(name="caf_status_id")
	private Long cafStatusId;
	
	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="pool_id")
	private Long poolId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Long getCafStatusId() {
		return cafStatusId;
	}

	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Long getPoolId() {
		return poolId;
	}

	public void setPoolId(Long poolId) {
		this.poolId = poolId;
	}

	@Override
	public String toString() {
		return "TempPool [id=" + id + ", cafTblId=" + cafTblId
				+ ", cafStatusId=" + cafStatusId + ", createdDate="
				+ createdDate + ", poolId=" + poolId + "]";
	}
}

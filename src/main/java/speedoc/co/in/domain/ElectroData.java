package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="electro_data_tbl")
public class ElectroData {
	
	@Id
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private Long id;
	
	@Column(name="voter_id")
	private String voterId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="age")
	private Integer age;
	
	@Column(name="father_name")
	private String fatherName;
	
	@Column(name="state")
	private String state;
	
	@Column(name="district")
	private String district;
	
	@Column(name="polling_station")
	private String pollingStation;
	
	@Column(name="polllingstation_no")
	private String pollingStationNo;
	
	@Column(name="assembly_constituency")
	private String assemblyConstituency;
	
	@Column(name="parlimentry_constituency")
	private String parlimentryConstituency;
	
	@Column(name="created_date", insertable=false, updatable=false)
	private Timestamp createdDate;
	
	@Column(name="electro_data_status")
	private String electroDataStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVoterId() {
		return voterId;
	}

	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPollingStation() {
		return pollingStation;
	}

	public void setPollingStation(String pollingStation) {
		this.pollingStation = pollingStation;
	}

	public String getAssemblyConstituency() {
		return assemblyConstituency;
	}

	public void setAssemblyConstituency(String assemblyConstituency) {
		this.assemblyConstituency = assemblyConstituency;
	}

	public String getParlimentryConstituency() {
		return parlimentryConstituency;
	}

	public void setParlimentryConstituency(String parlimentryConstituency) {
		this.parlimentryConstituency = parlimentryConstituency;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getPollingStationNo() {
		return pollingStationNo;
	}

	public void setPollingStationNo(String pollingStationNo) {
		this.pollingStationNo = pollingStationNo;
	}

	public String getElectroDataStatus() {
		return electroDataStatus;
	}

	public void setElectroDataStatus(String electroDataStatus) {
		this.electroDataStatus = electroDataStatus;
	}

	@Override
	public String toString() {
		return "ElectroData [id=" + id + ", voterId=" + voterId + ", name="
				+ name + ", age=" + age + ", fatherName=" + fatherName
				+ ", state=" + state + ", district=" + district
				+ ", pollingStation=" + pollingStation + ", pollingStationNo="
				+ pollingStationNo + ", assemblyConstituency="
				+ assemblyConstituency + ", parlimentryConstituency="
				+ parlimentryConstituency + ", createdDate=" + createdDate
				+ ", electroDataStatus=" + electroDataStatus + "]";
	}
}

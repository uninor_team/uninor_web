package speedoc.co.in.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "slc_data_tbl")
public class SlcData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5303763939215168675L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "slc_data_tbl_seq")
	@SequenceGenerator(name ="slc_data_tbl_seq" , sequenceName = "slc_data_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="speedoc_id")
	private String speedocId;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	
	@Column(name="caf_number")
	private String cafNumber;
	
	@Column(name="slc_import_id")
	private Long slcImportId;
	
	@Column(name = "created_date", insertable = false, updatable = false)
	private Timestamp createdDate;
	
	@Column(name="created_by")
	private Long createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpeedocId() {
		return speedocId;
	}

	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	public Long getSlcImportId() {
		return slcImportId;
	}

	public void setSlcImportId(Long slcImportId) {
		this.slcImportId = slcImportId;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	
}

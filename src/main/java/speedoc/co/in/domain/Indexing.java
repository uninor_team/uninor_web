package speedoc.co.in.domain;
// Generated Sep 14, 2013 10:33:04 AM by Hibernate Tools 3.4.0.CR1

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * IndexingTbl generated by hbm2java
 */
@Entity
@Table(name="indexing_tbl")
public class Indexing implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1808335787761812699L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "indexing_tbl_seq")
	@SequenceGenerator(name ="indexing_tbl_seq" , sequenceName = "indexing_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="caf_tbl_id")
	private Long cafTblId;
	
	@Column(name="caf_number")
	private String cafNumber;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	
	@Column(name="caf_tracking_status")
	private String cafTrackingStatus;
	
	@Column(name="imsi_number")
	private String imsiNumber;
	
	@Column(name="upc_code")
	private String upcCode;
	
	@Column(name="previous_service_provider_name")
	private String previousServiceProviderName;
	
	@Column(name="status_of_subscriber")
	private String statusOfSubscriber;
	
	@Column(name="existing_number")
	private String existingNumber;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="middle_name")
	private String middleName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="father_first_name")
	private String fatherFirstName;
	
	@Column(name="father_middle_name")
	private String fatherMiddleName;
	
	@Column(name="father_last_name")
	private String fatherLastName;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="dob")
	private Timestamp dob;
	
	@Column(name="nationality")
	private String nationality;
	
	@Column(name="pan_gir_number")
	private String panGirNumber;
	
	@Column(name="uid_number")
	private String uidNumber;
	
	@Column(name="local_address")
	private String localAddress;
	
	@Column(name="local_address_landmark")
	private String localAddressLandmark;

	@Column(name="local_city")
	private String localCity;
	
	@Column(name="local_district")
	private String localDistrict;
	
	@Column(name="local_state")
	private String localState;
	
	@Column(name="local_pincode")
	private String localPincode;
	
	@Column(name="permanent_address")
	private String permanentAddress;
	
	@Column(name="permanent_address_landmark")
	private String permanentAddressLandmark;
	
	@Column(name="permanent_city")
	private String permanentCity;
	
	@Column(name="permanent_district")
	private String permanentDistrict;
	
	@Column(name="permanent_state")
	private String permanentState;
	
	@Column(name="permanent_pincode")
	private String permanentPincode;

	@Column(name="existing_connection")
	private String existingConnection;
	
	@Column(name="tarrif_plan_applied")
	private String tarrifPlanApplied;
	
	@Column(name="email_address")
	private String emailAddress;
	
	@Column(name="value_added_service_applied")
	private String valueAddedServiceApplied;
	
	@Column(name="alternate_mobile_number")
	private String alternateMobileNumber;
	
	@Column(name="subscriber_profession")
	private String subscriberProfession;
	
	@Column(name="poi")
	private String poi;
	
	@Column(name="poiref")
	private String poiref;
	
	@Column(name="poi_issue_date")
	private Timestamp poiIssueDate;
	
	@Column(name="poi_issue_place")
	private String poiIssuePlace;
	
	@Column(name="poi_issuing_authority")
	private String poiIssuingAuthority;
	
	@Column(name="poa")
	private String poa;
	
	@Column(name="poaref")
	private String poaref;
	
	@Column(name="poa_issue_date")
	private Timestamp poaIssueDate;
		
	@Column(name="poa_issue_place")
	private String poaIssuePlace;
	
	@Column(name="poa_issuing_authority")
	private String poaIssuingAuthority;
	
	@Column(name="passport_number")
	private String passportNumber;
	
	@Column(name="visa_valid_till")
	private Timestamp visaValidTill;

	@Column(name="type_of_visa")
	private String typeOfVisa;
	
	@Column(name="visa_number")
	private String visaNumber;
	
	@Column(name="local_reference_name")
	private String localReferenceName;
	
	@Column(name="local_reference_address")
	private String localReferenceAddress;
	
	@Column(name="local_reference_city")
	private String localReferenceCity;
	
	@Column(name="local_reference_pincode")
	private String localReferencePincode;
	
	@Column(name="local_reference_contact_number")
	private String localReferenceContactNumber;
	
	@Column(name="calling_party_number")
	private String callingPartyNumber;
	
	@Column(name="friends_and_family")
	private String friendsAndFamily;
	
	@Column(name="gprs")
	private String gprs;
	
	@Column(name="wap")
	private String wap;
	
	@Column(name="mms")
	private String mms;
	
	@Column(name="caller_back_ring_tone")
	private String callerBackRingTone;
	
	@Column(name="status")
	private Integer status;
	
	@Column(name="search_count")
	private Integer searchCount;
	
	@Column(name="created_date", insertable=false, updatable=false)
	private Timestamp createdDate;
	
	@Column(name="electro_chitti_status")
	private Integer electroChittiStatus;
	
//	@Column(name="electro_id")
//	private Long electroId;

	public Indexing() {
	}

	public Indexing(Long id, Long cafTblId, Timestamp createdDate) {
		this.id = id;
		this.cafTblId = cafTblId;
		this.createdDate = createdDate;
	}

	public Indexing(Long id, Long cafTblId, 
			String cafNumber, String mobileNumber, String cafTrackingStatus,
			String imsiNumber, String upcCode,
			String previousServiceProviderName, String statusOfSubscriber,
			String existingNumber, String firstName, String middleName,
			String lastName, String fatherFirstName, String fatherMiddleName,
			String fatherLastName, String gender, Timestamp dob, String nationality,
			String panGirNumber, String uidNumber, String localAddress,
			String localAddressLandmark, String localCity,
			String localDistrict, String localState, String localPincode,
			String permanentAddress, String permanentAddressLandmark,
			String permanentCity, String permanentDistrict,
			String permanentState, String permanentPincode,
			String existingConnection, String tarrifPlanApplied,
			String emailAddress, String valueAddedServiceApplied,
			String alternateMobileNumber, String subscriberProfession,
			String poi, String poiref, Timestamp poiIssueDate, String poiIssuePlace,
			String poiIssuingAuthority, String poa, String poaref,
			Timestamp poaIssueDate, String poaIssuePlace,
			String poaIssuingAuthority, String passportNumber,
			Timestamp visaValidTill, String typeOfVisa, String visaNumber,
			String localReferenceName, String localReferenceAddress,
			String localReferenceCity, String localReferencePincode,
			String localReferenceContactNumber, String callingPartyNumber,
			String friendsAndFamily, String gprs, String wap, String mms,
			String callerBackRingTone, Integer status, Integer searchCount,
			Timestamp createdDate, Integer electroChittiStatus) {
		this.id = id;
		this.cafTblId = cafTblId;
		this.cafNumber = cafNumber;
		this.mobileNumber = mobileNumber;
		this.cafTrackingStatus = cafTrackingStatus;
		this.imsiNumber = imsiNumber;
		this.upcCode = upcCode;
		this.previousServiceProviderName = previousServiceProviderName;
		this.statusOfSubscriber = statusOfSubscriber;
		this.existingNumber = existingNumber;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.fatherFirstName = fatherFirstName;
		this.fatherMiddleName = fatherMiddleName;
		this.fatherLastName = fatherLastName;
		this.gender = gender;
		this.dob = dob;
		this.nationality = nationality;
		this.panGirNumber = panGirNumber;
		this.uidNumber = uidNumber;
		this.localAddress = localAddress;
		this.localAddressLandmark = localAddressLandmark;
		this.localCity = localCity;
		this.localDistrict = localDistrict;
		this.localState = localState;
		this.localPincode = localPincode;
		this.permanentAddress = permanentAddress;
		this.permanentAddressLandmark = permanentAddressLandmark;
		this.permanentCity = permanentCity;
		this.permanentDistrict = permanentDistrict;
		this.permanentState = permanentState;
		this.permanentPincode = permanentPincode;
		this.existingConnection = existingConnection;
		this.tarrifPlanApplied = tarrifPlanApplied;
		this.emailAddress = emailAddress;
		this.valueAddedServiceApplied = valueAddedServiceApplied;
		this.alternateMobileNumber = alternateMobileNumber;
		this.subscriberProfession = subscriberProfession;
		this.poi = poi;
		this.poiref = poiref;
		this.poiIssueDate = poiIssueDate;
		this.poiIssuePlace = poiIssuePlace;
		this.poiIssuingAuthority = poiIssuingAuthority;
		this.poa = poa;
		this.poaref = poaref;
		this.poaIssueDate = poaIssueDate;
		this.poaIssuePlace = poaIssuePlace;
		this.poaIssuingAuthority = poaIssuingAuthority;
		this.passportNumber = passportNumber;
		this.visaValidTill = visaValidTill;
		this.typeOfVisa = typeOfVisa;
		this.visaNumber = visaNumber;
		this.localReferenceName = localReferenceName;
		this.localReferenceAddress = localReferenceAddress;
		this.localReferenceCity = localReferenceCity;
		this.localReferencePincode = localReferencePincode;
		this.localReferenceContactNumber = localReferenceContactNumber;
		this.callingPartyNumber = callingPartyNumber;
		this.friendsAndFamily = friendsAndFamily;
		this.gprs = gprs;
		this.wap = wap;
		this.mms = mms;
		this.callerBackRingTone = callerBackRingTone;
		this.status = status;
		this.searchCount = searchCount;
		this.createdDate = createdDate;
		this.electroChittiStatus=electroChittiStatus;
		
		}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return this.cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public String getCafNumber() {
		return this.cafNumber;
	}

	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCafTrackingStatus() {
		return this.cafTrackingStatus;
	}

	public void setCafTrackingStatus(String cafTrackingStatus) {
		this.cafTrackingStatus = cafTrackingStatus;
	}

	public String getImsiNumber() {
		return this.imsiNumber;
	}

	public void setImsiNumber(String imsiNumber) {
		this.imsiNumber = imsiNumber;
	}

	public String getUpcCode() {
		return this.upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public String getPreviousServiceProviderName() {
		return this.previousServiceProviderName;
	}

	public void setPreviousServiceProviderName(
			String previousServiceProviderName) {
		this.previousServiceProviderName = previousServiceProviderName;
	}

	public String getStatusOfSubscriber() {
		return this.statusOfSubscriber;
	}

	public void setStatusOfSubscriber(String statusOfSubscriber) {
		this.statusOfSubscriber = statusOfSubscriber;
	}

	public String getExistingNumber() {
		return this.existingNumber;
	}

	public void setExistingNumber(String existingNumber) {
		this.existingNumber = existingNumber;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherFirstName() {
		return this.fatherFirstName;
	}

	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}

	public String getFatherMiddleName() {
		return this.fatherMiddleName;
	}

	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}

	public String getFatherLastName() {
		return this.fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Timestamp getDob() {
		return this.dob;
	}

	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPanGirNumber() {
		return this.panGirNumber;
	}

	public void setPanGirNumber(String panGirNumber) {
		this.panGirNumber = panGirNumber;
	}

	public String getUidNumber() {
		return this.uidNumber;
	}

	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}

	public String getLocalAddress() {
		return this.localAddress;
	}

	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	public String getLocalAddressLandmark() {
		return this.localAddressLandmark;
	}

	public void setLocalAddressLandmark(String localAddressLandmark) {
		this.localAddressLandmark = localAddressLandmark;
	}

	public String getLocalCity() {
		return this.localCity;
	}

	public void setLocalCity(String localCity) {
		this.localCity = localCity;
	}

	public String getLocalDistrict() {
		return this.localDistrict;
	}

	public void setLocalDistrict(String localDistrict) {
		this.localDistrict = localDistrict;
	}

	public String getLocalState() {
		return this.localState;
	}

	public void setLocalState(String localState) {
		this.localState = localState;
	}

	public String getLocalPincode() {
		return this.localPincode;
	}

	public void setLocalPincode(String localPincode) {
		this.localPincode = localPincode;
	}

	public String getPermanentAddress() {
		return this.permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAddressLandmark() {
		return this.permanentAddressLandmark;
	}

	public void setPermanentAddressLandmark(String permanentAddressLandmark) {
		this.permanentAddressLandmark = permanentAddressLandmark;
	}

	public String getPermanentCity() {
		return this.permanentCity;
	}

	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}

	public String getPermanentDistrict() {
		return this.permanentDistrict;
	}

	public void setPermanentDistrict(String permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public String getPermanentState() {
		return this.permanentState;
	}

	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}

	public String getPermanentPincode() {
		return this.permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getExistingConnection() {
		return this.existingConnection;
	}

	public void setExistingConnection(String existingConnection) {
		this.existingConnection = existingConnection;
	}

	public String getTarrifPlanApplied() {
		return this.tarrifPlanApplied;
	}

	public void setTarrifPlanApplied(String tarrifPlanApplied) {
		this.tarrifPlanApplied = tarrifPlanApplied;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getValueAddedServiceApplied() {
		return this.valueAddedServiceApplied;
	}

	public void setValueAddedServiceApplied(String valueAddedServiceApplied) {
		this.valueAddedServiceApplied = valueAddedServiceApplied;
	}

	public String getAlternateMobileNumber() {
		return this.alternateMobileNumber;
	}

	public void setAlternateMobileNumber(String alternateMobileNumber) {
		this.alternateMobileNumber = alternateMobileNumber;
	}

	public String getSubscriberProfession() {
		return this.subscriberProfession;
	}

	public void setSubscriberProfession(String subscriberProfession) {
		this.subscriberProfession = subscriberProfession;
	}

	public String getPoi() {
		return this.poi;
	}

	public void setPoi(String poi) {
		this.poi = poi;
	}

	public String getPoiref() {
		return this.poiref;
	}

	public void setPoiref(String poiref) {
		this.poiref = poiref;
	}

	public Timestamp getPoiIssueDate() {
		return this.poiIssueDate;
	}

	public void setPoiIssueDate(Timestamp poiIssueDate) {
		this.poiIssueDate = poiIssueDate;
	}

	public String getPoiIssuePlace() {
		return this.poiIssuePlace;
	}

	public void setPoiIssuePlace(String poiIssuePlace) {
		this.poiIssuePlace = poiIssuePlace;
	}

	public String getPoiIssuingAuthority() {
		return this.poiIssuingAuthority;
	}

	public void setPoiIssuingAuthority(String poiIssuingAuthority) {
		this.poiIssuingAuthority = poiIssuingAuthority;
	}

	public String getPoa() {
		return this.poa;
	}

	public void setPoa(String poa) {
		this.poa = poa;
	}

	public String getPoaref() {
		return this.poaref;
	}

	public void setPoaref(String poaref) {
		this.poaref = poaref;
	}

	public Timestamp getPoaIssueDate() {
		return this.poaIssueDate;
	}

	public void setPoaIssueDate(Timestamp poaIssueDate) {
		this.poaIssueDate = poaIssueDate;
	}

	public String getPoaIssuePlace() {
		return this.poaIssuePlace;
	}

	public void setPoaIssuePlace(String poaIssuePlace) {
		this.poaIssuePlace = poaIssuePlace;
	}

	public String getPoaIssuingAuthority() {
		return this.poaIssuingAuthority;
	}

	public void setPoaIssuingAuthority(String poaIssuingAuthority) {
		this.poaIssuingAuthority = poaIssuingAuthority;
	}

	public String getPassportNumber() {
		return this.passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Timestamp getVisaValidTill() {
		return this.visaValidTill;
	}

	public void setVisaValidTill(Timestamp visaValidTill) {
		this.visaValidTill = visaValidTill;
	}

	public String getTypeOfVisa() {
		return this.typeOfVisa;
	}

	public void setTypeOfVisa(String typeOfVisa) {
		this.typeOfVisa = typeOfVisa;
	}

	public String getVisaNumber() {
		return this.visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}

	public String getLocalReferenceName() {
		return this.localReferenceName;
	}

	public void setLocalReferenceName(String localReferenceName) {
		this.localReferenceName = localReferenceName;
	}

	public String getLocalReferenceAddress() {
		return this.localReferenceAddress;
	}

	public void setLocalReferenceAddress(String localReferenceAddress) {
		this.localReferenceAddress = localReferenceAddress;
	}

	public String getLocalReferenceCity() {
		return this.localReferenceCity;
	}

	public void setLocalReferenceCity(String localReferenceCity) {
		this.localReferenceCity = localReferenceCity;
	}

	public String getLocalReferencePincode() {
		return this.localReferencePincode;
	}

	public void setLocalReferencePincode(String localReferencePincode) {
		this.localReferencePincode = localReferencePincode;
	}

	public String getLocalReferenceContactNumber() {
		return this.localReferenceContactNumber;
	}

	public void setLocalReferenceContactNumber(
			String localReferenceContactNumber) {
		this.localReferenceContactNumber = localReferenceContactNumber;
	}

	public String getCallingPartyNumber() {
		return this.callingPartyNumber;
	}

	public void setCallingPartyNumber(String callingPartyNumber) {
		this.callingPartyNumber = callingPartyNumber;
	}

	public String getFriendsAndFamily() {
		return this.friendsAndFamily;
	}

	public void setFriendsAndFamily(String friendsAndFamily) {
		this.friendsAndFamily = friendsAndFamily;
	}

	public String getGprs() {
		return this.gprs;
	}

	public void setGprs(String gprs) {
		this.gprs = gprs;
	}

	public String getWap() {
		return this.wap;
	}

	public void setWap(String wap) {
		this.wap = wap;
	}

	public String getMms() {
		return this.mms;
	}

	public void setMms(String mms) {
		this.mms = mms;
	}

	public String getCallerBackRingTone() {
		return this.callerBackRingTone;
	}

	public void setCallerBackRingTone(String callerBackRingTone) {
		this.callerBackRingTone = callerBackRingTone;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSearchCount() {
		return this.searchCount;
	}

	public void setSearchCount(Integer searchCount) {
		this.searchCount = searchCount;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getElectroChittiStatus() {
		return electroChittiStatus;
	}

	public void setElectroChittiStatus(Integer electroChittiStatus) {
		this.electroChittiStatus = electroChittiStatus;
	}

//	public Long getElectroId() {
//		return electroId;
//	}
//
//	public void setElectroId(Long electroId) {
//		this.electroId = electroId;
//	}
	
		
}

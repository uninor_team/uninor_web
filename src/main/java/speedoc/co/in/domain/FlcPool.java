package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "flc_pool_tbl")
public class FlcPool {

	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "flc_pool_tbl_seq")
//	@SequenceGenerator(name = "flc_pool_tbl_seq" ,sequenceName ="flc_pool_tbl_seq" ,allocationSize = 1 )
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private Long id;

	@Column(name = "caf_tbl_id")
	private Long cafTblId;

	@Column(name = "start_date")
	private Timestamp startDate;

	@Column(name = "end_date")
	private Timestamp endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "FlcPool [id=" + id + ", cafTblId=" + cafTblId + ", startDate="
				+ startDate + ", endDate=" + endDate + "]";
	}
	
}

package speedoc.co.in.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="pos_cpos_tbl")
public class PosCpos {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pos_cpos_tbl_seq")
	@SequenceGenerator(name ="pos_cpos_tbl_seq" , sequenceName = "pos_cpos_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="speedoc_id")
	private String speedocId;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	
	@Column(name="caf_number")
	private String cafNumber;
	
	@Column(name="rtr_code")
	private String rtrCode;
	
	@Column(name="rtr_name")
	private String rtrName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpeedocId() {
		return speedocId;
	}

	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	public String getRtrCode() {
		return rtrCode;
	}

	public void setRtrCode(String rtrCode) {
		this.rtrCode = rtrCode;
	}

	public String getRtrName() {
		return rtrName;
	}

	public void setRtrName(String rtrName) {
		this.rtrName = rtrName;
	}

	@Override
	public String toString() {
		return "PosCpos [id=" + id + ", speedocId=" + speedocId
				+ ", mobileNumber=" + mobileNumber + ", cafNumber=" + cafNumber
				+ ", rtrCode=" + rtrCode + ", rtrName=" + rtrName + "]";
	}
}

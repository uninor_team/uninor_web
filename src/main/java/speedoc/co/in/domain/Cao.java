package speedoc.co.in.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="cao_tbl")
public class Cao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1645661263860774621L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cao_tbl_seq")
	@SequenceGenerator(name ="cao_tbl_seq" , sequenceName = "cao_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="cao_name")
	private String caoName;
	
	@Column(name="cao_code")
	private String caoCode;
	
	@Column(name="image_name")
	private String imageName;
	
	@Column(name="status")
	private int status;
	
	public Cao() {
		
	}
	
	public Cao(Long id, String caoName, String caoCode,
			String imageName,int status) {
		super();
		this.id = id;
		this.caoName = caoName;
		this.caoCode = caoCode;
		this.imageName = imageName;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaoName() {
		return caoName;
	}

	public void setCaoName(String caoName) {
		this.caoName = caoName;
	}

	public String getCaoCode() {
		return caoCode;
	}

	public void setCaoCode(String caoCode) {
		this.caoCode = caoCode;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
}

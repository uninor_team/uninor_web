package speedoc.co.in.domain;
// Generated Sep 14, 2013 10:33:04 AM by Hibernate Tools 3.4.0.CR1

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="caf_status_tbl")
public class CafStatus implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4889606782966310119L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "caf_status_seq")
	@SequenceGenerator(name ="caf_status_seq" , sequenceName = "caf_status_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="caf_tbl_id")
	private Long cafTblId;
	
	@Column(name="status_id")
	private Integer statusId;
	
	@Column(name="created_by")
	private Long createdBy;
	
	@Column(name = "created_date", insertable = false, updatable = false)
	private Timestamp createdDate;

	@Column(name="end_date")
	private Timestamp endDate;
	
	@Column(name="latest_ind")
	private Boolean latestInd;
	
	@Column(name="currently_accessing_user_id")
    private String currentlyAccessingUserId;

	public CafStatus() {
	}

	public CafStatus(Long id, Long cafTblId, Timestamp createdDate) {
		this.id = id;
		this.cafTblId = cafTblId;
		this.createdDate = createdDate;
	}
	public CafStatus(Long id, Long cafTblId, 
			Integer statusId, Long createdBy, Timestamp createdDate,
			Timestamp endDate, Boolean latestInd) {
		this.id = id;
		this.cafTblId = cafTblId;		
		this.statusId = statusId;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.endDate = endDate;
		this.latestInd = latestInd;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return this.cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Boolean getLatestInd() {
		return this.latestInd;
	}

	public void setLatestInd(Boolean latestInd) {
		this.latestInd = latestInd;
	}

	public String getCurrentlyAccessingUserId() {
		return currentlyAccessingUserId;
	}

	public void setCurrentlyAccessingUserId(String currentlyAccessingUserId) {
		this.currentlyAccessingUserId = currentlyAccessingUserId;
	}

	@Override
	public String toString() {
		return "CafStatus [id=" + id + ", cafTblId=" + cafTblId + ", statusId="
				+ statusId + ", createdBy=" + createdBy + ", createdDate="
				+ createdDate + ", endDate=" + endDate + ", latestInd="
				+ latestInd + ", currentlyAccessingUserId="
				+ currentlyAccessingUserId + "]";
	}
	
	
	
	

}

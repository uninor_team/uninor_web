package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class JasperReportVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7479228659999615216L;
	private Timestamp fromDate;
	private Timestamp toDate;
	private String statusId;
	public Timestamp getFromDate() {
		return fromDate;
	}
	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}
	public Timestamp getToDate() {
		return toDate;
	}
	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	@Override
	public String toString() {
		return "JasperReportVo [fromDate=" + fromDate + ", toDate=" + toDate
				+ ", statusId=" + statusId + "]";
	}
	
	
}

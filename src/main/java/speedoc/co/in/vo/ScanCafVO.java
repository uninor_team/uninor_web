package speedoc.co.in.vo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ScanCafVO {
	
	
	private String fileName;

	private CommonsMultipartFile file;
	
	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}

	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	
	
}

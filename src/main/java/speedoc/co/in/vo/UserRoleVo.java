package speedoc.co.in.vo;



import java.io.Serializable;

//mapping of user_role_tbl
public class UserRoleVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -716048177603211818L;
	private Integer id;
	private Integer roleId;
	private Integer userId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "UserRoleVo [id=" + id + ", roleId=" + roleId + ", userId="
				+ userId + "]";
	}
	

}

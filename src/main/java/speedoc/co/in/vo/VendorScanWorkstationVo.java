package speedoc.co.in.vo;


public class VendorScanWorkstationVo {

	private Long id;
	private String regKey;
	private String macId;
	private Long vendorId;
	private Long userId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegKey() {
		return regKey;
	}
	public void setRegKey(String regKey) {
		this.regKey = regKey;
	}
	public String getMacId() {
		return macId;
	}
	public void setMacId(String macId) {
		this.macId = macId;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "VendorScanWorkstationVo [id=" + id + ", regKey=" + regKey
				+ ", macId=" + macId + ", vendorId=" + vendorId + ", userId="
				+ userId + "]";
	}
	
	
	
}

package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class SlcDoneDataVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long slcFileId;
	private String speedocId;
	private String mobileNumber;
	private String cafNumber;
	private Timestamp createdDate;
	private Long createdBy;
	private Integer status;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSlcFileId() {
		return slcFileId;
	}
	public void setSlcFileId(Long slcFileId) {
		this.slcFileId = slcFileId;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "SlcDoneDataVo [id=" + id + ", slcFileId=" + slcFileId
				+ ", speedocId=" + speedocId + ", mobileNumber=" + mobileNumber
				+ ", cafNumber=" + cafNumber + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", status=" + status + "]";
	}
	
	
}

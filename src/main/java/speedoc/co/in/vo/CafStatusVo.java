package speedoc.co.in.vo;

import java.sql.Timestamp;

public class CafStatusVo {

	private Long id;
	private Long cafTblId;
	private Integer statusId;
	private Long createdBy;
	private Timestamp createdDate;
	private Timestamp endDate;
	private Boolean latestInd;
	private String speedocId;
    private String currentlyAccessingUserId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public Boolean getLatestInd() {
		return latestInd;
	}
	public void setLatestInd(Boolean latestInd) {
		this.latestInd = latestInd;
	}
	
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	
	
	public String getCurrentlyAccessingUserId() {
		return currentlyAccessingUserId;
	}
	public void setCurrentlyAccessingUserId(String currentlyAccessingUserId) {
		this.currentlyAccessingUserId = currentlyAccessingUserId;
	}
	@Override
	public String toString() {
		return "CafStatusVo [id=" + id + ", cafTblId=" + cafTblId
				+ ", statusId=" + statusId + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", endDate=" + endDate
				+ ", latestInd=" + latestInd + ", speedocId=" + speedocId
				+ ", currentlyAccessingUserId=" + currentlyAccessingUserId
				+ "]";
	}
	

	

}

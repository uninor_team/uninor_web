package speedoc.co.in.vo;

public class ScrutinyCaptureVo {

	public int indexId;
	public Long cafTblId;
	public String mobileNumber;
	public int statusId;
	public String rejectReason;
	public Integer allotedToUserSummaryId;
	public Integer scrutinyCount;
	public String remarks;
	
	public int getIndexId() {
		return indexId;
	}
	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}

	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public Integer getAllotedToUserSummaryId() {
		return allotedToUserSummaryId;
	}
	public void setAllotedToUserSummaryId(Integer allotedToUserSummaryId) {
		this.allotedToUserSummaryId = allotedToUserSummaryId;
	}
	public Integer getScrutinyCount() {
		return scrutinyCount;
	}
	public void setScrutinyCount(Integer scrutinyCount) {
		this.scrutinyCount = scrutinyCount;
	}
	@Override
	public String toString() {
		return "ScrutinyCaptureVo [indexId=" + indexId + ", cafTblId="
				+ cafTblId + ", mobileNumber=" + mobileNumber + ", statusId="
				+ statusId + ", rejectReason=" + rejectReason
				+ ", allotedToUserSummaryId=" + allotedToUserSummaryId
				+ ", scrutinyCount=" + scrutinyCount + ", remarks=" + remarks
				+ "]";
	}

	
	
	
	
	

	

	
}

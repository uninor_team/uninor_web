package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class FtpReportVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8291109217476908612L;
	private String speedocId;
	private String mobileNumber;
	private String unknownNumber;
	private String cafNumber;
	private String dtrCode;
	private Timestamp scannedDate;
	private String caoCode;
	private String username;
	private Timestamp createdDate;
	private String fromDate;
	private String toDate;
	private String cafFileLocation;
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getUnknownNumber() {
		return unknownNumber;
	}
	public void setUnknownNumber(String unknownNumber) {
		this.unknownNumber = unknownNumber;
	}
	public String getDtrCode() {
		return dtrCode;
	}
	public void setDtrCode(String dtrCode) {
		this.dtrCode = dtrCode;
	}
	public Timestamp getScannedDate() {
		return scannedDate;
	}
	public void setScannedDate(Timestamp scannedDate) {
		this.scannedDate = scannedDate;
	}
	public String getCaoCode() {
		return caoCode;
	}
	public void setCaoCode(String caoCode) {
		this.caoCode = caoCode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public String getCafFileLocation() {
		return cafFileLocation;
	}
	public void setCafFileLocation(String cafFileLocation) {
		this.cafFileLocation = cafFileLocation;
	}
	@Override
	public String toString() {
		return "FtpReportVo [speedocId=" + speedocId + ", mobileNumber="
				+ mobileNumber + ", unknownNumber=" + unknownNumber
				+ ", cafNumber=" + cafNumber + ", dtrCode=" + dtrCode
				+ ", scannedDate=" + scannedDate + ", caoCode=" + caoCode
				+ ", username=" + username + ", createdDate=" + createdDate
				+ ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", cafFileLocation=" + cafFileLocation + "]";
	}
	
	
}

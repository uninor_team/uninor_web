package speedoc.co.in.vo;

import java.io.Serializable;

public class SearchResultVo implements Serializable{

	private static final long serialVersionUID = -7606213530438423232L;
	private String speedocId;
	private String mobileNumber;
	private String cafNumber;
	private String firstName;
	private String lastName;
	private String statusName;
	private String createdDate;
	private String username;
	private Long cafTblId;
	private Long cafStatusId;
	
	public Long getCafStatusId() {
		return cafStatusId;
	}
	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	
	@Override
	public String toString() {
		return "SearchResultVo [speedocId=" + speedocId + ", mobileNumber="
				+ mobileNumber + ", cafNumber=" + cafNumber + ", firstName="
				+ firstName + ", lastName=" + lastName + ", statusName="
				+ statusName + ", createdDate=" + createdDate + ", username="
				+ username + ", cafTblId=" + cafTblId + ", cafStatusId="
				+ cafStatusId + "]";
	}	
}

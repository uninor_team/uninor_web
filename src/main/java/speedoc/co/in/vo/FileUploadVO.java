package speedoc.co.in.vo;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadVO {

	private MultipartFile files;

	public MultipartFile getFiles() {
		return files;
	}

	public void setFiles(MultipartFile files) {
		this.files = files;
	}

	@Override
	public String toString() {
		return "FileUploadVO [files=" + files + "]";
	}

}

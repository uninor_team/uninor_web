package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class SlcImportVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 424913364809273154L;
	private Long id;
	private String fileName;
	private Integer totalRecord;
	private Integer recordUpdated;
	private Integer duplicateRecord;
	private String status;
	private Timestamp createdDate;
	private Long createdBy;
	private String username;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Integer totalRecord) {
		this.totalRecord = totalRecord;
	}
	public Integer getRecordUpdated() {
		return recordUpdated;
	}
	public void setRecordUpdated(Integer recordUpdated) {
		this.recordUpdated = recordUpdated;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Integer getDuplicateRecord() {
		return duplicateRecord;
	}
	public void setDuplicateRecord(Integer duplicateRecord) {
		this.duplicateRecord = duplicateRecord;
	}
	@Override
	public String toString() {
		return "SlcImportVo [id=" + id + ", fileName=" + fileName
				+ ", totalRecord=" + totalRecord + ", recordUpdated="
				+ recordUpdated + ", duplicateRecord=" + duplicateRecord
				+ ", status=" + status + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", username=" + username + "]";
	}	
}

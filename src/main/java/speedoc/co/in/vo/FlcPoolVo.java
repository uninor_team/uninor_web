package speedoc.co.in.vo;

import java.sql.Timestamp;

public class FlcPoolVo {

	private Long id ;
	
	private Long cafTblId ;
	
	private Timestamp startDate ;
	
	private Timestamp endDate ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "FlcPoolVo [id=" + id + ", cafTblId=" + cafTblId
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
	
}

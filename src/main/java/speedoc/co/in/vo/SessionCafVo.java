package speedoc.co.in.vo;

import java.sql.Timestamp;

public class SessionCafVo {
	private Integer id;
	private String sessionId;
	private Long cafTblId;
	private String mobileNumber;
	private Long userId;
	private Long statusId;
	private Timestamp createdDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "SessionCafVo [id=" + id + ", sessionId=" + sessionId
				+ ", cafTblId=" + cafTblId + ", mobileNumber=" + mobileNumber
				+ ", userId=" + userId + ", statusId=" + statusId
				+ ", createdDate=" + createdDate + "]";
	}
}

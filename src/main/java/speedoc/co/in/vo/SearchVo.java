package speedoc.co.in.vo;

public class SearchVo {
	private String searchTxt;
	private String columnName;
	public String getSearchTxt() {
		return searchTxt;
	}
	public void setSearchTxt(String searchTxt) {
		this.searchTxt = searchTxt;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	@Override
	public String toString() {
		return "SearchVO [searchTxt=" + searchTxt + ", columnName="
				+ columnName + "]";
	}
	
	
}

package speedoc.co.in.vo;

public class RotateVO {
	private String fileName;	
	private Integer pageNo;	
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public String toString() {
		return "RotateVO [fileName=" + fileName + ", pageNo=" + pageNo + "]";
	}	
}

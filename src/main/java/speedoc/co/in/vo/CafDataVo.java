package speedoc.co.in.vo;

import java.sql.Timestamp;


public class CafDataVo {
	private Long id;
	private String speedocId;
	private String fileId;
	private String mobileNumber;
	private String simNumber;
	private String cafNumber;
	private String unknownNumber;
	private String report;
	private String photograph;
	private String mnp;
	private String existingNumber;
	private String connectionType;
	private String foreignNational;
	private String outstation;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private String email;
	private String service;
	private Timestamp dob;
	private String nationality;
	private String fatherFirstName;
	private String fatherMiddleName;
	private String fatherLastName;
	private String localAddress;
	private String localAddressLandmark;
	private String localCity;
	private String localPincode;
	private String localDistrict;
	private String localState;
	private String alternateMobileNumber;
	private String permanentAddress;
	private String permanentAddressLandmark;
	private String permanentCity;
	private String permanentPincode;
	private String permanentDistrict;
	private String permanentState;
	private String panGirNumber;
	private String poi;
	private String poiref;
	private String poa;
	private String poaref;
	private String passportNumber;
	private Timestamp visaValidTill;
	private String typeOfVisa;
	private String visaNumber;
	private String localReferenceName;
	private String localReferenceAddress;
	private String localReferenceCity;
	private String localReferencePincode;
	private String localReferenceContactNumber;
	private String category;
	private Timestamp cafDate;
	private Integer form6061;
	private Integer languageId;
	private Integer cafType;
	private String cafFileLocation;
	private Integer pages;
	private String parentId;
	private Timestamp activationDate;
	private Integer monthlyExpenditure;
	private String existingConnection;
	private String connectionOptingFor;
	private String callerBackRingTone;
	private String friendsAndFamily;
	private String gprs; 
	private String wap;
	private String mms;
	private String coverageStrength;
	private String billingTransparency;
	private String qualityOfCustomerService;
	private String moreRetailOutlets;
	private String attractiveCallRates;
	private String callingPartyNumber;
	private Timestamp poiIssueDate;
	private String poiIssuePlace;
	private String poiIssuingAuthority;
	private Timestamp poaIssueDate;
	private String poaIssuePlace;
	private String poaIssuingAuthority;
	private String upcCode;
	private String previousOperatorCircle;
	private String pointOfSaleCode;
	private String posAddress;
	private String posState;
	private String posCity;
	private String posPinCode;
	private String posCode;
	private String activationOfficerName;
	private String activationOfficerDesignation;
	private String statusOfSubscriber;
	private String valueAddedServiceApplied;
	private String imsiNumber;
	private String areaType;
	private String subscriberProfession;
	private String uidNumber;
	private String tarrifPlanApplied;
	private String previousServiceProviderName;
	private String dtrCode;
	private Long cpId;
	private Timestamp createdDate;
	private Timestamp modifiedDate;
	private Long createdBy;
	private Long modifiedBy;
	private Timestamp scannedDate;
	private String caoCode;
	private Integer onhold;
	private String rtrCode;// wap code
	private String rtrName;// mms code
	private String cafTrackingStatus;
	private String pollingStation;
	private String voterId;
	private String voterName;
	
	
	public String getCafTrackingStatus() {
		return cafTrackingStatus;
	}
	public void setCafTrackingStatus(String caftrackingstatus) {
		this.cafTrackingStatus = caftrackingstatus;
	}
	public Integer getOnhold() {
		return onhold;
	}
	public void setOnhold(Integer onhold) {
		this.onhold = onhold;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocid) {
		this.speedocId = speedocid;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileid) {
		this.fileId = fileid;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobilenumber) {
		this.mobileNumber = mobilenumber;
	}
	public String getSimNumber() {
		return simNumber;
	}
	public void setSimNumber(String simnumber) {
		this.simNumber = simnumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafnumber) {
		this.cafNumber = cafnumber;
	}
	public String getUnknownNumber() {
		return unknownNumber;
	}
	public void setUnknownNumber(String unknownnumber) {
		this.unknownNumber = unknownnumber;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getPhotograph() {
		return photograph;
	}
	public void setPhotograph(String photograph) {
		this.photograph = photograph;
	}
	public String getMnp() {
		return mnp;
	}
	public void setMnp(String mnp) {
		this.mnp = mnp;
	}
	public String getExistingNumber() {
		return existingNumber;
	}
	public void setExistingNumber(String existingnumber) {
		this.existingNumber = existingnumber;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectiontype) {
		this.connectionType = connectiontype;
	}
	public String getForeignNational() {
		return foreignNational;
	}
	public void setForeignNational(String foreignnational) {
		this.foreignNational = foreignnational;
	}
	public String getOutstation() {
		return outstation;
	}
	public void setOutstation(String outstation) {
		this.outstation = outstation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstname) {
		this.firstName = firstname;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middlename) {
		this.middleName = middlename;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastname) {
		this.lastName = lastname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public Timestamp getDob() {
		return dob;
	}
	public void setDob(Timestamp dob) {
		this.dob = dob;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherfirstname) {
		this.fatherFirstName = fatherfirstname;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fathermiddlename) {
		this.fatherMiddleName = fathermiddlename;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherlastname) {
		this.fatherLastName = fatherlastname;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localaddress) {
		this.localAddress = localaddress;
	}
	public String getLocalAddressLandmark() {
		return localAddressLandmark;
	}
	public void setLocalAddressLandmark(String localaddresslandmark) {
		this.localAddressLandmark = localaddresslandmark;
	}
	public String getLocalCity() {
		return localCity;
	}
	public void setLocalCity(String localcity) {
		this.localCity = localcity;
	}
	public String getLocalPincode() {
		return localPincode;
	}
	public void setLocalPincode(String localpincode) {
		this.localPincode = localpincode;
	}
	public String getLocalDistrict() {
		return localDistrict;
	}
	public void setLocalDistrict(String localdistrict) {
		this.localDistrict = localdistrict;
	}
	public String getLocalState() {
		return localState;
	}
	public void setLocalState(String localstate) {
		this.localState = localstate;
	}
	public String getAlternateMobileNumber() {
		return alternateMobileNumber;
	}
	public void setAlternateMobileNumber(String alternatemobilenumber) {
		this.alternateMobileNumber = alternatemobilenumber;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentaddress) {
		this.permanentAddress = permanentaddress;
	}
	public String getPermanentAddressLandmark() {
		return permanentAddressLandmark;
	}
	public void setPermanentAddressLandmark(String permanentaddresslandmark) {
		this.permanentAddressLandmark = permanentaddresslandmark;
	}
	public String getPermanentCity() {
		return permanentCity;
	}
	public void setPermanentCity(String permanentcity) {
		this.permanentCity = permanentcity;
	}
	public String getPermanentPincode() {
		return permanentPincode;
	}
	public void setPermanentPincode(String permanentpincode) {
		this.permanentPincode = permanentpincode;
	}
	public String getPermanentDistrict() {
		return permanentDistrict;
	}
	public void setPermanentDistrict(String permanentdistrict) {
		this.permanentDistrict = permanentdistrict;
	}
	public String getPermanentState() {
		return permanentState;
	}
	public void setPermanentState(String permanentstate) {
		this.permanentState = permanentstate;
	}
	public String getPanGirNumber() {
		return panGirNumber;
	}
	public void setPanGirNumber(String pangirnumber) {
		this.panGirNumber = pangirnumber;
	}
	public String getPoi() {
		return poi;
	}
	public void setPoi(String poi) {
		this.poi = poi;
	}
	public String getPoiref() {
		return poiref;
	}
	public void setPoiref(String poiref) {
		this.poiref = poiref;
	}
	public String getPoa() {
		return poa;
	}
	public void setPoa(String poa) {
		this.poa = poa;
	}
	public String getPoaref() {
		return poaref;
	}
	public void setPoaref(String poaref) {
		this.poaref = poaref;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportnumber) {
		this.passportNumber = passportnumber;
	}
	public Timestamp getVisaValidTill() {
		return visaValidTill;
	}
	public void setVisaValidTill(Timestamp visavalidtill) {
		this.visaValidTill = visavalidtill;
	}
	public String getTypeOfVisa() {
		return typeOfVisa;
	}
	public void setTypeOfVisa(String typeofvisa) {
		this.typeOfVisa = typeofvisa;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visanumber) {
		this.visaNumber = visanumber;
	}
	public String getLocalReferenceName() {
		return localReferenceName;
	}
	public void setLocalReferenceName(String localreferencename) {
		this.localReferenceName = localreferencename;
	}
	public String getLocalReferenceAddress() {
		return localReferenceAddress;
	}
	public void setLocalReferenceAddress(String localreferenceaddress) {
		this.localReferenceAddress = localreferenceaddress;
	}
	public String getLocalReferenceCity() {
		return localReferenceCity;
	}
	public void setLocalReferenceCity(String localreferencecity) {
		this.localReferenceCity = localreferencecity;
	}
	public String getLocalReferencePincode() {
		return localReferencePincode;
	}
	public void setLocalReferencePincode(String localreferencepincode) {
		this.localReferencePincode = localreferencepincode;
	}
	public String getLocalReferenceContactNumber() {
		return localReferenceContactNumber;
	}
	public void setLocalReferenceContactNumber(String localreferencecontactnumber) {
		this.localReferenceContactNumber = localreferencecontactnumber;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Timestamp getCafDate() {
		return cafDate;
	}
	public void setCafDate(Timestamp cafdate) {
		this.cafDate = cafdate;
	}
	public Integer getForm6061() {
		return form6061;
	}
	public void setForm6061(Integer form6061) {
		this.form6061 = form6061;
	}
	public Integer getLanguageId() {
		return languageId;
	}
	public void setLanguageId(Integer languageid) {
		this.languageId = languageid;
	}
	public Integer getCafType() {
		return cafType;
	}
	public void setCafType(Integer caftype) {
		this.cafType = caftype;
	}
	public String getCafFileLocation() {
		return cafFileLocation;
	}
	public void setCafFileLocation(String caffilelocation) {
		this.cafFileLocation = caffilelocation;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentid) {
		this.parentId = parentid;
	}
	public Timestamp getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Timestamp activationdate) {
		this.activationDate = activationdate;
	}
	public Integer getMonthlyExpenditure() {
		return monthlyExpenditure;
	}
	public void setMonthlyExpenditure(Integer monthlyexpenditure) {
		this.monthlyExpenditure = monthlyexpenditure;
	}
	public String getExistingConnection() {
		return existingConnection;
	}
	public void setExistingConnection(String existingconnection) {
		this.existingConnection = existingconnection;
	}
	public String getConnectionOptingFor() {
		return connectionOptingFor;
	}
	public void setConnectionOptingFor(String connectionoptingfor) {
		this.connectionOptingFor = connectionoptingfor;
	}
	public String getCallerBackRingTone() {
		return callerBackRingTone;
	}
	public void setCallerBackRingTone(String callerbackringtone) {
		this.callerBackRingTone = callerbackringtone;
	}
	public String getFriendsAndFamily() {
		return friendsAndFamily;
	}
	public void setFriendsAndFamily(String friendsandfamily) {
		this.friendsAndFamily = friendsandfamily;
	}
	public String getGprs() {
		return gprs;
	}
	public void setGprs(String gprs) {
		this.gprs = gprs;
	}
	public String getWap() {
		return wap;
	}
	public void setWap(String wap) {
		this.wap = wap;
	}
	public String getMms() {
		return mms;
	}
	public void setMms(String mms) {
		this.mms = mms;
	}
	public String getCoverageStrength() {
		return coverageStrength;
	}
	public void setCoverageStrength(String coveragestrength) {
		this.coverageStrength = coveragestrength;
	}
	public String getBillingTransparency() {
		return billingTransparency;
	}
	public void setBillingTransparency(String billingtransparency) {
		this.billingTransparency = billingtransparency;
	}
	public String getQualityOfCustomerService() {
		return qualityOfCustomerService;
	}
	public void setQualityOfCustomerService(String qualityofcustomerservice) {
		this.qualityOfCustomerService = qualityofcustomerservice;
	}
	public String getMoreRetailOutlets() {
		return moreRetailOutlets;
	}
	public void setMoreRetailOutlets(String moreretailoutlets) {
		this.moreRetailOutlets = moreretailoutlets;
	}
	public String getAttractiveCallRates() {
		return attractiveCallRates;
	}
	public void setAttractiveCallRates(String attractivecallrates) {
		this.attractiveCallRates = attractivecallrates;
	}
	public String getCallingPartyNumber() {
		return callingPartyNumber;
	}
	public void setCallingPartyNumber(String callingpartynumber) {
		this.callingPartyNumber = callingpartynumber;
	}
	public Timestamp getPoiIssueDate() {
		return poiIssueDate;
	}
	public void setPoiIssueDate(Timestamp poiissuedate) {
		this.poiIssueDate = poiissuedate;
	}
	public String getPoiIssuePlace() {
		return poiIssuePlace;
	}
	public void setPoiIssuePlace(String poiissueplace) {
		this.poiIssuePlace = poiissueplace;
	}
	public String getPoiIssuingAuthority() {
		return poiIssuingAuthority;
	}
	public void setPoiIssuingAuthority(String poiissuingauthority) {
		this.poiIssuingAuthority = poiissuingauthority;
	}
	public Timestamp getPoaIssueDate() {
		return poaIssueDate;
	}
	public void setPoaIssueDate(Timestamp poaissuedate) {
		this.poaIssueDate = poaissuedate;
	}
	public String getPoaIssuePlace() {
		return poaIssuePlace;
	}
	public void setPoaIssuePlace(String poaissueplace) {
		this.poaIssuePlace = poaissueplace;
	}
	public String getPoaIssuingAuthority() {
		return poaIssuingAuthority;
	}
	public void setPoaIssuingAuthority(String poaissuingauthority) {
		this.poaIssuingAuthority = poaissuingauthority;
	}
	public String getUpcCode() {
		return upcCode;
	}
	public void setUpcCode(String upccode) {
		this.upcCode = upccode;
	}
	public String getPreviousOperatorCircle() {
		return previousOperatorCircle;
	}
	public void setPreviousOperatorCircle(String previousoperatorcircle) {
		this.previousOperatorCircle = previousoperatorcircle;
	}
	public String getPointOfSaleCode() {
		return pointOfSaleCode;
	}
	public void setPointOfSaleCode(String pointofsalecode) {
		this.pointOfSaleCode = pointofsalecode;
	}
	public String getPosAddress() {
		return posAddress;
	}
	public void setPosAddress(String posaddress) {
		this.posAddress = posaddress;
	}
	public String getPosState() {
		return posState;
	}
	public void setPosState(String posstate) {
		this.posState = posstate;
	}
	public String getPosCity() {
		return posCity;
	}
	public void setPosCity(String poscity) {
		this.posCity = posCity;
	}
	public String getPosPinCode() {
		return posPinCode;
	}
	public void setPosPinCode(String pospincode) {
		this.posPinCode = pospincode;
	}
	public String getPosCode() {
		return posCode;
	}
	public void setPosCode(String poscode) {
		this.posCode = poscode;
	}
	public String getActivationOfficerName() {
		return activationOfficerName;
	}
	public void setActivationOfficerName(String activationofficername) {
		this.activationOfficerName = activationofficername;
	}
	public String getActivationOfficerDesignation() {
		return activationOfficerDesignation;
	}
	public void setActivationOfficerDesignation(String activationofficerdesignation) {
		this.activationOfficerDesignation = activationofficerdesignation;
	}
	public String getStatusOfSubscriber() {
		return statusOfSubscriber;
	}
	public void setStatusOfSubscriber(String statusofsubscriber) {
		this.statusOfSubscriber = statusofsubscriber;
	}
	public String getValueAddedServiceApplied() {
		return valueAddedServiceApplied;
	}
	public void setValueAddedServiceApplied(String valueaddedserviceapplied) {
		this.valueAddedServiceApplied = valueaddedserviceapplied;
	}
	public String getImsiNumber() {
		return imsiNumber;
	}
	public void setImsiNumber(String imsinumber) {
		this.imsiNumber = imsinumber;
	}
	public String getAreaType() {
		return areaType;
	}
	public void setAreaType(String areatype) {
		this.areaType = areatype;
	}
	public String getSubscriberProfession() {
		return subscriberProfession;
	}
	public void setSubscriberProfession(String subscriberprofession) {
		this.subscriberProfession = subscriberprofession;
	}
	public String getUidNumber() {
		return uidNumber;
	}
	public void setUidNumber(String uidnumber) {
		this.uidNumber = uidnumber;
	}
	public String getTarrifPlanApplied() {
		return tarrifPlanApplied;
	}
	public void setTarrifPlanApplied(String tarrifplanapplied) {
		this.tarrifPlanApplied = tarrifplanapplied;
	}
	public String getPreviousServiceProviderName() {
		return previousServiceProviderName;
	}
	public void setPreviousServiceProviderName(String previousserviceprovidername) {
		this.previousServiceProviderName = previousserviceprovidername;
	}
	public String getDtrCode() {
		return dtrCode;
	}
	public void setDtrCode(String dtrcode) {
		this.dtrCode = dtrcode;
	}
	public Long getCpId() {
		return cpId;
	}
	public void setCpId(Long cpid) {
		this.cpId = cpid;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createddate) {
		this.createdDate = createddate;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifieddate) {
		this.modifiedDate = modifieddate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdby) {
		this.createdBy = createdby;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedby) {
		this.modifiedBy = modifiedby;
	}
	public Timestamp getScannedDate() {
		return scannedDate;
	}
	public void setScannedDate(Timestamp scanneddate) {
		this.scannedDate = scanneddate;
	}
	
	public String getRtrCode() {
		return rtrCode;
	}
	public void setRtrCode(String rtrcode) {
		this.rtrCode = rtrcode;
	}
	public String getRtrName() {
		return rtrName;
	}
	public void setRtrName(String rtrname) {
		this.rtrName = rtrname;
	}
	public void setCaoCode(String caocode) {
		this.caoCode = caocode;
	}
	public String getCaoCode() {
		return caoCode;
	}
	public String getPollingStation() {
		return pollingStation;
	}
	public void setPollingStation(String pollingstation) {
		this.pollingStation = pollingstation;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterid) {
		this.voterId = voterid;
	}
	public String getVoterName() {
		return voterName;
	}
	public void setVoterName(String votername) {
		this.voterName = votername;
	}
	@Override
	public String toString() {
		return "CafDataVo [id=" + id + ", speedocId=" + speedocId + ", fileId="
				+ fileId + ", mobileNumber=" + mobileNumber + ", simNumber="
				+ simNumber + ", cafNumber=" + cafNumber + ", unknownNumber="
				+ unknownNumber + ", report=" + report + ", photograph="
				+ photograph + ", mnp=" + mnp + ", existingNumber="
				+ existingNumber + ", connectionType=" + connectionType
				+ ", foreignNational=" + foreignNational + ", outstation="
				+ outstation + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", gender=" + gender
				+ ", email=" + email + ", service=" + service + ", dob=" + dob
				+ ", nationality=" + nationality + ", fatherFirstName="
				+ fatherFirstName + ", fatherMiddleName=" + fatherMiddleName
				+ ", fatherLastName=" + fatherLastName + ", localAddress="
				+ localAddress + ", localAddressLandmark="
				+ localAddressLandmark + ", localCity=" + localCity
				+ ", localPincode=" + localPincode + ", localDistrict="
				+ localDistrict + ", localState=" + localState
				+ ", alternateMobileNumber=" + alternateMobileNumber
				+ ", permanentAddress=" + permanentAddress
				+ ", permanentAddressLandmark=" + permanentAddressLandmark
				+ ", permanentCity=" + permanentCity + ", permanentPincode="
				+ permanentPincode + ", permanentDistrict=" + permanentDistrict
				+ ", permanentState=" + permanentState + ", panGirNumber="
				+ panGirNumber + ", poi=" + poi + ", poiref=" + poiref
				+ ", poa=" + poa + ", poaref=" + poaref + ", passportNumber="
				+ passportNumber + ", visaValidTill=" + visaValidTill
				+ ", typeOfVisa=" + typeOfVisa + ", visaNumber=" + visaNumber
				+ ", localReferenceName=" + localReferenceName
				+ ", localReferenceAddress=" + localReferenceAddress
				+ ", localReferenceCity=" + localReferenceCity
				+ ", localReferencePincode=" + localReferencePincode
				+ ", localReferenceContactNumber="
				+ localReferenceContactNumber + ", category=" + category
				+ ", cafDate=" + cafDate + ", form6061=" + form6061
				+ ", languageId=" + languageId + ", cafType=" + cafType
				+ ", cafFileLocation=" + cafFileLocation + ", pages=" + pages
				+ ", parentId=" + parentId + ", activationDate="
				+ activationDate + ", monthlyExpenditure=" + monthlyExpenditure
				+ ", existingConnection=" + existingConnection
				+ ", connectionOptingFor=" + connectionOptingFor
				+ ", callerBackRingTone=" + callerBackRingTone
				+ ", friendsAndFamily=" + friendsAndFamily + ", gprs=" + gprs
				+ ", wap=" + wap + ", mms=" + mms + ", coverageStrength="
				+ coverageStrength + ", billingTransparency="
				+ billingTransparency + ", qualityOfCustomerService="
				+ qualityOfCustomerService + ", moreRetailOutlets="
				+ moreRetailOutlets + ", attractiveCallRates="
				+ attractiveCallRates + ", callingPartyNumber="
				+ callingPartyNumber + ", poiIssueDate=" + poiIssueDate
				+ ", poiIssuePlace=" + poiIssuePlace + ", poiIssuingAuthority="
				+ poiIssuingAuthority + ", poaIssueDate=" + poaIssueDate
				+ ", poaIssuePlace=" + poaIssuePlace + ", poaIssuingAuthority="
				+ poaIssuingAuthority + ", upcCode=" + upcCode
				+ ", previousOperatorCircle=" + previousOperatorCircle
				+ ", pointOfSaleCode=" + pointOfSaleCode + ", posAddress="
				+ posAddress + ", posState=" + posState + ", posCity="
				+ posCity + ", posPinCode=" + posPinCode + ", posCode="
				+ posCode + ", activationOfficerName=" + activationOfficerName
				+ ", activationOfficerDesignation="
				+ activationOfficerDesignation + ", statusOfSubscriber="
				+ statusOfSubscriber + ", valueAddedServiceApplied="
				+ valueAddedServiceApplied + ", imsiNumber=" + imsiNumber
				+ ", areaType=" + areaType + ", subscriberProfession="
				+ subscriberProfession + ", uidNumber=" + uidNumber
				+ ", tarrifPlanApplied=" + tarrifPlanApplied
				+ ", previousServiceProviderName="
				+ previousServiceProviderName + ", dtrCode=" + dtrCode
				+ ", cpId=" + cpId + ", createdDate=" + createdDate
				+ ", modifiedDate=" + modifiedDate + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", scannedDate=" + scannedDate
				+ ", caoCode=" + caoCode + ", onhold=" + onhold + ", rtrCode="
				+ rtrCode + ", rtrName=" + rtrName + ", cafTrackingStatus="
				+ cafTrackingStatus + ", pollingStation=" + pollingStation
				+ ", voterId=" + voterId + ", voterName=" + voterName + "]";
	}
}

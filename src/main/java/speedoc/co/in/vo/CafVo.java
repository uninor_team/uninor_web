package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;


public class CafVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String speedocId;
	private String fileId;
	private String mobileNumber;
	private String simNumber;
	private String cafNumber;
	private String unknownNumber;
	private String report;
	private String photograph;
	private String mnp;
	private String existingNumber;
	private String connectionType;
	private String foreignNational;
	private String outstation;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private String email;
	private String service;
	private Timestamp dob;
	private String nationality;
	private String fatherFirstName;
	private String fatherMiddleName;
	private String fatherLastName;
	private String localAddress;
	private String localAddressLandmark;
	private String localCity;
	private String localPincode;
	private String localDistrict;
	private String localState;
	private String alternateMobileNumber;
	private String permanentAddress;
	private String permanentAddressLandmark;
	private String permanentCity;
	private String permanentPincode;
	private String permanentDistrict;
	private String permanentState;
	private String panGirNumber;
	private String poi;
	private String poiref;
	private String poa;
	private String poaref;
	private String passportNumber;
	private Timestamp visaValidTill;
	private String typeOfVisa;
	private String visaNumber;
	private String localReferenceName;
	private String localReferenceAddress;
	private String localReferenceCity;
	private String localReferencePincode;
	private String localReferenceContactNumber;
	private String category;
	private Timestamp cafDate;
	private Integer form6061;
	private Integer languageId;
	private Integer cafType;
	private String cafFileLocation;
	private Integer pages;
	private String parentId;
	private Timestamp activationDate;
	private Integer monthlyExpenditure;
	private String existingConnection;
	private String connectionOptingFor;
	private String callerBackRingTone;
	private String friendsAndFamily;
	private String gprs; 
	private String wap;
	private String mms;
	private String coverageStrength;
	private String billingTransparency;
	private String qualityOfCustomerService;
	private String moreRetailOutlets;
	private String attractiveCallRates;
	private String callingPartyNumber;
	private Timestamp poiIssueDate;
	private String poiIssuePlace;
	private String poiIssuingAuthority;
	private Timestamp poaIssueDate;
	private String poaIssuePlace;
	private String poaIssuingAuthority;
	private String upcCode;
	private String previousOperatorCircle;
	private String pointOfSaleCode;
	private String posAddress;
	private String posState;
	private String posCity;
	private String posPinCode;
	private String posCode;
	private String activationOfficerName;
	private String activationOfficerDesignation;
	private String statusOfSubscriber;
	private String valueAddedServiceApplied;
	private String imsiNumber;
	private String areaType;
	private String subscriberProfession;
	private String uidNumber;
	private String tarrifPlanApplied;
	private String previousServiceProviderName;
	private String dtrCode;
	private Long cpId;
	private Timestamp createdDate;
	private Timestamp modifiedDate;
	private Long createdBy;
	private Long modifiedBy;
	private Timestamp scannedDate;
	private String caoCode;
	private Integer onhold;
	private String rtrCode;// wap code
	private String rtrName;// mms code
	private String cafTrackingStatus;
	private String pollingStation;
	private String voterId;
	private String voterName;

	public String getCafTrackingStatus() {
		return cafTrackingStatus;
	}
	public void setCafTrackingStatus(String cafTrackingStatus) {
		this.cafTrackingStatus = cafTrackingStatus;
	}
	public Integer getOnhold() {
		return onhold;
	}
	public void setOnhold(Integer onhold) {
		this.onhold = onhold;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSimNumber() {
		return simNumber;
	}
	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public String getUnknownNumber() {
		return unknownNumber;
	}
	public void setUnknownNumber(String unknownNumber) {
		this.unknownNumber = unknownNumber;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getPhotograph() {
		return photograph;
	}
	public void setPhotograph(String photograph) {
		this.photograph = photograph;
	}
	public String getMnp() {
		return mnp;
	}
	public void setMnp(String mnp) {
		this.mnp = mnp;
	}
	public String getExistingNumber() {
		return existingNumber;
	}
	public void setExistingNumber(String existingNumber) {
		this.existingNumber = existingNumber;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	public String getForeignNational() {
		return foreignNational;
	}
	public void setForeignNational(String foreignNational) {
		this.foreignNational = foreignNational;
	}
	public String getOutstation() {
		return outstation;
	}
	public void setOutstation(String outstation) {
		this.outstation = outstation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public Timestamp getDob() {
		return dob;
	}
	public void setDob(Timestamp dob) {
		this.dob = dob;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public String getLocalAddressLandmark() {
		return localAddressLandmark;
	}
	public void setLocalAddressLandmark(String localAddressLandmark) {
		this.localAddressLandmark = localAddressLandmark;
	}
	public String getLocalCity() {
		return localCity;
	}
	public void setLocalCity(String localCity) {
		this.localCity = localCity;
	}
	public String getLocalPincode() {
		return localPincode;
	}
	public void setLocalPincode(String localPincode) {
		this.localPincode = localPincode;
	}
	public String getLocalDistrict() {
		return localDistrict;
	}
	public void setLocalDistrict(String localDistrict) {
		this.localDistrict = localDistrict;
	}
	public String getLocalState() {
		return localState;
	}
	public void setLocalState(String localState) {
		this.localState = localState;
	}
	public String getAlternateMobileNumber() {
		return alternateMobileNumber;
	}
	public void setAlternateMobileNumber(String alternateMobileNumber) {
		this.alternateMobileNumber = alternateMobileNumber;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getPermanentAddressLandmark() {
		return permanentAddressLandmark;
	}
	public void setPermanentAddressLandmark(String permanentAddressLandmark) {
		this.permanentAddressLandmark = permanentAddressLandmark;
	}
	public String getPermanentCity() {
		return permanentCity;
	}
	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}
	public String getPermanentPincode() {
		return permanentPincode;
	}
	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}
	public String getPermanentDistrict() {
		return permanentDistrict;
	}
	public void setPermanentDistrict(String permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}
	public String getPermanentState() {
		return permanentState;
	}
	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}
	public String getPanGirNumber() {
		return panGirNumber;
	}
	public void setPanGirNumber(String panGirNumber) {
		this.panGirNumber = panGirNumber;
	}
	public String getPoi() {
		return poi;
	}
	public void setPoi(String poi) {
		this.poi = poi;
	}
	public String getPoiref() {
		return poiref;
	}
	public void setPoiref(String poiref) {
		this.poiref = poiref;
	}
	public String getPoa() {
		return poa;
	}
	public void setPoa(String poa) {
		this.poa = poa;
	}
	public String getPoaref() {
		return poaref;
	}
	public void setPoaref(String poaref) {
		this.poaref = poaref;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public Timestamp getVisaValidTill() {
		return visaValidTill;
	}
	public void setVisaValidTill(Timestamp visaValidTill) {
		this.visaValidTill = visaValidTill;
	}
	public String getTypeOfVisa() {
		return typeOfVisa;
	}
	public void setTypeOfVisa(String typeOfVisa) {
		this.typeOfVisa = typeOfVisa;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}
	public String getLocalReferenceName() {
		return localReferenceName;
	}
	public void setLocalReferenceName(String localReferenceName) {
		this.localReferenceName = localReferenceName;
	}
	public String getLocalReferenceAddress() {
		return localReferenceAddress;
	}
	public void setLocalReferenceAddress(String localReferenceAddress) {
		this.localReferenceAddress = localReferenceAddress;
	}
	public String getLocalReferenceCity() {
		return localReferenceCity;
	}
	public void setLocalReferenceCity(String localReferenceCity) {
		this.localReferenceCity = localReferenceCity;
	}
	public String getLocalReferencePincode() {
		return localReferencePincode;
	}
	public void setLocalReferencePincode(String localReferencePincode) {
		this.localReferencePincode = localReferencePincode;
	}
	public String getLocalReferenceContactNumber() {
		return localReferenceContactNumber;
	}
	public void setLocalReferenceContactNumber(String localReferenceContactNumber) {
		this.localReferenceContactNumber = localReferenceContactNumber;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Timestamp getCafDate() {
		return cafDate;
	}
	public void setCafDate(Timestamp cafDate) {
		this.cafDate = cafDate;
	}
	public Integer getForm6061() {
		return form6061;
	}
	public void setForm6061(Integer form6061) {
		this.form6061 = form6061;
	}
	public Integer getLanguageId() {
		return languageId;
	}
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	public Integer getCafType() {
		return cafType;
	}
	public void setCafType(Integer cafType) {
		this.cafType = cafType;
	}
	public String getCafFileLocation() {
		return cafFileLocation;
	}
	public void setCafFileLocation(String cafFileLocation) {
		this.cafFileLocation = cafFileLocation;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public Timestamp getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}
	public Integer getMonthlyExpenditure() {
		return monthlyExpenditure;
	}
	public void setMonthlyExpenditure(Integer monthlyExpenditure) {
		this.monthlyExpenditure = monthlyExpenditure;
	}
	public String getExistingConnection() {
		return existingConnection;
	}
	public void setExistingConnection(String existingConnection) {
		this.existingConnection = existingConnection;
	}
	public String getConnectionOptingFor() {
		return connectionOptingFor;
	}
	public void setConnectionOptingFor(String connectionOptingFor) {
		this.connectionOptingFor = connectionOptingFor;
	}
	public String getCallerBackRingTone() {
		return callerBackRingTone;
	}
	public void setCallerBackRingTone(String callerBackRingTone) {
		this.callerBackRingTone = callerBackRingTone;
	}
	public String getFriendsAndFamily() {
		return friendsAndFamily;
	}
	public void setFriendsAndFamily(String friendsAndFamily) {
		this.friendsAndFamily = friendsAndFamily;
	}
	public String getGprs() {
		return gprs;
	}
	public void setGprs(String gprs) {
		this.gprs = gprs;
	}
	public String getWap() {
		return wap;
	}
	public void setWap(String wap) {
		this.wap = wap;
	}
	public String getMms() {
		return mms;
	}
	public void setMms(String mms) {
		this.mms = mms;
	}
	public String getCoverageStrength() {
		return coverageStrength;
	}
	public void setCoverageStrength(String coverageStrength) {
		this.coverageStrength = coverageStrength;
	}
	public String getBillingTransparency() {
		return billingTransparency;
	}
	public void setBillingTransparency(String billingTransparency) {
		this.billingTransparency = billingTransparency;
	}
	public String getQualityOfCustomerService() {
		return qualityOfCustomerService;
	}
	public void setQualityOfCustomerService(String qualityOfCustomerService) {
		this.qualityOfCustomerService = qualityOfCustomerService;
	}
	public String getMoreRetailOutlets() {
		return moreRetailOutlets;
	}
	public void setMoreRetailOutlets(String moreRetailOutlets) {
		this.moreRetailOutlets = moreRetailOutlets;
	}
	public String getAttractiveCallRates() {
		return attractiveCallRates;
	}
	public void setAttractiveCallRates(String attractiveCallRates) {
		this.attractiveCallRates = attractiveCallRates;
	}
	public String getCallingPartyNumber() {
		return callingPartyNumber;
	}
	public void setCallingPartyNumber(String callingPartyNumber) {
		this.callingPartyNumber = callingPartyNumber;
	}
	public Timestamp getPoiIssueDate() {
		return poiIssueDate;
	}
	public void setPoiIssueDate(Timestamp poiIssueDate) {
		this.poiIssueDate = poiIssueDate;
	}
	public String getPoiIssuePlace() {
		return poiIssuePlace;
	}
	public void setPoiIssuePlace(String poiIssuePlace) {
		this.poiIssuePlace = poiIssuePlace;
	}
	public String getPoiIssuingAuthority() {
		return poiIssuingAuthority;
	}
	public void setPoiIssuingAuthority(String poiIssuingAuthority) {
		this.poiIssuingAuthority = poiIssuingAuthority;
	}
	public Timestamp getPoaIssueDate() {
		return poaIssueDate;
	}
	public void setPoaIssueDate(Timestamp poaIssueDate) {
		this.poaIssueDate = poaIssueDate;
	}
	public String getPoaIssuePlace() {
		return poaIssuePlace;
	}
	public void setPoaIssuePlace(String poaIssuePlace) {
		this.poaIssuePlace = poaIssuePlace;
	}
	public String getPoaIssuingAuthority() {
		return poaIssuingAuthority;
	}
	public void setPoaIssuingAuthority(String poaIssuingAuthority) {
		this.poaIssuingAuthority = poaIssuingAuthority;
	}
	public String getUpcCode() {
		return upcCode;
	}
	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}
	public String getPreviousOperatorCircle() {
		return previousOperatorCircle;
	}
	public void setPreviousOperatorCircle(String previousOperatorCircle) {
		this.previousOperatorCircle = previousOperatorCircle;
	}
	public String getPointOfSaleCode() {
		return pointOfSaleCode;
	}
	public void setPointOfSaleCode(String pointOfSaleCode) {
		this.pointOfSaleCode = pointOfSaleCode;
	}
	public String getPosAddress() {
		return posAddress;
	}
	public void setPosAddress(String posAddress) {
		this.posAddress = posAddress;
	}
	public String getPosState() {
		return posState;
	}
	public void setPosState(String posState) {
		this.posState = posState;
	}
	public String getPosCity() {
		return posCity;
	}
	public void setPosCity(String posCity) {
		this.posCity = posCity;
	}
	public String getPosPinCode() {
		return posPinCode;
	}
	public void setPosPinCode(String posPinCode) {
		this.posPinCode = posPinCode;
	}
	public String getPosCode() {
		return posCode;
	}
	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}
	public String getActivationOfficerName() {
		return activationOfficerName;
	}
	public void setActivationOfficerName(String activationOfficerName) {
		this.activationOfficerName = activationOfficerName;
	}
	public String getActivationOfficerDesignation() {
		return activationOfficerDesignation;
	}
	public void setActivationOfficerDesignation(String activationOfficerDesignation) {
		this.activationOfficerDesignation = activationOfficerDesignation;
	}
	public String getStatusOfSubscriber() {
		return statusOfSubscriber;
	}
	public void setStatusOfSubscriber(String statusOfSubscriber) {
		this.statusOfSubscriber = statusOfSubscriber;
	}
	public String getValueAddedServiceApplied() {
		return valueAddedServiceApplied;
	}
	public void setValueAddedServiceApplied(String valueAddedServiceApplied) {
		this.valueAddedServiceApplied = valueAddedServiceApplied;
	}
	public String getImsiNumber() {
		return imsiNumber;
	}
	public void setImsiNumber(String imsiNumber) {
		this.imsiNumber = imsiNumber;
	}
	public String getAreaType() {
		return areaType;
	}
	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}
	public String getSubscriberProfession() {
		return subscriberProfession;
	}
	public void setSubscriberProfession(String subscriberProfession) {
		this.subscriberProfession = subscriberProfession;
	}
	public String getUidNumber() {
		return uidNumber;
	}
	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}
	public String getTarrifPlanApplied() {
		return tarrifPlanApplied;
	}
	public void setTarrifPlanApplied(String tarrifPlanApplied) {
		this.tarrifPlanApplied = tarrifPlanApplied;
	}
	public String getPreviousServiceProviderName() {
		return previousServiceProviderName;
	}
	public void setPreviousServiceProviderName(String previousServiceProviderName) {
		this.previousServiceProviderName = previousServiceProviderName;
	}
	public String getDtrCode() {
		return dtrCode;
	}
	public void setDtrCode(String dtrCode) {
		this.dtrCode = dtrCode;
	}
	public Long getCpId() {
		return cpId;
	}
	public void setCpId(Long cpId) {
		this.cpId = cpId;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getScannedDate() {
		return scannedDate;
	}
	public void setScannedDate(Timestamp scannedDate) {
		this.scannedDate = scannedDate;
	}
	
	public String getRtrCode() {
		return rtrCode;
	}
	public void setRtrCode(String rtrCode) {
		this.rtrCode = rtrCode;
	}
	public String getRtrName() {
		return rtrName;
	}
	public void setRtrName(String rtrName) {
		this.rtrName = rtrName;
	}
	public void setCaoCode(String caoCode) {
		this.caoCode = caoCode;
	}
	public String getCaoCode() {
		return caoCode;
	}
	public String getPollingStation() {
		return pollingStation;
	}
	public void setPollingStation(String pollingStation) {
		this.pollingStation = pollingStation;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getVoterName() {
		return voterName;
	}
	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}
	@Override
	public String toString() {
		return "CafVo [id=" + id + ", speedocId=" + speedocId + ", fileId="
				+ fileId + ", mobileNumber=" + mobileNumber + ", simNumber="
				+ simNumber + ", cafNumber=" + cafNumber + ", unknownNumber="
				+ unknownNumber + ", report=" + report + ", photograph="
				+ photograph + ", mnp=" + mnp + ", existingNumber="
				+ existingNumber + ", connectionType=" + connectionType
				+ ", foreignNational=" + foreignNational + ", outstation="
				+ outstation + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", gender=" + gender
				+ ", email=" + email + ", service=" + service + ", dob=" + dob
				+ ", nationality=" + nationality + ", fatherFirstName="
				+ fatherFirstName + ", fatherMiddleName=" + fatherMiddleName
				+ ", fatherLastName=" + fatherLastName + ", localAddress="
				+ localAddress + ", localAddressLandmark="
				+ localAddressLandmark + ", localCity=" + localCity
				+ ", localPincode=" + localPincode + ", localDistrict="
				+ localDistrict + ", localState=" + localState
				+ ", alternateMobileNumber=" + alternateMobileNumber
				+ ", permanentAddress=" + permanentAddress
				+ ", permanentAddressLandmark=" + permanentAddressLandmark
				+ ", permanentCity=" + permanentCity + ", permanentPincode="
				+ permanentPincode + ", permanentDistrict=" + permanentDistrict
				+ ", permanentState=" + permanentState + ", panGirNumber="
				+ panGirNumber + ", poi=" + poi + ", poiref=" + poiref
				+ ", poa=" + poa + ", poaref=" + poaref + ", passportNumber="
				+ passportNumber + ", visaValidTill=" + visaValidTill
				+ ", typeOfVisa=" + typeOfVisa + ", visaNumber=" + visaNumber
				+ ", localReferenceName=" + localReferenceName
				+ ", localReferenceAddress=" + localReferenceAddress
				+ ", localReferenceCity=" + localReferenceCity
				+ ", localReferencePincode=" + localReferencePincode
				+ ", localReferenceContactNumber="
				+ localReferenceContactNumber + ", category=" + category
				+ ", cafDate=" + cafDate + ", form6061=" + form6061
				+ ", languageId=" + languageId + ", cafType=" + cafType
				+ ", cafFileLocation=" + cafFileLocation + ", pages=" + pages
				+ ", parentId=" + parentId + ", activationDate="
				+ activationDate + ", monthlyExpenditure=" + monthlyExpenditure
				+ ", existingConnection=" + existingConnection
				+ ", connectionOptingFor=" + connectionOptingFor
				+ ", callerBackRingTone=" + callerBackRingTone
				+ ", friendsAndFamily=" + friendsAndFamily + ", gprs=" + gprs
				+ ", wap=" + wap + ", mms=" + mms + ", coverageStrength="
				+ coverageStrength + ", billingTransparency="
				+ billingTransparency + ", qualityOfCustomerService="
				+ qualityOfCustomerService + ", moreRetailOutlets="
				+ moreRetailOutlets + ", attractiveCallRates="
				+ attractiveCallRates + ", callingPartyNumber="
				+ callingPartyNumber + ", poiIssueDate=" + poiIssueDate
				+ ", poiIssuePlace=" + poiIssuePlace + ", poiIssuingAuthority="
				+ poiIssuingAuthority + ", poaIssueDate=" + poaIssueDate
				+ ", poaIssuePlace=" + poaIssuePlace + ", poaIssuingAuthority="
				+ poaIssuingAuthority + ", upcCode=" + upcCode
				+ ", previousOperatorCircle=" + previousOperatorCircle
				+ ", pointOfSaleCode=" + pointOfSaleCode + ", posAddress="
				+ posAddress + ", posState=" + posState + ", posCity="
				+ posCity + ", posPinCode=" + posPinCode + ", posCode="
				+ posCode + ", activationOfficerName=" + activationOfficerName
				+ ", activationOfficerDesignation="
				+ activationOfficerDesignation + ", statusOfSubscriber="
				+ statusOfSubscriber + ", valueAddedServiceApplied="
				+ valueAddedServiceApplied + ", imsiNumber=" + imsiNumber
				+ ", areaType=" + areaType + ", subscriberProfession="
				+ subscriberProfession + ", uidNumber=" + uidNumber
				+ ", tarrifPlanApplied=" + tarrifPlanApplied
				+ ", previousServiceProviderName="
				+ previousServiceProviderName + ", dtrCode=" + dtrCode
				+ ", cpId=" + cpId + ", createdDate=" + createdDate
				+ ", modifiedDate=" + modifiedDate + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", scannedDate=" + scannedDate
				+ ", caoCode=" + caoCode + ", onhold=" + onhold + ", rtrCode="
				+ rtrCode + ", rtrName=" + rtrName + ", cafTrackingStatus="
				+ cafTrackingStatus + ", pollingStation=" + pollingStation
				+ ", voterId=" + voterId + ", voterName=" + voterName + "]";
	}
}

package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class AuditPoolVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4289368166667342786L;
	private Long id;

	private Long cafTblId;

	private Timestamp startDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "ScrutinyPoolVo [id=" + id + ", cafTblId=" + cafTblId
				+ ", startDate=" + startDate + "]";
	}
}

package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class StatusResultVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6233813427836055765L;
	private String status;
	private String remark;
	private String createdBy;
	private Timestamp createdDate;
	private Long cafTblId;
	private Long cafStatusId;
	private Timestamp activationDate;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public Long getCafStatusId() {
		return cafStatusId;
	}
	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}	
	public Timestamp getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}
	
	@Override
	public String toString() {
		return "StatusResultVo [status=" + status + ", remark=" + remark
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", cafTblId=" + cafTblId + ", cafStatusId=" + cafStatusId
				+ ", activationDate=" + activationDate + "]";
	}
}

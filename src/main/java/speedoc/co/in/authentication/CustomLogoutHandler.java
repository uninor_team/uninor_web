package speedoc.co.in.authentication;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.UserService;
import speedoc.co.in.vo.UserVo;

public class CustomLogoutHandler  extends
  SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {
 
	@Autowired
	protected UserService userService;
	
	@Autowired
	IFlcService flcService;
	
	@Override
	   public void onLogoutSuccess
	     (HttpServletRequest request, HttpServletResponse response, Authentication authentication)
	     throws IOException, ServletException {
	           if(authentication != null){
	                    User user = (User) authentication.getPrincipal();
	                UserVo userVo = userService.findByUniqueUserName(user.getUsername());
	                 @SuppressWarnings("unused")
					Boolean result = flcService.updateCafStatus(userVo) ; 
	                 logger.info("Logged out user :: "+user);
	               super.onLogoutSuccess(request, response, authentication);
	           }else{
	                   response.sendRedirect("login/loginPage");
	           }
	   }
	
}

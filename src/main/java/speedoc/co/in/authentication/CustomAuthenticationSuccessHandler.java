package speedoc.co.in.authentication;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

public class CustomAuthenticationSuccessHandler extends  SimpleUrlAuthenticationSuccessHandler {
	protected static Logger logger = Logger.getLogger("CustomAuthenticationSuccessHandler");
   // private AuthenticationSuccessHandler target = new SavedRequestAwareAuthenticationSuccessHandler();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		UserDetails userDetails = (User)authentication.getPrincipal();
		System.out.println(userDetails);
		//userDetails.
			logger.debug("Authentication success");
			logger.info("onAuthenticationSuccess() : Authentication success");

	        super.onAuthenticationSuccess(request, response, authentication);

	}

}

package speedoc.co.in.manager;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.ISlcDoneDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.CAFUtils;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDoneFileVo;

import com.csvreader.CsvReader;

@Component
@Transactional
public class SlcDoneManager {
	
	@Autowired
	ISlcDoneDao slcDoneDao;
	
	@Autowired
	IUserService userService;
	
	@Value("${SLC_DONE_COMPLETE_FILE_PATH}")
	private String slcDoneCompleteFilePath;
	
	@Value("${SLC_DONE_USER}")
	private String slcDoneUser;
	
	@Autowired
	IUserDao userDao;
	
	private static Logger logger=Logger.getLogger(SlcDoneManager.class);

	public List<SlcDoneFileVo> getSlcDoneMonthWiseList(
			FilterVo<SlcDoneFileVo> filterVo, String month) {
		// month MM yy
		if (month != null && !month.equals("")) {
			String[] monthArr = month.split(" ");
			String monthName = monthArr[0];
			logger.info("monthName = " + monthName);
			String year = monthArr[1];
			logger.info("year = " + year);

			int monthNumber = AvamManager.getMonthNumber(monthName);
			return slcDoneDao.getSlcDoneMonthWiseList(filterVo, monthNumber,year);
		}
		return null;
	}

	public Long getSlcDoneCount() {
		return slcDoneDao.getSlcDoneCount();
	}

	//slc data read and insert tbl
	public boolean readSlcDoneCSVData(String path) {
		logger.info("loading file =  " + path);
		try {
			CsvReader reader = new CsvReader(path,',');		
			int numLines=0;			
				while(reader.readRecord()){
					numLines++;
				}
			logger.info("numLines :: "+numLines);
			Long slcFileId = addSlcCSVFileId(path,numLines-1);
			if (slcFileId != 0 && slcFileId != null) {
				 slcDoneDao.addSlcDoneDataFile(path);
				 slcDoneDao.updateSlcFileId(slcFileId);
				 slcDoneDao.updateSlcDoneFileByFIleId(0, 0, slcFileId);
				 logger.info("slcFileId :: readSlcDoneCSVData in SlcDoneManager:: "+slcFileId);
				 CAFUtils.moveFile1(path, slcDoneCompleteFilePath + File.separator + FilenameUtils.getName(path));
			}
			return true;
		} catch (Exception e) {
			logger.error("Error in readSlcDoneCSVData :: "+e.getMessage(),e);
			return false;
		}
	}
	
	// set data to slc_done_file_tbl 
	private Long addSlcCSVFileId(String path, int totalRecords) {
		SlcDoneFileVo slcDoneFileVo = new SlcDoneFileVo();
		slcDoneFileVo.setCreatedBy(userDao.findByUniqueUserName(slcDoneUser.trim()).getId()); //user for live
		slcDoneFileVo.setFileName(FilenameUtils.getName(path));
		slcDoneFileVo.setStatus("INPROCESS");
		slcDoneFileVo.setTotalRecord(totalRecords);
		SlcDoneFileVo insertedSlcImportRecordVo = slcDoneDao.addSlcDoneFileRecord(slcDoneFileVo);
		return insertedSlcImportRecordVo.getId();
	}
	
	public Integer uploadSlcDoneFile() {
		try{
			logger.info("inside procedure function uploadSlcDoneFile");
			Integer uploadSlcDoneFile = slcDoneDao.uploadSlcDoneFile();
			logger.info("uploadSlcDoneFile :: "+uploadSlcDoneFile);
			return uploadSlcDoneFile;
		}catch (Exception e) {
			logger.error(""+e.getMessage(),e);
			return null;
		}
	}

	public boolean clearAllCurrentAccessList() {
		boolean b=slcDoneDao.clearAllCurrentAccessList();
		return b;
	}

}

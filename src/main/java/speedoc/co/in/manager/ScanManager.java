package speedoc.co.in.manager;

import java.io.File;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.IScanDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.dao.IVendorDao;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.util.CAFUtils;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.ScanClientInfoVO;
import speedoc.co.in.vo.UserVo;
import speedoc.co.in.vo.VendorScanWorkstationVo;

@Component
@Transactional
public class ScanManager {

	private static Logger log = Logger.getLogger(ScanManager.class.getName());
	
	@Autowired
	IUserDao userDao;

	@Autowired
	IVendorDao vendorDao;
	
	@Autowired
	ICafStatusDao cafStatusDao;
	
	@Autowired
	ICafDao cafDao;
	
	@Autowired
	IScanDao scanDao;
	
	@Autowired
	AvamManager avamManager;
	
	@Value("${CAF_DATA_PATH}")
	private String cafDataPath;
	
	@Value("${TEMP_DATA_PATH}")
	private String tempDataPath;
	
	@Autowired
	private SessionService sessionService;
	
	private static String dateNow;
	
	private CafStatusVo saveCafStatusFromScanning(CafVo cafVo) {
		CafStatusVo cafStatusVo = new CafStatusVo();
		try{
			cafStatusVo.setCafTblId(cafVo.getId());
			cafStatusVo.setCreatedBy(cafVo.getCreatedBy());			
			cafStatusVo.setLatestInd(Boolean.TRUE);
			cafStatusVo.setStatusId(StatusCodeEnum.SCAN_COMPLETE.getStatusCode());			
			CafStatusVo savedCafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
			return savedCafStatusVo;
		}catch (Exception e) {
			log.error("Error in saveCafStatusFromScanning :: "+e.getMessage(),e);
			return null;
		}
	}
	
	private String writeFileToTempFolder(String fileName, InputStream in) {
		try{
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
			dateNow = formatter.format(currentDate.getTime());

			CAFUtils.createFoldersIfNotExists(dateNow , cafDataPath);

			String tempFilePath = null;
			String newFileName = CAFUtils.createFileIfExistsTemp(tempDataPath , FilenameUtils.getBaseName(fileName));

//			if (newFileName != null) {
//				tempFilePath = tempDataPath + File.separator + newFileName;
//				Base64.decodeToFile(in, tempFilePath);
//			}
			
			if (in != null) {

				tempFilePath = tempDataPath + File.separator + newFileName;
				
				log.debug("writing CAF to TEMP path :: " + tempFilePath);
	            File file = new File(tempFilePath);
				
				FileUtils.copyInputStreamToFile(in, file);
			}
			return tempFilePath;
		}catch (Exception e) {
			log.error("writeFileToTempFolder :: "+e.getMessage(),e);
			return null;
		}		
	}

	public ScanClientInfoVO getScanClientInfo(ScanClientInfoVO scanClientInfoVO) {
		ScanClientInfoVO scanClientInfo = new ScanClientInfoVO();
		scanClientInfo.setUserVO(userDao.findById(scanClientInfoVO.getUserVO().getId()));
		VendorScanWorkstationVo vendorScanWorkstationVo = vendorDao.getVendorScanWorkStationByRegKey(scanClientInfoVO.getRegKey());
		if (vendorScanWorkstationVo != null && StringUtils.trimToNull(vendorScanWorkstationVo.getMacId())!= null) {
			// Already Registered
			log.info("Scan User already registerd");
			scanClientInfo.setMacAddress(vendorScanWorkstationVo.getMacId().trim());
		} else if(vendorScanWorkstationVo != null){
			// new Registration
			log.info("Scan User NEW Registration");
			vendorDao.updateVendorScanWorkStationByRegKey(scanClientInfoVO.getMacAddress(),scanClientInfoVO.getRegKey());
			scanClientInfo.setMacAddress(scanClientInfoVO.getMacAddress());
		}else{
			log.info("Error Inside Scan User :: getScanClientInfo. vendorScanWorkstationVo is null.");
			scanClientInfo.setUserVO(new UserVo());
		}
		scanClientInfo.setPostUrl(scanClientInfoVO.getPostUrl());
		scanClientInfo.setRegKey(scanClientInfoVO.getRegKey());

		return scanClientInfo;
	}
	
	public boolean checkFileIdExistsInCaf(String fileId, String speedocId) {
		return scanDao.checkFileIdExistsInCaf(fileId,speedocId);
	}
	
	private String getFileLocation(CafVo cafVo) {

		String fileName = null;
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
			String locationDate = dateFormat.format(cafVo.getScannedDate().getTime());
			String speedocId = StringUtils.trimToNull(cafVo.getSpeedocId());
			String unknownNumber = StringUtils.trimToNull(cafVo.getUnknownNumber());
			if (speedocId != null) {
				fileName = speedocId	+ ".pdf";
			} else if (cafVo.getUnknownNumber() != null) {
				fileName = unknownNumber+ ".pdf";
			} else {
				log.info("File Location Not Decided Properly. :: cafVO "+ cafVo.toString());
				throw new Exception();
			}
			String newCafFileLocation = CAFUtils.checkAndGetCafFileLocation(cafDataPath,locationDate, FilenameUtils.getBaseName(fileName));
			newCafFileLocation = locationDate + File.separator + newCafFileLocation; 
			log.info(" newCafFileLocation :: " + newCafFileLocation);
			return newCafFileLocation;
		}catch (Exception e) {
			log.error("Error in getFileLocation :: "+e.getMessage(),e);
			return null;
		}
		
	}

	public String savePDF(InputStream in, String fileName) {
		String tempFilePath = null;
		String speedocId = null;
		try {
			
			tempFilePath = writeFileToTempFolder(fileName, in);

			CafVo cafVo = CAFUtils.getCafVoFromPdf(tempFilePath);
			log.info("savePDF cafVo  :: "+cafVo);
			
			speedocId = StringUtils.trimToNull(cafVo.getSpeedocId());

			if (!checkFileIdExistsInCaf(cafVo.getFileId() , speedocId)) {

				cafVo.setCafFileLocation(getFileLocation(cafVo));
				CafVo savedCafVo = cafDao.saveCaf(cafVo);

				if (savedCafVo != null) {

					CAFUtils.moveFile(tempFilePath, cafDataPath + File.separator +cafVo.getCafFileLocation());
					tempFilePath = cafDataPath +File.separator +cafVo.getCafFileLocation();

					CafStatusVo cafStatusVo = saveCafStatusFromScanning(savedCafVo);
					cafStatusDao.updateCafStatusLatestToOld(Boolean.FALSE,cafStatusVo.getId());

					if (speedocId != null && cafStatusVo != null) {
						AvamVo avamVo = avamManager.getAvamBySpeedocId(speedocId); 
						if (avamVo != null	&& avamVo.getSpeedocId().equals(speedocId)) {
							CafStatusVo statusVo = cafStatusDao.saveCafStatus(Utils.getCafStatusVoForAvamStatus(StatusCodeEnum.AVAM_SUCCESS.getStatusCode(), savedCafVo));
							if(statusVo != null){
								boolean updateAvamTblByAvamId = avamManager.updateAvamTblByAvamId(savedCafVo,avamVo.getId());
								log.info("scanManager savePDF method :: updateAvamTblByAvamId :: "+updateAvamTblByAvamId);
								savedCafVo.setModifiedDate(new Timestamp(new Date().getTime()));
								savedCafVo.setModifiedBy(cafVo.getCreatedBy());
								boolean updateCafTblByAvamData = scanDao.updateCafTblByAvamData(savedCafVo, avamVo);
								log.info("scanManager savePDF method :: updateCafTblByAvamData:: "+updateCafTblByAvamData);
							}
						} else {
							cafStatusDao.saveCafStatus(Utils.getCafStatusVoForAvamStatus(StatusCodeEnum.AVAM_FAIL.getStatusCode(), savedCafVo));
						}
					} else {
						cafStatusDao.saveCafStatus( Utils.getCafStatusVoForAvamStatus(StatusCodeEnum.UNKNOWN.getStatusCode(),savedCafVo));
					}
					return "success";
				} else {
					log.error(" savedCafVo is NULL.");
					throw new Exception();
				}
			} else {					
				log.error("File Id Already Exists OR SpeedocId will Already Exists...");
				return "already_exists";
			}
		} catch (Exception e) {			
			log.error("ROLL BACK FROM UNINOR SCAN WEB :: " + e.getMessage(),e);
			return "failure";				
		}		
	}
	
}

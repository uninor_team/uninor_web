package speedoc.co.in.manager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.IActivationOfficerDao;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;

@Component
@Transactional
public class ActivationOfficerManager {

	private static Logger logger=Logger.getLogger(ActivationOfficerManager.class);
	
	@Autowired
	IActivationOfficerDao activationOfficerDao;
	
	@Value("${ACTIVATION_OFFICER_FILE_PATH}")
	private String caoPath;
	
	public CaoVo saveActivationOfficer(CaoVo activationOfficerVo){
		return activationOfficerDao.saveActivationOfficer(activationOfficerVo);
	}
	
	public List<CaoVo> getActivationList(
			FilterVo<CaoVo> filterVo) {	
			return activationOfficerDao.getActivationList(filterVo);		
	}

	public Boolean isCaoCodeAlreadyExist(String caoCode) {
		
		return activationOfficerDao.isCaoCodeAlreadyExist(caoCode);
	}

	public CaoVo getSignatureById(Long id) {
		logger.info("Manager :: getSignatureById :: id :: "+ id);
		return activationOfficerDao.getSignatureById(id);
	}
	
	public boolean changeCaoStatus(Long id,int status) {
		
		return activationOfficerDao.changeCaoStatus(id,status);
		
	}

	public void getSignFile(String fileName, OutputStream out) {
		fileName = caoPath + File.separator + fileName;
			logger.info("fileName :: "+fileName);
		try {
			FileUtils.copyFile(new File(fileName), out);
		} catch (IOException e1) {
			logger.error(e1.getMessage(), e1);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public Long getActivationCount() {
		return activationOfficerDao.getActivationCount();
	}
}

package speedoc.co.in.manager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.dao.IndexingDao;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.IndexingVo;
import speedoc.co.in.vo.ScrutinyCaptureVo;
import speedoc.co.in.vo.UserSummaryVo;

@Component
public class IndexingManager {
	@Autowired
	IndexingDao indexingDao;

	@Autowired
	ICafDao cafDao;

	@Autowired
	ICafStatusDao cafStatusDao;
	
	@Autowired
	IUserDao userDao;
	
	@Value("${INDEX_USER_NAME}")
	private String indexUserName;

	private TransactionTemplate txTemplate;

	private static Logger log = Logger.getLogger(IndexingManager.class.getName());

	@Autowired
	public void setTransactionManager(PlatformTransactionManager txManager) {
		this.txTemplate = new TransactionTemplate(txManager);
		this.txTemplate.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
	}

	public Integer saveIndexData(IndexingVo indexingVo) {
		Integer resultCode = indexingDao.saveIndexData(indexingVo);
		log.info("indexingManager indexingVo :: " + indexingVo);
		return resultCode;
	}
	
	// schedular for indexing data
	public void getNewIndexData() {
		log.info("\n schedular for indexing :::::::::: ");
		try {
			ArrayList<IndexingVo> indexList = (ArrayList<IndexingVo>) indexingDao.findNewIndexData(StatusCodeEnum.NEW.getStatusCode());
			if (indexList != null && indexList.size() > 0) {
				log.info("indexList.size ::  " + indexList.size());
				for (IndexingVo indexingVo : indexList) {
					searchAndUpdate(indexingVo);
				}
			}
		} catch (Exception e) {
			log.error("Error in getNewIndexData :::::: " + e.getMessage(), e);
		}
	}

	private void searchAndUpdate(final IndexingVo indexingVo) {
		log.info("Inside searchAndUpdate..");
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			
		@Override
		protected void doInTransactionWithoutResult(TransactionStatus status) {
		try {
			int count = indexingVo.getSearchCount();
			count++;
			Integer indexStatus = null;
			CafVo cafVO = new CafVo();
			CafStatusVo cafStatusVO = indexingDao.searchCafTblForNewMobileIndexData(indexingVo.getMobileNumber(),indexingVo.getCafNumber());
			if (cafStatusVO != null && cafStatusVO.getCafTblId() != null) {
				if (cafStatusVO.getStatusId() == StatusCodeEnum.AVAM_SUCCESS.getStatusCode()) {
					indexStatus = StatusCodeEnum.FOUND.getStatusCode();
					Utils.copyProperties(cafVO, indexingVo);
					cafVO.setId(cafStatusVO.getCafTblId());
					cafVO.setModifiedBy(userDao.findByUniqueUserName(indexUserName.trim()).getId());     
					cafVO.setModifiedDate(new Timestamp(new Date().getTime()));					
					cafDao.updateCafFromIndexing(cafVO);
					log.info("Caf Table Updated :::::: ");

					CafStatusVo cafStatusVoForDEStatus = new CafStatusVo();
					cafStatusVoForDEStatus.setCafTblId(cafStatusVO.getCafTblId());
					cafStatusVoForDEStatus.setCreatedBy(userDao.findByUniqueUserName(indexUserName.trim()).getId());
					cafStatusVoForDEStatus.setLatestInd(true);
					cafStatusVoForDEStatus.setStatusId(StatusCodeEnum.DE_COMPLETE.getStatusCode());

					cafStatusDao.updateCafStatusLatestToOld(false,cafStatusVO.getId());
					cafStatusDao.saveCafStatus(cafStatusVoForDEStatus);
					
					log.info("Inserted Status DE_COMPLETED ::::::: ");
					indexingDao.updateIndexingTbl(indexingVo.getId(), count,indexStatus, cafStatusVO.getCafTblId());
				} else {
					log.info("Inside Not Avam_Fail :::::::::::: ");
					indexStatus = StatusCodeEnum.NEW.getStatusCode();
					indexingDao.updateIndexingTbl(indexingVo.getId(), count,indexStatus, null);
				}
			} else {
				indexStatus = StatusCodeEnum.NEW.getStatusCode();
				indexingDao.updateIndexingTbl(indexingVo.getId(), count,indexStatus, null);
			}
		} catch (Exception e) {
			log.error("Error in searchAndUpdate ::::::: " + e);
			status.setRollbackOnly();
		}
	   }
	  });
	}

	public List<ScrutinyCaptureVo> getScrutinyCompletedRecords() {
		return indexingDao.getScrutinyCompletedRecords();
	}

	public String setIndexingCompleted(ScrutinyCaptureVo scrutinyCaptureVO) {
		log.info("inside IndexingManager :: setIndexingCompleted().");
		String saveStatusToIndexingTbl = indexingDao.saveStatusToIndexingTbl(
				StatusCodeEnum.INDEXING_COMPLETE.getStatusCode(),
				scrutinyCaptureVO.getCafTblId());
		log.info("isupdate saveStatusToIndexingTbl = "
				+ saveStatusToIndexingTbl);
		if (saveStatusToIndexingTbl.equalsIgnoreCase("success")) {
			try {
				CafStatusVo cafStatusVoForDEStatus = new CafStatusVo();
				cafStatusVoForDEStatus.setCafTblId(scrutinyCaptureVO.getCafTblId());
				cafStatusVoForDEStatus.setCreatedBy(userDao.findByUniqueUserName(indexUserName.trim()).getId());
				cafStatusVoForDEStatus.setLatestInd(true);
				cafStatusVoForDEStatus.setStatusId(StatusCodeEnum.INDEXING_COMPLETE.getStatusCode());

				CafStatusVo latestCafStatusVoByCafTblId = cafStatusDao.getLatestCafStatusVoByCafTblId(scrutinyCaptureVO.getCafTblId());
				cafStatusDao.updateCafStatusLatestToOld(false,latestCafStatusVoByCafTblId.getId());
				cafStatusDao.saveCafStatus(cafStatusVoForDEStatus);
			} catch (Exception e) {
				log.error(e);
			}
			indexingDao.updateScrutinySummaryTbl(scrutinyCaptureVO.getAllotedToUserSummaryId());
			return "success";
		} else {
			return "failure";
		}
	}

	public ArrayList<String> getUserToAllot() {
		UserSummaryVo userSummaryVO = indexingDao.getUserToAllot();
		log.info(" userSummaryVO" + userSummaryVO);
		if (userSummaryVO != null) {
			ArrayList<String> summaryIdUsername = new ArrayList<String>();
			int summaryId = indexingDao.insertUserIntoSummary(userSummaryVO);
			log.info("summary table id generated :: " + summaryId);
			summaryIdUsername.add(summaryId + "");
			summaryIdUsername.add(userSummaryVO.getUserName());
			return summaryIdUsername;
		} else {
			return null;
		}
	}

	public Integer updateIndexingStatus() {
		try {
			Integer updateStatus = indexingDao.updateIndexingStatus();
			return updateStatus;
		} catch (Exception e) {
			log.error(""+e.getMessage(),e);
			return null;
		}
	}
	
	public IndexingVo getElectroData() {
		return indexingDao.getElectroData();
	}


	@SuppressWarnings("unused")
	public Integer saveElectroData(IndexingDataVO indexingDataVO) {
		System.out.println(indexingDataVO.getId());
		 Integer saveId = indexingDao.saveElectroData(indexingDataVO);
		// update the data in the indexing table after the data retrieval
		System.out.println(indexingDataVO);
		System.out.println(saveId);
			Integer updateStatus= indexingDao.updateIndexingTbl(indexingDataVO);
		return saveId;
	}
}
package speedoc.co.in.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.ISearchDao;
import speedoc.co.in.vo.SearchResultVo;

@Component
@Transactional
public class SearchManager {
	
	@Autowired
	ISearchDao searchDao;

	public List<SearchResultVo> getSearchDataByMobileNumber(String mobileNumber) {
		return searchDao.getSearchDataByMobileNumber(mobileNumber);		 
	}

	public List<SearchResultVo> getSearchDataBySpeedocId(String speedocId) {
		return searchDao.getSearchDataBySpeedocId(speedocId);
	}	
}

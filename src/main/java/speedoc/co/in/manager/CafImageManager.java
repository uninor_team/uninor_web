package speedoc.co.in.manager;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.util.ImageUtils;

@Service
@Transactional
public class CafImageManager {
	
	private Logger log = Logger.getLogger(CafImageManager.class);
	
	@Value("${CAF_DATA_PATH}")
	private String cafDataPath;
	
	@Value("${TEMP_DATA_PATH}")
	private String cafTempPath;	

	public String getImageRotatePdf(String fileName, int pageNo){
		String newFileName = null;
		newFileName = getRotatePdf(fileName, pageNo);
		if (!newFileName.equals(null)) {
			getCafImageNames(fileName, false);
		}
		return newFileName;
	}
	
	// get rorate images
	@SuppressWarnings({ "rawtypes" })
	private String getRotatePdf(String fileName, int pageNo) {
		String currentFileName = cafDataPath + File.separator + fileName;
//		String currentFileName = "D:\\data01\\appdata\\speedoc\\uninor\\caf-files\\29052014\\103500004.pdf";
		PDDocument document1 = null; 
		String[] folderName = fileName.split("/");
		 log.info("FolderName " + folderName[0]);
		String filename = folderName[1].replaceAll(".pdf", ""); 
//		String filename = "103500004.pdf";
		
		PDDocumentInformation info = null; 
		PDDocument document = null;

		try {
			document = PDDocument.load(currentFileName);
			document1 = new PDDocument();

			List pages = document.getDocumentCatalog().getAllPages();
			int imageCounter = 0;
			Iterator iter = pages.iterator();
			while (iter.hasNext()) {
				PDPage page = (PDPage) iter.next();
				PDResources resources = page.getResources();

				Map images = resources.getImages();
				if (images != null) {
					Iterator imageIter = images.keySet().iterator();
					while (imageIter.hasNext()) {
						String key = (String) imageIter.next();
						PDXObjectImage image = (PDXObjectImage) images.get(key);
						log.info(" getRotatePdf :: key :: "+key);
						String tempJpgFilePath1 = cafTempPath + "/"
								+ filename + "_" + imageCounter;  
//						String tempJpgFilePath1 = "D:\\data01\\appdata\\speedoc\\uninor\\temp-files\\29052014\\103500004" + "_" + imageCounter; 
						
						 log.info("tempJpgFilePath ::" + tempJpgFilePath1);
						image.write2file(tempJpgFilePath1);
						String tempJpgFilePath = tempJpgFilePath1
								+ ".jpg";
						
						if (imageCounter == (pageNo - 1)) {
							 log.info("page counter" +imageCounter);
							rotateImage(tempJpgFilePath, tempJpgFilePath);
//							resize(tempJpgFilePath, tempJpgFilePath, 0.5);
							
							// rotateImage(tempJpgFilePath,"/home/yogesh_pokale/training"+imageCounter+".jpg");
						}
						try {
							compressImage(new File(tempJpgFilePath));
						} catch (Exception e) {
							log.info("error : "+e.getMessage(),e); 
						}
						createPageAndAddImage(tempJpgFilePath, document1);
						
						File removeFile = new File(tempJpgFilePath);
						if (removeFile.exists()) {
							boolean remove = removeFile.delete();
							log.info("delete " + remove);
						}
						imageCounter++;
					}
				}
			}
			info = document.getDocumentInformation();
			
			// document.close();
		} catch (Exception e) {
			log.error("CafImageManager :: getRotatePdf ::  " + e.getMessage(),e);
		}
		document1.setDocumentInformation(info);
		try {
			// document1.save(currentFileName);/home/yogesh_pokale/training
			document.close();
			document1.save(currentFileName);
			document1.close();
			
		} catch (COSVisitorException e) {
			log.error("CafImageManager :: getRotatePdf ::  " + e.getMessage(),e);
		} catch (IOException e) {
			log.error("CafImageManager :: getRotatePdf ::  " + e.getMessage(),e);
		}
		return fileName;
	}
	
	//get caf from images
	@SuppressWarnings("rawtypes")
	public String getCafImageNames(String cafFileName, boolean renameImage) {
		PDDocument document = null;
		String imageNames = "";
		try {
			String tempJpgFilePath = cafTempPath + "/"+ cafFileName;
			String pdfPath = cafDataPath + File.separator + cafFileName;
			//local
//			String tempJpgFilePath = "D:\\data01\\appdata\\speedoc\\uninor\\temp-files\\"+ cafFileName;
//			String pdfPath = "D:\\data01\\appdata\\speedoc\\uninor\\caf-files\\29052014" + File.separator + cafFileName;

			document = PDDocument.load(pdfPath);

			log.info("After Loading");

			List pages = document.getDocumentCatalog().getAllPages();
			int imageCounter = 0;
			Iterator iter = pages.iterator();
			while (iter.hasNext()) {
				PDPage page = (PDPage) iter.next();
				PDResources resources = page.getResources();

				Map images = resources.getImages();

				if (images != null) {
					Iterator imageIter = images.keySet().iterator();
					while (imageIter.hasNext()) {
						String key = (String) imageIter.next();
						PDXObjectImage image = (PDXObjectImage) images.get(key);

						String cafFileNameNew = cafFileName.replaceAll(".pdf","");
						imageNames = imageNames + ";" + cafFileNameNew + "_"
								+ imageCounter + ".jpg";
						String name = tempJpgFilePath + "_" + imageCounter;
						name = name.replaceAll(".pdf", "");
						String folderForTemp = cafTempPath + File.separator + cafFileName.split("/")[0];
						//local
//						String folderForTemp = "D:\\data01\\appdata\\speedoc\\uninor\\temp-files" + File.separator s+ cafFileName.split("/")[0];

						File tempFile = new File(tempJpgFilePath);
						 log.debug("Creating folder wile renameing " +
						 tempFile.getParent());

						File file = new File(tempFile.getParent());
						if (!file.exists()) {
							boolean success = (new File(tempFile.getParent())).mkdir();
							if (success) {
								log.info("Directory: " + folderForTemp + " created");
							} else {
								log.info("Directory: " + folderForTemp + " creation failed");
							}
						}
						image.write2file(name);
						try {
							compressImage(new File(name+".jpg"));
						} catch (Exception e) {
							log.info("error : "+e.getMessage(),e); 
						}
						imageCounter++;
					}
				}
				if (renameImage) {
					break;
				}
			}
			document.close();
		} catch (Exception eX) {
			log.error("CafImageManager :: getCafImageNames :: " + eX.getMessage());
			return null;
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (IOException e) {
					log.error("CafImageManager :: getCafImageNames :: "+ e.getMessage());
				}
			}
		}
		if (imageNames == "") {
			imageNames = null;
		}

		imageNames = imageNames.replaceAll(".pdf", "");
		log.info("imageNames ::::::: " + imageNames);
		return imageNames;
	}
	
	void rotateImage(String inFileName, String outFileName) {
		try {
			BufferedImage bif = ImageUtils.readImage(inFileName);   //commented on 26-02-2015
			BufferedImage rotatedImage = ImageUtils.rotateImage(bif, 90);
			ImageUtils.writeImage(rotatedImage, outFileName, "jpg");
		} catch (Exception e) {
			log.error("CafImageManager :: rotateImage ::  " + e.getMessage());
		}
	}
	
	// create page and add image in pdf 
	private void createPageAndAddImage(String imageFile, PDDocument pdfDoc)
			throws Exception {

		PDXObjectImage ximage = null;
		if (imageFile.toLowerCase().endsWith(".jpg")) {
			
			 File inputFile = new File(imageFile);		
			 ximage = new PDJpeg(pdfDoc, ImageIO.read(inputFile));
		} else if (imageFile.toLowerCase().endsWith(".tif")
				|| imageFile.toLowerCase().endsWith(".tiff")) {
			ximage = new PDCcitt(pdfDoc, new RandomAccessFile(new File(
					imageFile), "r"));
		} else {
			throw new IOException("Image type not supported:" + imageFile);
		}

		PDRectangle rect = new PDRectangle(ximage.getWidth(), ximage.getHeight());
		PDPage page = new PDPage(rect);
		pdfDoc.addPage(page);
		PDPageContentStream contentStream = new PDPageContentStream(pdfDoc, page);

		contentStream.drawImage(ximage, 10, 10);
		contentStream.close();
	}
	
	//compress images 
	private void compressImage(File input) throws IOException{
		log.info("Inside compressImage");
	      BufferedImage image = ImageIO.read(input);

	      File compressedImageFile = new File(input.getPath());
	      OutputStream os = new FileOutputStream(compressedImageFile);
	      Iterator<ImageWriter>writers = 
	      ImageIO.getImageWritersByFormatName("jpg");
	      ImageWriter writer = (ImageWriter) writers.next();

	      ImageOutputStream ios = ImageIO.createImageOutputStream(os);
	      writer.setOutput(ios);

	      ImageWriteParam param = writer.getDefaultWriteParam();
	      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	      //6-8-2015
	      //param.setCompressionQuality(0.2f);
	      param.setCompressionQuality(0.1f);
	      writer.write(null, new IIOImage(image, null, null), param);
	      os.close();
	      ios.close();
	      writer.dispose();	
	}	
	
	// not use this methods
	// resize image smaller by 50%
	public static void resize(String inputImagePath,
	           String outputImagePath, double percent) throws IOException {
	       File inputFile = new File(inputImagePath);
	       BufferedImage inputImage = ImageIO.read(inputFile);
	       int scaledWidth = (int) (inputImage.getWidth() * percent);
	       int scaledHeight = (int) (inputImage.getHeight() * percent);
	       resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
	   }
	// resize to fixed width
	public static void resize(String inputImagePath, String outputImagePath, int scaledWidth, int scaledHeight) throws IOException {
	       // reads input image
	       File inputFile = new File(inputImagePath);
	       BufferedImage inputImage = ImageIO.read(inputFile);
	       // creates output image
	       BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());
	       // scales the input image to the output image
	       Graphics2D g2d = outputImage.createGraphics();
	       g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
	       g2d.dispose();
	       // extracts extension of output file
	       String formatName = outputImagePath.substring(outputImagePath.lastIndexOf(".") + 1);
	       // writes to output file
	       ImageIO.write(outputImage, formatName, new File(outputImagePath));
	   }
}

	
	
	
	
	
	
	
	
	
	
	
	
package speedoc.co.in.manager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.ICaoDao;
import speedoc.co.in.dao.IScrutinyPoolDao;
import speedoc.co.in.dao.ISlcRemarkDao;
import speedoc.co.in.dao.SessionCafDaoImpl;
import speedoc.co.in.vo.SessionCafVo;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.PosCposVo;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.SlcRemarkVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

@Component
@Transactional
public class ScrutinyManager {

	@Autowired
	private IScrutinyPoolDao scrutinyPoolDao;

	@Autowired
	private ICafDao cafDao;

	@Autowired
	protected SessionCafDaoImpl sessionCafDaoImpl;
	
	@Autowired
	private ISlcRemarkDao slcRemarkDao;

	@Autowired
	private ICafStatusDao cafStatusDao;

	@Value("${TEMP_DATA_PATH}")
	private String cafTempPath;

	@Value("${ACTIVATION_OFFICER_FILE_PATH}")
	private String caoPath;

	@Autowired
	private ICaoDao caoDao;

	private Logger log = Logger.getLogger(ScrutinyManager.class);

	public HttpSession session() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		return attr.getRequest().getSession(false); // true == allow create
	}
	
	public CafScrutinyVo getDECompletedCafs(UserVo userVo) {
		CafScrutinyVo cafScrutinyVo = null;
		try {
			cafScrutinyVo = cafDao.getDeCompletedCaf(userVo);
			log.info("\ngetDECompletedCafs() of ScrutinyManager :: cafScrutinyVo :: \n"+ cafScrutinyVo + "\n\n");
			
			if(cafScrutinyVo !=null){
				if(sessionCafDaoImpl.getSessionCafVoByCafId(cafScrutinyVo.getId())){
					log.info("caf already exist" + cafScrutinyVo);
					return getDECompletedCafs(userVo);
				}
				cafScrutinyVo.setSessionId(session().getId());
				insertSessionCaf(userVo, cafScrutinyVo);
				log.info("cafScrutinyVo = " + cafScrutinyVo);
			}else{
				cafScrutinyVo = null;
			}
		} catch (Exception e) {
			log.error("Error in getDECompletedCafs() of ScrutinyManager :: "+ e.getMessage(),e);
		}
		return cafScrutinyVo;
	}
	public Boolean save(CafScrutinyVo cafScrutinyVo, UserVo userVo) {
	boolean result = false;
	try {
		//  Update CafStatus tbl (Setting end date and latestInd to false and current_accessing_user_id null)
			boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false,cafScrutinyVo.getCafStatusId());
			log.info("Scrutiny Rows Updated  in updateCafStatus ::"+ poolResult);
			
		if (poolResult) {

			// Insert status to slc accepted
			CafStatusVo cafStatusVo = new CafStatusVo();
			cafStatusVo.setCafTblId(cafScrutinyVo.getId());
			cafStatusVo.setLatestInd(true);
			cafStatusVo.setStatusId(StatusCodeEnum.SCRUTINY_COMPLETE.getStatusCode());
			cafStatusVo.setCreatedBy(userVo.getId());
			if (!cafStatusDao.alreadyScrutinyComplete(cafStatusVo)) {
				cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);

				log.info("Saved Status Scrutiny Accepted  with id ::"
						+ cafStatusVo.getId());
			}
			// Delete the images
			String[] imageNames = cafScrutinyVo.getImages1().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
						}
					}
				}
				result = true;
			}
			removeCaf(cafScrutinyVo.getId());
		} catch (Exception e) {
			log.error("Error in save() of ScrutinyManager :: " + e.getMessage(),e);
			result = false;
			removeCaf(cafScrutinyVo.getId());
		}
		return result;
	}

	public Boolean rejectSlc(String rejectReason, CafScrutinyVo cafScrutinyVo,
			UserVo userVo) {
		boolean result = false;
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false)
				boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false,cafScrutinyVo.getCafStatusId());
				log.info("Scrutiny Rows Updated  in updateCafStatus ::" + poolResult);

			if (poolResult) {

				// Add status
				log.info("rejectSlc cafScrutinyVo.getId() = "
						+ cafScrutinyVo.getId());
				CafStatusVo cafStatusVo = new CafStatusVo();
				cafStatusVo.setCafTblId(cafScrutinyVo.getId());
				cafStatusVo.setLatestInd(true);
				cafStatusVo.setStatusId(StatusCodeEnum.SCRUTINY_REJECT
						.getStatusCode());
				cafStatusVo.setCreatedBy(userVo.getId());
				cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
				log.info("Saved rejectSlc Status Rejected with id ::"
						+ cafStatusVo.getId());

				String[] rejectReasonArr = rejectReason.split(",");
				// Save remarks in Remark Tbl
				for (int i = 0; i < rejectReasonArr.length; i++) {
					SlcRemarkVo slcRemarkVo = new SlcRemarkVo();
					slcRemarkVo.setCafTblId(cafScrutinyVo.getId());
					slcRemarkVo.setRemarkId(Long.parseLong(rejectReasonArr[i]
							.trim()));
					slcRemarkVo.setUserId(userVo.getId());
					slcRemarkVo.setStatusId(cafStatusVo.getId());
					Long id = slcRemarkDao.save(slcRemarkVo);
					log.info("Saved rejectSlc with id ::" + id);
				}

				// Delete the images
				String[] imageNames = cafScrutinyVo.getImages1().split(",");
				if (imageNames.length > 0) {
					for (int i = 0; i < imageNames.length; i++) {
						if (!imageNames[i].equals("")) {
							deleteFile(imageNames[i]);
						}
					}
				}
				result = true;
				removeCaf(cafScrutinyVo.getId());
			}
		} catch (Exception e) {
			log.error("Error in rejectSlc() of ScrutinyManager :: "+ e.getMessage(), e);
			result = false;
			removeCaf(cafScrutinyVo.getId());
		}
		return result;
	}

	public void deleteFile(String fileName) throws Exception {
		File file = new File(cafTempPath + File.separator + fileName);
		if (file.exists()) {
			file.delete();
			log.info("File :" + fileName + "Deleted");
		} else {
			log.info("ScrutinyManager Unable to delete File. File not found ::"
					+ fileName);
		}
	}

	public CaoVo getCaoImage(String caoCode) {
		return caoDao.getCaoImageByCaoCode(caoCode);
	}

	public void getCafFile(String fileName, OutputStream out) {

		fileName = caoPath + File.separator + fileName;
		try {
			FileUtils.copyFile(new File(fileName), out);
		} catch (IOException e1) {
			log.error("Error in getCafFile caoCode file :: " + e1.getMessage(),
					e1);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				log.error("Error in getCafFile caoCode file :: " + e.getMessage(),e);
			}
		}
	}

	public void deleteSlcPool() {
		slcRemarkDao.deleteSlcPool();
	}

	public boolean findSpeedocIdExists(String speedocId) {
		return cafDao.findSpeedocIdAndStatusExists(speedocId);
	}

	public List<SearchResultVo> getSlcSearchDataByMobileNumber(
			String mobileNumber, UserVo userVo) {

		return scrutinyPoolDao.getSlcSearchDataByMobileNumber(mobileNumber);
	}

	public List<SearchResultVo> getSlcSearchDataBySpeedocId(String speedocId,
			UserVo userVo) {
		return scrutinyPoolDao.getSlcSearchDataBySpeedocId(speedocId);
	}

	public List<SearchResultVo> getSlcSearchDataByCafNumber(String cafNumber,
			UserVo userVo) {
		return scrutinyPoolDao.getSlcSearchDataByCafNumber(cafNumber);
	}

	//get data for slc search
	public CafScrutinyVo getSlcSearchCaf(UserVo userVo, String cafId) {

		CafVo cafVo;
		CafScrutinyVo cafScrutinyVo = null;
		try {			
			cafVo = cafDao.findCafById(Long.valueOf(cafId));
			log.info("cafVo = " + cafVo);
			PosCposVo posCposVo = cafDao.getPosDetail(cafVo.getSpeedocId());
			if (posCposVo != null) {
				cafVo.setRtrCode(posCposVo.getRtrCode());
				cafVo.setRtrName(posCposVo.getRtrName());
			}
			IndexingDataVO electroDataVo=scrutinyPoolDao.getElectroData(cafVo.getPoiref());
			if(electroDataVo != null){
				cafVo.setVoterId(electroDataVo.getVoterId());
				cafVo.setPollingStation(electroDataVo.getPollingStation());
				cafVo.setVoterName(cafVo.getFirstName());
			}
			cafScrutinyVo = new CafScrutinyVo();
			Utils.copyProperties(cafScrutinyVo, cafVo);
			if(cafScrutinyVo !=null){
				if(sessionCafDaoImpl.getSessionCafVoByCafId(cafScrutinyVo.getId())){
					log.info("caf already exist" + cafScrutinyVo);
					return getSlcSearchCaf(userVo,cafId);
				}
				cafScrutinyVo.setSessionId(session().getId());
				insertSessionCaf(userVo, cafScrutinyVo);
				log.info("cafScrutinyVo = " + cafScrutinyVo);
			}
			return cafScrutinyVo;
		} catch (Exception e) {
			log.error("Error in ScrutinyManager :: getSlcSearchCaf :: "+e.getMessage(),e);
		}
		return cafScrutinyVo;
	}

	// caf_status_tbl update for slc search
	public Boolean updateCafStatusLatestToOldSearch(boolean latestInd,
			Long cafTblId, Long userId) {
		Boolean result=cafStatusDao.updateCafStatusLatestToOldSearch(latestInd, cafTblId,userId);
		log.info("ScrutinyManager :: getSlcSearchCaf :: result :: "+result);
		return result;
	}

	// cancel slc page
	public boolean cancelCaf(String cafId, UserVo userVo) {
		try {
			Long cafTblId = Long.valueOf(cafId);
			scrutinyPoolDao.cancelCaf(cafTblId);
			scrutinyPoolDao.deleteFromSlcPoolTbl(cafTblId);
			removeCaf(Long.parseLong(cafId));
		} catch (Exception e) {
			log.error("Error in ScrutinyManager :: cancelCaf :: "+e.getMessage(),e);
			removeCaf(Long.parseLong(cafId));
		}
		return true;
	}

	public List<StatusResultVo> getStatusList(StatusResultVo statusResultVo) {
		List<StatusResultVo> statusResultVos=scrutinyPoolDao.getStatusList(statusResultVo);
			for(StatusResultVo stResultVo : statusResultVos){
				CafVo cafVo = scrutinyPoolDao.getCafData(stResultVo.getCafTblId());
				stResultVo.setActivationDate(cafVo.getActivationDate());
			}
		return statusResultVos;
	}

	// slc search save method
	public Boolean searchSave(CafScrutinyVo cafScrutinyVo, UserVo userVo) {
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false and current_accessing_user_id null)
				
			if(scrutinyPoolDao.cafNotDeComplete(cafScrutinyVo.getId())){
				boolean bool = cafStatusDao.updateCurrentUserId(cafScrutinyVo.getId());
				log.info("Scrutiny Rows Updated  in updateCafStatus ::"
						+ bool);
			
				// Insert status to slc accepted
				CafStatusVo cafStatusVo = new CafStatusVo();
				cafStatusVo.setCafTblId(cafScrutinyVo.getId());
				cafStatusVo.setLatestInd(true);			
				cafStatusVo.setCreatedBy(userVo.getId());
				cafStatusVo.setStatusId(StatusCodeEnum.SCRUTINY_COMPLETE.getStatusCode());
				cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
				log.info("Saved Status Scrutiny Accepted  with id ::"+ cafStatusVo.getId());
			}else{
				scrutinyPoolDao.cancelCaf(cafScrutinyVo.getId());
				removeCaf(cafScrutinyVo.getId());
				return false;
			}
			// Delete the images
			String[] imageNames = cafScrutinyVo.getImages1().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
					}
				}
			}
			removeCaf(cafScrutinyVo.getId());
			return true;
		} catch (Exception e) {
			log.error("Error in save() of ScrutinyManager :: " + e.getMessage(),e);
			removeCaf(cafScrutinyVo.getId());
			return false;
		}
	}

	// slc search reject method 
	public Boolean rejectSearchSlc(String slcremak,
			CafScrutinyVo cafScrutinyVo, UserVo userVo) {
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false)
				
			if(!scrutinyPoolDao.cafNotDeComplete(cafScrutinyVo.getId())){
				boolean bool = cafStatusDao.updateCurrentUserId(cafScrutinyVo.getId());
				log.info("Scrutiny Rows Updated  in updateCafStatus ::" + bool);
				// Add status
				log.info("rejectSlc cafScrutinyVo.getId() = "
						+ cafScrutinyVo.getId());
				CafStatusVo cafStatusVo = new CafStatusVo();
				cafStatusVo.setCafTblId(cafScrutinyVo.getId());
				cafStatusVo.setLatestInd(true);
				cafStatusVo.setStatusId(StatusCodeEnum.SCRUTINY_REJECT.getStatusCode());
				cafStatusVo.setCreatedBy(userVo.getId());
				cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
				log.info("Saved rejectSlc Status Rejected with id ::" + cafStatusVo.getId());
	
				String[] rejectReasonArr = slcremak.split(",");
				// Save remarks in Remark Tbl
				for (int i = 0; i < rejectReasonArr.length; i++) {
					SlcRemarkVo slcRemarkVo = new SlcRemarkVo();
					slcRemarkVo.setCafTblId(cafScrutinyVo.getId());
					slcRemarkVo.setRemarkId(Long.parseLong(rejectReasonArr[i]
							.trim()));
					slcRemarkVo.setUserId(userVo.getId());
					slcRemarkVo.setStatusId(cafStatusVo.getId());
					Long id = slcRemarkDao.save(slcRemarkVo);
					log.info("Saved rejectSlc with id ::" + id);
				}
			}else{
				scrutinyPoolDao.cancelCaf(cafScrutinyVo.getId());
				removeCaf(cafScrutinyVo.getId());
				return false;
			}
			// Delete the images
			String[] imageNames = cafScrutinyVo.getImages1().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
					}
				}
			}
			removeCaf(cafScrutinyVo.getId());
			return true;
		} catch (Exception e) {
			log.error("Error in rejectSlc() of ScrutinyManager :: "+ e.getMessage(), e);
			removeCaf(cafScrutinyVo.getId());
			return false;
		}
	}
	
	private void insertSessionCaf(UserVo userVo, CafScrutinyVo cafScrutinyVo){
		SessionCafVo sessionCafVo = new SessionCafVo();
		sessionCafVo.setSessionId(cafScrutinyVo.getSessionId());
		sessionCafVo.setCafTblId(cafScrutinyVo.getId());
		sessionCafVo.setMobileNumber(cafScrutinyVo.getMobileNumber());
		sessionCafVo.setUserId(userVo.getId());
		sessionCafVo.setStatusId(4001L);
		sessionCafVo.setCreatedDate(new Timestamp(new Date().getTime()));
		sessionCafDaoImpl.insertSessionCaf(sessionCafVo);	
		System.out.println("Get caf Status ID :: "+cafScrutinyVo.getCafStatusId());

	}
	private void removeCaf(Long cafTblId) {
		log.info("INSIDE removeCaf");
		sessionCafDaoImpl.deleteSessionCaf(cafTblId);
	}
}

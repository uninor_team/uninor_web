package speedoc.co.in.service;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.ScanManager;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.ScanClientInfoVO;

@Service
public class ScanService {

	private static Logger logger = Logger.getLogger(ScanService.class);

	@Autowired
	ScanManager scanManager;

	@Value("${POST_URL}")
	public String postUrl;

	@Value("${HOST_URL}")
	public String hostUrl;



	public ScanManager getScanManager() {
		return scanManager;
	}

	public void setScanManager(ScanManager scanManager) {
		this.scanManager = scanManager;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public ServiceResponse<ScanClientInfoVO> getScanClientInfo(
			ScanClientInfoVO scanClientInfoVO) {
		ServiceResponse<ScanClientInfoVO> response = new ServiceResponse<ScanClientInfoVO>();
		ScanClientInfoVO scanClientInfoVO1 = scanManager
				.getScanClientInfo(scanClientInfoVO);
		if (scanClientInfoVO1 != null) {
			response.setSuccessful(true);
			response.setResponse(scanClientInfoVO1);
		} else {
			response.setSuccessful(false);
			response.setMessage("Information Not match.");
		}
		return response;
	}

	public ServiceResponse<String> savePDF(InputStream in, String fileName) {
		ServiceResponse<String> serviceResponse = new ServiceResponse<String>();
		try {
			String saveScanCaf = scanManager.savePDF(in, fileName);
			if (saveScanCaf == "success") {
				serviceResponse.setSuccessful(true);
				serviceResponse.setMessage(saveScanCaf);
			} else if (saveScanCaf == "already_exists") {
				serviceResponse.setSuccessful(true);
				serviceResponse.setMessage(saveScanCaf);
			} else if (saveScanCaf == "failure") {
				serviceResponse.setSuccessful(false);
				serviceResponse.setMessage(saveScanCaf);
			}
		} catch (Exception e) {
			serviceResponse.setSuccessful(false);
			logger.error("ScanService :: saveScanCaf :: " + e.getMessage());
		}
		return serviceResponse;
	}

	@Override
	public String toString() {
		return "ScanService [scanManager=" + scanManager + ", postUrl="
				+ postUrl + ", hostUrl=" + hostUrl + "]";
	}

	

}

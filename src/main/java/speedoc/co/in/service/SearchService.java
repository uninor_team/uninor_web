package speedoc.co.in.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.SearchManager;
import speedoc.co.in.vo.SearchResultVo;

@Service
public class SearchService implements ISearchService{
	
	@Autowired
	SearchManager searchManager;

	@Override
	public List<SearchResultVo> getSearchDataByMobileNumber(String mobileNumber) {
		return searchManager.getSearchDataByMobileNumber(mobileNumber);
	}

	@Override
	public List<SearchResultVo> getSearchDataBySpeedocId(String speedocId) {
		return searchManager.getSearchDataBySpeedocId(speedocId);
	}
	
}

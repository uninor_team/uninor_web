package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

public interface IAuditService {

	ServiceResponse<CafAuditVo> getSlcRejectedCafs(UserVo userVo);

	ServiceResponse<String> save(CafAuditVo cafAuditVo, UserVo userVo);

	ServiceResponse<CaoVo> getCaoImage(String caoCode);

	void getCafFile(String fileName, OutputStream out);

	ServiceResponse<String> rejectAudit(String auditremak,
			CafAuditVo cafAuditVo, UserVo userVo);

	ServiceResponse<List<RemarkVo>> getAuditRejectReasons();

	ServiceResponse<CafAuditVo> getCafData(Long long1);

}

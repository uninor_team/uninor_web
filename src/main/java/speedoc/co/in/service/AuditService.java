package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.AuditManager;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Service
public class AuditService implements IAuditService{
	
	@Autowired
	private AuditManager auditManager;

	private static Logger logger=Logger.getLogger(AuditService.class);
	@Override
	public ServiceResponse<CafAuditVo> getSlcRejectedCafs(UserVo userVo) {
		CafAuditVo cafAuditVo =  auditManager.getSlcRejectedCafs(userVo); 
		
		ServiceResponse<CafAuditVo> serviceResponse = new ServiceResponse<CafAuditVo>();
		if (cafAuditVo != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(cafAuditVo);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO Caf found.");
		}
		return serviceResponse;
	}

	@Override
	public ServiceResponse<String> save(CafAuditVo cafAuditVo, UserVo userVo) {
		ServiceResponse<String> rs = new ServiceResponse<String>() ;
		Boolean bool = auditManager.save(cafAuditVo,userVo);

		if(bool != null && bool){
			rs.setSuccessful(bool);
		}else{
			rs.setSuccessful(false);
		}
		return rs ;
	}

	@Override
	public ServiceResponse<CaoVo> getCaoImage(String caoCode) {
		ServiceResponse<CaoVo> serviceResponse = new ServiceResponse<CaoVo>();
		CaoVo caoVo = auditManager.getCaoImage(caoCode);
		if (caoVo != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(caoVo);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO  Image found.");
		}
		return serviceResponse;
	}

	@Override
	public void getCafFile(String fileName, OutputStream out) {
		auditManager.getCafFile(fileName, out);		
	}

	@Override
	public ServiceResponse<String> rejectAudit(String auditremak,
			CafAuditVo cafAuditVo, UserVo userVo) {
		ServiceResponse<String> rs = new ServiceResponse<String>() ; 
		Boolean bool = auditManager.rejectAudit(auditremak,cafAuditVo,userVo);
		if(bool){
			rs.setSuccessful(true);
		}else{
			rs.setSuccessful(false);
		}
		return rs ;
	}

	@Override
	public ServiceResponse<List<RemarkVo>> getAuditRejectReasons() {
		ServiceResponse<List<RemarkVo>> sr = new ServiceResponse<List<RemarkVo>>();
		List<RemarkVo> remarkVos = auditManager.getAuditRejectReasons() ;
		if(remarkVos != null && remarkVos.size() > 0){
			sr.setSuccessful(true) ;
			sr.setResult(remarkVos) ;
		}else{
			sr.setSuccessful(false) ;
			sr.setResult(remarkVos) ;
		}
		return sr ;
	}

	@Override
	public ServiceResponse<CafAuditVo> getCafData(Long cafTblId) {
		ServiceResponse<CafAuditVo> response = new ServiceResponse<CafAuditVo>();
		CafAuditVo cafAuditVo =auditManager.getCafData(cafTblId);
		logger.info("getCafData :: cafAuditVo :: "+cafAuditVo);
		if(cafAuditVo == null){
			response.setSuccessful(false);
			response.setMessage("Caf is in process. Try after some time");
		}else{
			response.setSuccessful(true);
			response.setResult(cafAuditVo);
		}
		return response;
	}

}

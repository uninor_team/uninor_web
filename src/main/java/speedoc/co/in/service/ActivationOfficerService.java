package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.ActivationOfficerManager;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;

@Service
public class ActivationOfficerService implements IActivationOfficerService{
	
	@Autowired
	ActivationOfficerManager activationOfficerManager;
	
	public CaoVo saveActivationOfficer(CaoVo activationOfficerVo){
		return activationOfficerManager.saveActivationOfficer(activationOfficerVo);
	}

	@Override
	public ServiceResponse<GridDataVo<List<CaoVo>>> getActivationList(
			FilterVo<CaoVo> filterVo) {
		ServiceResponse<GridDataVo<List<CaoVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<CaoVo>>>();
		GridDataVo<List<CaoVo>> gridDataVo = new GridDataVo<List<CaoVo>>();
		List<CaoVo> activationOfficerVos = activationOfficerManager.getActivationList(filterVo);
		gridDataVo.setResult(activationOfficerVos);		
		Long count = activationOfficerManager.getActivationCount();
		gridDataVo.setRecords(count.toString());
		if (filterVo.getRowsPerPage() == activationOfficerVos.size()) {
			gridDataVo.setHasMorePages(true);
		} else {
			gridDataVo.setHasMorePages(false);
		}
		gridDataVo.setMaxPageNumber((int)Math.floor(count/filterVo.getRowsPerPage()+ (count%filterVo.getRowsPerPage() >= 0 ? 1 : 0)));
		gridDataVo.setMaxRowCount(count);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}

	@Override
	public Boolean isCaoCodeAlreadyExist(String caoCode) {		
		return activationOfficerManager.isCaoCodeAlreadyExist(caoCode);
	}

	@Override
	public ServiceResponse<CaoVo> getSignatureById(Long id) {
		ServiceResponse<CaoVo> response = new ServiceResponse<CaoVo>() ;
		response.setResponse(activationOfficerManager.getSignatureById(id) );
		return response ;
	}
	
	@Override
	public boolean changeCaoStatus(Long id,int status) {
		
		return activationOfficerManager.changeCaoStatus(id,status);
		
	}
	

	@Override
	public void getSignFile(String fileName, OutputStream out) {
	activationOfficerManager.getSignFile(fileName,out);
	}
}

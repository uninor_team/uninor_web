package speedoc.co.in.service;

import org.springframework.stereotype.Service;

import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

@Service
public class SessionService {
	
	private String sessionId;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	private UserVo userVo ;
	private RoleVo roleVo ;
	
	public UserVo getUserVo() {
		return userVo;
	}
	public void setUserVo( final UserVo userVo) {
		this.userVo = userVo;
	}
	public RoleVo getRoleVo() {
		return roleVo;
	}
	public void setRoleVo(RoleVo roleVo) {
		this.roleVo = roleVo;
	}
	@Override
	public String toString() {
		return "SessionService [sessionId=" + sessionId + ", userVo=" + userVo
				+ ", roleVo=" + roleVo + "]";
	}
}

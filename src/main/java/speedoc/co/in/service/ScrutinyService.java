package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.FlcManager;
import speedoc.co.in.manager.ScrutinyManager;
import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

@Service
public class ScrutinyService implements IScrutinyService{

	@Autowired
	private ScrutinyManager scrutinyManager ;
	
	@Autowired
	private FlcManager flcPoolManager ;
	
	public ServiceResponse<CafScrutinyVo> getDECompletedCafs(UserVo userVo){
		CafScrutinyVo cafScrutinyVo =  scrutinyManager.getDECompletedCafs(userVo); 
		
		ServiceResponse<CafScrutinyVo> serviceResponse = new ServiceResponse<CafScrutinyVo>();
		if (cafScrutinyVo != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(cafScrutinyVo);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO Caf found.");
		}
		return serviceResponse;
	}
	
	public ServiceResponse<String> save(CafScrutinyVo cafScrutinyVo,UserVo userVo){
		ServiceResponse<String> rs = new ServiceResponse<String>() ;
		Boolean bool = scrutinyManager.save(cafScrutinyVo,userVo);

		if(bool != null && bool){
			rs.setSuccessful(bool);
		}else{
			rs.setSuccessful(false);
		}
		return rs ;
	}
	
	public ServiceResponse<String> rejectSlc(String remark,CafScrutinyVo cafScrutinyVo,UserVo userVo) {
		ServiceResponse<String> rs = new ServiceResponse<String>() ; 
			Boolean bool = scrutinyManager.rejectSlc(remark,cafScrutinyVo,userVo);
			if(bool){
				rs.setSuccessful(true);
			}else{
				rs.setSuccessful(false);
			}
			return rs ;
	}
	
	public ServiceResponse<CaoVo> getCaoImage(String caoCode) {
		ServiceResponse<CaoVo> serviceResponse = new ServiceResponse<CaoVo>();
		CaoVo caoVo = scrutinyManager.getCaoImage(caoCode);
		if (caoVo != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(caoVo);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO  Image found.");
		}
		return serviceResponse;
		
	}
	
	public void getCafFile(String fileName, OutputStream out) {
		scrutinyManager.getCafFile(fileName, out);
	}
	
	public ServiceResponse<List<RemarkVo>> getSlcRejectReasons(){
		ServiceResponse<List<RemarkVo>> sr = new ServiceResponse<List<RemarkVo>>();
		List<RemarkVo> remarkVos = flcPoolManager.getSlcRejectReasons() ;
		if(remarkVos != null && remarkVos.size() > 0){
			sr.setSuccessful(true) ;
			sr.setResult(remarkVos) ;
		}else{
			sr.setSuccessful(false) ;
			sr.setResult(remarkVos) ;
		}
		return sr ;
	}

	@Override
	public ServiceResponse<List<RemarkVo>> getRejectReasons(String header) {
		ServiceResponse<List<RemarkVo>> sr = new ServiceResponse<List<RemarkVo>>();
		List<RemarkVo> remarkVos = flcPoolManager.getRejectReasons(header) ;
		if(remarkVos != null && remarkVos.size() > 0){
			sr.setSuccessful(true) ;
			sr.setResult(remarkVos) ;
		}else{
			sr.setSuccessful(false) ;
			sr.setResult(remarkVos) ;
		}
		return sr ;
	}

	@Override
	public boolean findSpeedocIdExists(String speedocId) {
		return scrutinyManager.findSpeedocIdExists(speedocId);
	}
	
	@Override
	public List<SearchResultVo> getSlcSearchDataByMobileNumber(String mobileNumber, UserVo userVo) {
		return scrutinyManager.getSlcSearchDataByMobileNumber(mobileNumber, userVo);
	}

	@Override
	public List<SearchResultVo> getSlcSearchDataBySpeedocId(String speedocId, UserVo userVo) {
		return scrutinyManager.getSlcSearchDataBySpeedocId(speedocId, userVo);
	}
	
	@Override
	public List<SearchResultVo> getSlcSearchDataByCafNumber(String cafNumber, UserVo userVo){
		return scrutinyManager.getSlcSearchDataByCafNumber(cafNumber, userVo);
	}
	
	@Override
	public CafScrutinyVo getSlcSearchCaf(UserVo userVo, String cafId) {
		return scrutinyManager.getSlcSearchCaf(userVo, cafId);
	}

	@Override
	public Boolean updateCafStatusLatestToOldSearch(boolean latestInd, Long cafTblId, Long userId) {
		return scrutinyManager.updateCafStatusLatestToOldSearch(latestInd, cafTblId, userId);
	}

	@Override
	public boolean cancelCaf(String cafId, UserVo userVo) {
		return scrutinyManager.cancelCaf(cafId,userVo);
	}

	@Override
	public List<StatusResultVo> getStatusList(StatusResultVo statusResultVo) {
		return scrutinyManager.getStatusList(statusResultVo);
	}

	@Override
	public ServiceResponse<String> searchSave(CafScrutinyVo cafScrutinyVo,
			UserVo userVo) {
		ServiceResponse<String> rs = new ServiceResponse<String>() ;
		Boolean bool = scrutinyManager.searchSave(cafScrutinyVo,userVo);

		if(bool){
			rs.setSuccessful(true);
		}else{
			rs.setSuccessful(false);
			rs.setMessage("Caf current status is not de complete.");
		}
		return rs ;
	}

	@Override
	public ServiceResponse<String> rejectSearchSlc(String slcremak,
			CafScrutinyVo cafScrutinyVo, UserVo userVo) {
		ServiceResponse<String> rs = new ServiceResponse<String>() ; 
		Boolean bool = scrutinyManager.rejectSearchSlc(slcremak,cafScrutinyVo,userVo);
		if(bool){
			rs.setSuccessful(true);
		}else{
			rs.setSuccessful(false);
			rs.setMessage("Caf current status is not de complete.");
		}
		return rs ;
	}
}











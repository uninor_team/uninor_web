package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import speedoc.co.in.vo.CafScrutinyVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

public interface IScrutinyService {

	public ServiceResponse<CafScrutinyVo> getDECompletedCafs(UserVo userVo) ;
	public ServiceResponse<String> save(CafScrutinyVo cafScrutinyVo, UserVo userVo) ;
	public ServiceResponse<String> rejectSlc(String remark,CafScrutinyVo cafScrutinyVo, UserVo userVo)  ;
	public ServiceResponse<CaoVo> getCaoImage(String caoCode)  ;
	public void getCafFile(String fileName, OutputStream out)  ;
	public ServiceResponse<List<RemarkVo>> getSlcRejectReasons() ;
	public ServiceResponse<List<RemarkVo>> getRejectReasons(String header);
	public boolean findSpeedocIdExists(String speedocId);
	public List<SearchResultVo> getSlcSearchDataByMobileNumber(String mobileNumber , UserVo userVo);
	public List<SearchResultVo> getSlcSearchDataBySpeedocId(String speedocId, UserVo userVo);
//	public String getScrutinyRotatePdf(String fileName, int pageNo); 
	List<SearchResultVo> getSlcSearchDataByCafNumber(String cafNumber, UserVo userVo);
	public CafScrutinyVo getSlcSearchCaf(UserVo userVo, String cafId);
	public Boolean updateCafStatusLatestToOldSearch(boolean b, Long id, Long id2);
	public boolean cancelCaf(String cafId, UserVo userVo);
	public List<StatusResultVo> getStatusList(StatusResultVo statusResultVo);
	public ServiceResponse<String> searchSave(CafScrutinyVo cafScrutinyVo,
			UserVo userVo);
	public ServiceResponse<String> rejectSearchSlc(String slcremak,
			CafScrutinyVo safScrutinyVo, UserVo userVo);
}

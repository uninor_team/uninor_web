package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;

public interface IActivationOfficerService {
	public CaoVo saveActivationOfficer(CaoVo caoVo);
	public ServiceResponse<GridDataVo<List<CaoVo>>> getActivationList(
			FilterVo<CaoVo> filterVo);
	public Boolean isCaoCodeAlreadyExist(String caoCode);
	public ServiceResponse<CaoVo> getSignatureById(Long id);
	public void getSignFile(String fileName, OutputStream out);	 
	public boolean changeCaoStatus(Long id, int status);  
}

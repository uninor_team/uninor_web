package speedoc.co.in.service;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.SlcUploadManager;
import speedoc.co.in.util.CAFUtils;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcImportVo;

@Service
public class SlcUploadService implements ISlcUploadService{
	
	@Autowired
	private SlcUploadManager slcUploadManager;
	
	@Value("${SLC_INPROCESS_FILE_PATH}")
	private String slcInprocessDataPath;

	@Value("${SLC_COMPLETE_FILE_PATH}")
	private String slcCompletedDataPath;
	
	private static Logger logger=Logger.getLogger(SlcUploadService.class);

	@Override
	public ServiceResponse<GridDataVo<List<SlcImportVo>>> getSlcList(
			FilterVo<SlcImportVo> filterVo) {
		ServiceResponse<GridDataVo<List<SlcImportVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<SlcImportVo>>>();
		GridDataVo<List<SlcImportVo>> gridDataVo = new GridDataVo<List<SlcImportVo>>();
		Long count = null;
		List<SlcImportVo> slcImportVos = slcUploadManager.getSlcList(filterVo);
		if(slcImportVos.size() > 0){
			gridDataVo.setResult(slcImportVos);	
			count = slcUploadManager.getSlcCount();
			gridDataVo.setRecords(count.toString());
		}	
		
		if (filterVo.getRowsPerPage() == slcImportVos.size()) {
			gridDataVo.setHasMorePages(true);
		} else {
			gridDataVo.setHasMorePages(false);
		}
		gridDataVo.setMaxPageNumber((int)Math.floor(count/filterVo.getRowsPerPage()+ (count%filterVo.getRowsPerPage() >= 0 ? 1 : 0)));
		gridDataVo.setMaxRowCount(count);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}
	
	@SuppressWarnings("rawtypes")
	public boolean readSlcCSVData() {		
		logger.info("inside reading data from schedular of readSlcUploadCsvData...");
		Collection<File> slcFiles = FileUtils.listFiles(new File(slcInprocessDataPath), new String[]{"csv"}, true);
		logger.info("Total file found = " + slcFiles.size());
		for (Iterator iterator = slcFiles.iterator(); iterator.hasNext();) {
			File file = (File) iterator.next();
			boolean readSlcExcelData = slcUploadManager.readSlcExcelData(file.getPath());
			try {
				if(readSlcExcelData == true){
					CAFUtils.moveFile1(file.getPath(), slcCompletedDataPath + File.separator + FilenameUtils.getName(file.getPath()));
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return true;
	}
	
	public Integer uploadSlcFile(){
		 Integer uploadSlcFile = slcUploadManager.uploadSlcFile();
		 logger.info("uploadSlcFile :: "+uploadSlcFile);
		return uploadSlcFile;
	}

}

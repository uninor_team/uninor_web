package speedoc.co.in.service;

import java.util.List;

import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcImportVo;

public interface ISlcUploadService {

	ServiceResponse<GridDataVo<List<SlcImportVo>>> getSlcList(
			FilterVo<SlcImportVo> filterVo);

}

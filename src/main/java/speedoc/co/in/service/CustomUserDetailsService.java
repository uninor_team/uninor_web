package speedoc.co.in.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.manager.UserManager;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
	protected static Logger logger = Logger
			.getLogger(CustomUserDetailsService.class);

	@Autowired
	private UserManager userAuthenticationManager;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		UserDetails userDetails = null;

		try {
			logger.info("loadUserByUsername : username : " + username);
			logger.info("loadUserByUsername : userAuthenticationManager object : " + userAuthenticationManager);
			UserVo userVo = userAuthenticationManager
					.findByUserName(username);

			userDetails = new User(userVo.getUsername(),
					userVo.getPwd(),
					userVo.getEnabled(), true, true, true,
					getAuthorities(userVo.getRoleList()));

		} catch (Exception e) {
			logger.error("Error in retrieving user : " + e.getMessage());
			throw new UsernameNotFoundException("Error in retrieving user");
		}
		return userDetails;
	}

	public Collection<GrantedAuthority> getAuthorities(List<RoleVo> roleList) {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

		for (RoleVo roleVo : roleList) {
			authList.add(new SimpleGrantedAuthority(roleVo.getRoleType()));
			logger.info("getAuthorities() : Role : " + roleVo.getRoleType());
		}

		return authList;
	}

	public void setUserAuthenticationManager(UserManager userAuthenticationManager) {
		this.userAuthenticationManager = userAuthenticationManager;
	}
}
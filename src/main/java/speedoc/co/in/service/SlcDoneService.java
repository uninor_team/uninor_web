package speedoc.co.in.service;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.SlcDoneManager;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcDoneFileVo;

@Service
public class SlcDoneService implements ISlcDoneService{
	
	@Autowired
	SlcDoneManager slcDoneManager;
	
	@Value("${SLC_DONE_INPROCESS_FILE_PATH}")
	private String slcDoneInprocessFilePath;
	
	private static Logger logger=Logger.getLogger(SlcDoneService.class);

	@Override
	public ServiceResponse<GridDataVo<List<SlcDoneFileVo>>> getSlcDoneList(FilterVo<SlcDoneFileVo> filterVo, String month) {
		ServiceResponse<GridDataVo<List<SlcDoneFileVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<SlcDoneFileVo>>>();
		GridDataVo<List<SlcDoneFileVo>> gridDataVo = new GridDataVo<List<SlcDoneFileVo>>();
		List<SlcDoneFileVo> slcDoneFileVos = slcDoneManager.getSlcDoneMonthWiseList(filterVo,month);
		gridDataVo.setResult(slcDoneFileVos);	
		Long count = slcDoneManager.getSlcDoneCount();
		gridDataVo.setRecords(count.toString());
		if (filterVo.getRowsPerPage() == slcDoneFileVos.size()) {
			gridDataVo.setHasMorePages(true);
		} else {
			gridDataVo.setHasMorePages(false);
		}
		gridDataVo.setMaxPageNumber((int)Math.floor(count/filterVo.getRowsPerPage()+ (count%filterVo.getRowsPerPage() >= 0 ? 1 : 0)));
		gridDataVo.setMaxRowCount(count);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}
	
	//read data slc done csv upload file
	@SuppressWarnings("rawtypes")
	public boolean readSlcDoneCSVData(){
		logger.info("inside reading data from schedular of readSlcDoneCSVData...");
		Collection<File> slcFiles = FileUtils.listFiles(new File(slcDoneInprocessFilePath), new String[]{"csv"}, true);
		logger.info("Total file found = " + slcFiles.size());
		// iterate file
		for (Iterator iterator = slcFiles.iterator(); iterator.hasNext();) {
			File file = (File) iterator.next();
			boolean readSlcDoneCSVData = slcDoneManager.readSlcDoneCSVData(file.getPath());
			logger.info("readSlcDoneCSVData :: "+readSlcDoneCSVData);
			try {
				if(readSlcDoneCSVData == true){
					logger.info("inside readSlcDoneCSVData");
				}
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		return true;
	}
	
	// upload file and update caf_status_tbl 
	public Integer uploadSlcDoneFile(){
		 Integer uploadSlcFile = slcDoneManager.uploadSlcDoneFile();
		 logger.info("uploadSlcFile :: "+uploadSlcFile);
		return uploadSlcFile;
	}
	
	//clear all currently accessing user id and end_date and latest ind
	public boolean clearAllCurrentAccessList(){
		boolean b=slcDoneManager.clearAllCurrentAccessList();
		return b;
	}
}

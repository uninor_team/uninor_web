package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import speedoc.co.in.manager.FlcManager;
import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Service
public class FlcService implements IFlcService{
	
	@Autowired
	private FlcManager flcPoolManager ;
	
	public ServiceResponse<CafFlcVo> getUnknownCaf(UserVo userVo){
		CafFlcVo cafFlcVo =  flcPoolManager.getUnknownCaf(userVo); 
		ServiceResponse<CafFlcVo> serviceResponse = new ServiceResponse<CafFlcVo>();
		if (cafFlcVo != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(cafFlcVo);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO Caf found.");
		}
		return serviceResponse;
	}
	
	public ServiceResponse<String> getCafImageNames(String cafFileName) {
		ServiceResponse<String> serviceResponse = new ServiceResponse<String>();
		String fileNames = flcPoolManager.getCafImageNames(cafFileName, false);
		if (fileNames != null) {
			serviceResponse.setSuccessful(true);
			serviceResponse.setResult(fileNames);
		} else {
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("NO Caf found.");
		}
		return serviceResponse;
	}
	
	public void getCafFile(String fileName, OutputStream out) {
		flcPoolManager.getCafFile(fileName, out);
	}
	
	public ServiceResponse<CafFlcVo> save(CafFlcVo cafFlcVo,UserVo userVo){
		ServiceResponse<CafFlcVo> rs = new ServiceResponse<CafFlcVo>() ;
		Boolean bool = flcPoolManager.save(cafFlcVo,userVo);

		if(bool != null && bool){
			rs.setSuccessful(bool);
		}else{
			rs.setSuccessful(false);
		}
		return rs ;
	}
	
	public ServiceResponse<String> skipFlc(CafFlcVo cafFlcVo,UserVo userVo){
		ServiceResponse<String> rs = new ServiceResponse<String>() ;
		Boolean bool = flcPoolManager.skipFlc(cafFlcVo,userVo);
		if(bool){
			rs.setSuccessful(true);
		}else{
			rs.setSuccessful(false);
		}
		return rs ;
	}
		
	public ServiceResponse<String> rejectFlc(String remark,CafFlcVo cafFlcVo,UserVo userVo){
		ServiceResponse<String> rs = new ServiceResponse<String>() ; 
			Boolean bool = flcPoolManager.rejectFlc(remark, cafFlcVo,userVo);
			if(bool){
				rs.setSuccessful(true);
			}else{
				rs.setSuccessful(false);
			}
			return rs ;
	}
	
	public ServiceResponse<List<RemarkVo>> getRejectReasons(){
		ServiceResponse<List<RemarkVo>> sr = new ServiceResponse<List<RemarkVo>>();
		List<RemarkVo> remarkVos = flcPoolManager.getRejectReasons() ;
		if(remarkVos != null && remarkVos.size() > 0){
			sr.setSuccessful(true) ;
			sr.setResult(remarkVos) ;
		}else{
			sr.setSuccessful(false) ;
			sr.setResult(remarkVos) ;
		}
		return sr ;
	}

	@Override
	public boolean checkSpeedocIdExist(String speedocId) {		
		return flcPoolManager.checkSpeedocIdExist(speedocId);
	}

	@Override
	public void updateCafTempPool(UserVo userVo) {
//		flcPoolManager.updateCafTempPool(userVo);
	}

	@Override
	public Boolean updateCafStatus(UserVo userVo) {
		return flcPoolManager.updateCafStatus(userVo);
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}

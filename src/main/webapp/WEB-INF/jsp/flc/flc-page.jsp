<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>	

<html>
<head>
<meta http-equiv="Content-path" content="text/html; charset=ISO-8859-1">
<title>Flc</title>
<script src="<c:url value="/resources/scripts/flc/REPipe.js" />" type="text/javascript"></script>
<script src="<c:url value='/resources/scripts/flc/flc.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/uninor/PageRotation.js' />" ></script>

<c:url var="imageUrl" value="/resources/images/uninor/" />


<style>
#bg_image_photo {
	width: 250px;
	height: 300px;
	margin: 5px 5px 5px 5px;
}

#bg_image_signature {
	width: 350px;
	height: 150px;
	margin: 5px 5px 5px 5px;
}
#hiddenImageDiv{
	width:100%;
}
#hiddenImageDiv:hover {
    cursor: pointer;
    width:140%;
}
</style>
<style type="text/css">
#idletimeout { /*background:#CC5100;*/ /*border:3px solid #FF6500;*/
	color: #fff;
	font-family: arial, sans-serif;
	text-align: center;
	font-size: 12px;
	padding: 10px;
	position: relative;
	top: 0px;
	left: 0;
	right: 0;
	z-index: 2;
	display: none;
}

#idletimeout a {
	color: #fff;
	font-weight: bold
}

#idletimeout span {
	font-weight: bold
}
 a{
	text-decoration: none;
}
</style>
<script type="text/javascript">
	function checkSpeedocIdAlreadyExists() {
	  var speedocId = $("#speedocId").val(); 
	  	$.post("<c:url value='/flc/check-speedocId-exist' />",jQuery.param({speedocId:speedocId}), function(data) {
			if (data == "success") {	
				$("#speedocId").css("color","black");
// 				jAlert("Speedoc Id Already Exist.");
				$("#speedocId").val("");
				$("#speedocId").focus();
// 				$("#speedocId").css("color","black");
			}else{
				$("#speedocId").css("color","red");
				$("#speedocId").val(speedocId);
				jAlert("Speedoc Id Not Exist in Avam.");
				$("#speedocId").focus();			
			}
	  	});
	}
</script>
</head>

<body onload="SetFocus()">

	<!-- 	end body header -->

	<div id="int-container">
		<table style="width: 100%;">
			<tr align="center" valign="top">
				<td align="left" style="width: 78%;">
					<div class="plugin" style="width: 100%">						
					
						<div id="currentImgDiv" style="overflow: scroll; width: 100%; height: 500px;">
							<img src="" alt="" id="cafContainer" width="100%" />
						</div>
						<div id="hdiv" style="display: none; height: 0px"></div>
						<div id="rotateFlcHiddendiv" style="display: none; height: 0px"></div>
							
						
						
					</div>
				</td>

				<td align="left" style="width: 50%;border-style: solid;border-radius:10px;">

					<div class="smbox">
						<div class="smbox-cont">
							<div id="form">
								<table>
									<tr>
										<td class="tdheader">
											<form id="renameform">

												<div id="acceptoption1">
													<p style="padding-top:25px">
														<label style="font-size: 16px;"><b>Speedoc Id:</b></label> <span class="textbox-bg">											
															<input type="text" id="speedocId" name="speedocId" value="" 
															class="textbox" maxlength="9" tabindex="1" size= "21.9"
															onchange="checkSpeedocIdAlreadyExists()" style="font-size: 12px;"></input>
														</span>
													</p>
													
													<input id="cafId" type="hidden" name="cafId" />
													<input id="userId" type="hidden" name="userId" />
													<input id="cafStatusId" type="hidden" name="cafStatusId" />
													<input id="flcPoolId" type="hidden" name="flcPoolId" />
													<input id="currentStatus" type="hidden" name="currentStatus" />
													<input id="fileLocation" type="hidden" name="fileLocation" />
													<input id="remark1" type="hidden" name="remark" />
													<input id="images" type="hidden" name="images" />
														
													<p style="float: left; width: 269px;">
														<button type="button" id="rename" tabindex="5">Accept</button>
 														<button type="button" id="Reject" tabindex="6">Reject</button>
<!-- 														<button type="button" tabindex="7" id="cancel">Cancel</button> -->
	 													<button type="button" id="skip" tabindex="7">Skip</button>
													</p>

												</div>
											</form>
										</td>
									</tr>
									<tr>
										<td>
											<div id="rejectOpt"
												style="width: 100%; border: 0px solid red; overflow: auto; height: 300px">
												<form name="frmRadio" id="frmRadio">
													<table border="0">

													<c:forEach var="remarkVo1" items="${remarkVoList}">
														<tr>
															<td align="right" valign="top"><input
																type="checkbox"
																value="${remarkVo1.id}"
																name="remark"
																id="${remarkVo1.rejectReason}" /></td>
															<td><label style="width: 100%; margin-left: 4px; font-size: 12px;">${remarkVo1.rejectReason}</label></td>
														</tr>
													</c:forEach>
														<tr>
															<td></td>
 															<td><button id="rejectClose" type="button">OK</button></td>
														</tr>
													</table>
													</form>
											</div>

										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="smbox-bottom"></div>
					</div>
					<div class="smbox" style="display: none;">
						<div class="smbox-cont">
							<input id="pageNo" type="hidden" name="pageNo" /> <input
								id="cafIdRotate" type="hidden" name="cafIdRotate" />
						</div>
						<div class="smbox-bottom"></div>
					</div>
					<div class="clear"></div>
				</td>
			</tr>
			<tr>
				<td>
				<div> 
				<div>
					<form id="rotatecafflc" name="rotatecafflc">									
						<input id="fileName1" type="hidden" name="fileName"/>
						<input type="hidden" id="lblNoOfPage1" /> 
						<label style="font-size: 12px;">Page :</label> <select id="pageNo1" name="pageNo"></select>								   	
					</form>
					<button title="rotate" style=" margin-left: 15%; position:relative; bottom: 44px; float: left;" id="flcImageRotateBtn" align="top">Rotate</button>
				</div>							
				</div>
				</td>
			</tr>
		</table>
	</div>
	<!-- End Wrapper -->
	<div id="int-bottom">
		<span></span>
	</div>

</body>
</html>
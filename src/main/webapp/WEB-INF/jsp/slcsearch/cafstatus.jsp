<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CAF Status Viewer</title>
</head>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-1.10.0.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.2.1.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.2.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-ui-1.9.2.custom.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/json.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/JqueryConfirm.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery.validate.js" />"></script>
<script type="text/javascript" src='<c:url value="/resources/scripts/js/jquery.alerts.js"/>'></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/grid.locale-en.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery.jqGrid.min.js" />"></script>
<link href="<c:url value="/resources/styles/css/ui.jqgrid.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery-ui.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery-ui.min.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.ui.theme.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/style.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.alerts.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.confirm.css" />" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/jquery.maskedinput-1.3.js'/>"></script>
<script src="<c:url value="/resources/scripts/js/date.format.js" />"	type="text/javascript"></script>
<script type="text/javascript">
	
	function loadStatus(){
		$.postJSON("<c:url value='/slc/getstatus?cafId='/>"+$("#cafId").val(), null, function(data){
			var serviceResponse = data;
			//$.unblockUI();
			var statusResultVos = serviceResponse.result;
			if(serviceResponse.successful){
					displayResult(statusResultVos);
			}else{
				jAlert(serviceResponse.message);
			}
	});
	}
	
	function displayResult(statusResultVos){
		var htmlStr = "<table width='100%' cellspacing='2' cellpadding='0' border='0' align='center' id='cafStatusDisplay' style='margin-left: 0px;margin-right: 2px;'>";
		htmlStr += 	"<tr align='left' valign='middle' style='color: #43567E; height:54px; background-color: #FEFEFE;'>";
	
		htmlStr +=	"<th class='tbl-top'>Sr No.</th>";
		htmlStr +=	"<th class='tbl-top'>Status</th>";
		htmlStr +=	"<th class='tbl-top'>Remark</th>";
		htmlStr +=	"<th class='tbl-top'>Created By</th>";
		htmlStr +=	"<th class='tbl-top'>Created Date</th>";
		htmlStr +=	"<th class='tbl-top'>Activation Date</th>";	
		htmlStr += "</tr>";
		
		for ( var i = 0; i < statusResultVos.length; i++) {
			if(i%2==0){
				htmlStr +="<tr align='left' valign='middle' class='row-alter'>";
			}else{
				htmlStr +="<tr align='left' valign='middle' class='row'>";
			}
			
			htmlStr +="<td>"+(i+1)+"</td>";
			
			if(statusResultVos[i].status != null){
				htmlStr +="<td>"+statusResultVos[i].status+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(statusResultVos[i].remark != null){
				htmlStr +="<td>"+statusResultVos[i].remark+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(statusResultVos[i].createdBy != null){
				htmlStr +="<td>"+statusResultVos[i].createdBy+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}			
			if(statusResultVos[i].createdDate != null){
				htmlStr +="<td>"+getDate(statusResultVos[i].createdDate,true)+"</td>";
// 				htmlStr +="<td>"+statusResultVos[i].createdDate+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			
			if(statusResultVos[i].activationDate != null){
				htmlStr +="<td>"+getDate(statusResultVos[i].activationDate,false)+"</td>";
// 				htmlStr +="<td>"+statusResultVos[i].activationDate+"</td>";
			}else{
				htmlStr +="<td>NA</td>";	
			}
			htmlStr +="</tr>";
		}
		htmlStr += "</table>"; 	 	

		$("#resultDiv").html("");
		$("#resultDiv").html(htmlStr);
	}
	
	function getDate(date,withTime){
		var crtDate = new Date(date);
		if(withTime){
			return crtDate.format("d mmmm,yyyy HH:MM:ss");
		}else{
			return crtDate.format("d mmmm, yyyy");
		}
	}
</script>
<style>
	#cafStatusDisplay{
		margin-left: 2px;
		border:1px double #999999;
	}
	
	#form{
		margin-top: 20px;
	}
	
	.cbox-cont {
	  	background: none repeat scroll 0 0 #EFEFEF;
	    border-left: 1px solid #C1C7DD;
	    border-radius: 10px 10px 10px 10px;
	    border-right: 1px solid #C1C7DD;
	    margin-left: 0.5%;
	    position: relative;
	    width: 99%;
	}

	.content-header {
	    color: #43567E;
	    font-size: 0.9em;
	    font-weight: 700;
	    height: 30px;
	    padding: 0;
	    margin-left: 2%;
	}

	.content {
   	 	padding: 10px 0 0;
	}

	tr.row-alter td {
	    background: url("../resources/images/uninor/row-alter-bg.gif") repeat-x scroll 0 bottom #F7F9FC;
	    border-right: 2px solid #D5D9E7;
	    color: #43567E;
	    font-size: 0.75em;
	    padding: 7px 10px 13px;
	}

	.tbl-top {
	    background: url("../resources/images/uninor/tbl-top.gif") repeat-x scroll 0 0 transparent;
	    border-right: 2px solid #E7EAF2;
	    color: #43567E;
	    font-size: 0.9em;
	    padding-left: 5px;
	}

	tr.row td {
	   	background: none repeat scroll 0 0 #EEEEEE;
	    border-right: 2px solid #D5D9E7;
	    color: #43567E;
	    font-size: 0.75em;
	    padding: 7px 10px 13px;
	}
</style>
</head>
<c:url value="/resources/images/uninor/" var="imageUrl"></c:url>
<body onload="loadStatus()" style="margin: 0;">
	<div class="ui-widget ui-widget-header" style="height:70px;" >
		<img class="ui-image" src="${imageUrl}/logo.jpg" />
	</div>
	<div id="wrapper">
		<div id="int-container">
    		<div class="cbox-top"><span></span></div>
    		<div class="cbox-cont">
        		<div class="content-header">
            		<table width="100%">
                		<tr><td class="content-title" align="left" style="margin-left:2%; padding-top: 10px;">Status</td>   </tr>
            		</table>
        		</div>
        		<div class="content">
            		<div id="form">
		    			<table style="padding-left: 50px;" align="center">
							<tr><td><input type="hidden" value="${cafId}" id="cafId"></td></tr>
						</table>
						<div id='resultDiv'></div>
					</div>
				</div>
    		</div>
		</div>
   	 	<div class="cbox-bottom"><span></span></div>
	</div>
</body>
</html>
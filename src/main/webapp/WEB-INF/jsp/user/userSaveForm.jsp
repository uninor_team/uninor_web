<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:url var="imageUrl" value="/resources/images/uninor/" />

<script type="text/javascript">
	$(document).ready(function() {
		$("#btnSave").button();
		$("#btnReset").button();

		$.get('<c:url value="/userController/getusergrid" />',function(data){
			$("#displayGrid").html(data);
		});
		
	$("#savaForm").validate({
		rules: {
			userType:{ 
				required: true
			},
			username: {
				required: true,
				noSpecialChars: false,
				checkDuplicate: true
			},
			pwd: {
				required: true,
				minlength : 4,
				maxlength :20
			},
			firstName: {
				required: true,
				noSpecialChars: true
			},
			middleName: {
				required: true,
				noSpecialChars: true				
			},
			lastName: {
				required: true,
				noSpecialChars: true
			},
			address: {
				required: true,
				noSpecialChars: true,
			},
			city: {
				required: true,
				noSpecialChars: true,
			},
			state: {
				required: true,
				noSpecialChars: true
			},
			mobileNumber: {
				required: true,
				number: true,
				minlength : 10
			},
			emailAddress: {				
				email: true
			}
		},
		messages: {
			userType: {
				required :"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please Select User Type." 
			},
			username: { 
				required: "Please Enter Username.", 
				noSpecialChars: "Special characters not allowed.", 
				checkDuplicate: "Username Already Exists."
			},
			pwd: {
				required: "Please Enter Password.",
				minlength: "Required 4 Characters",
				maxlength: "Required 20 Characters"				
			},
			firstName: {
				required: "Please Enter First Name.",
				noSpecialChars: "Special characters not allowed."
			},
			middleName:{
				required: "Please Enter Middle Name.",
				noSpecialChars: "Special characters not allowed.",
			},
			lastName: {
				required: "Please Enter Last Name.",
				noSpecialChars: "Special characters not allowed."
			},
			mobileNumber: {
				required: "Please Enter Mobile Number.",
				number: "Please Enter number only",
				minlength: "Required 10 digits"
			},
			address: {
				required: "Please Enter Address.",
				noSpecialChars: "Special characters not allowed."
			},
			city: {
				required: "Please Enter City.",
				noSpecialChars: "Special characters not allowed."
			},
			state: {
				required: "Please Enter State.",
				noSpecialChars: "Special characters not allowed."
			},
			emailAddress: {
				email : "Enter a Valid Email Address"
			}
		},
		submitHandler: function(form) {  
	           var formData = $('#savaForm').serializeObject();       
	           if(validateForm()){
	        	   $(".serror").html("");
	        	   $.post('<c:url value="/userController/saveuser" />', formData, function(data) {		
		        		$('#displayGrid').html(data);						
						jAlert("User Save Successfully");
						$("#popup_content").css("background","white");						 
						$('#savaForm')[0].reset();										        		           																				      			           		
          		   });	
		       }	        	  	                           
	      }	
	});
		 	
		$.validator.addMethod("noSpecialChars", function(value, element) {
	      	return this.optional(element) || /^[a-z\_ 0-9]+$/i.test(value);
	  	}, "Username must contain only letters or underscore.");

		$.validator.addMethod("checkDuplicate",function(value, element) {
			var flag = true;
				$.ajaxSetup({async : false});
				$.post("<c:url value='/userController/checkUser' />",jQuery.param({username : value}),function(data) {
					if (data == "success") {
						flag = false;						
				}
			});
				
			return flag;
		}, "Username already exists.");

		$.validator.addMethod("alpha",function(value, element) {
			return this.optional(element)|| value == value.match(/^[a-z A-Z]+$/);
		}, "only Alphabets allowed");		
	});

	function validateForm(){
		if($("#userType").val() == "Scan User"){
			if($("#emailAddress").val() == null || $("#emailAddress").val() == "" ){
				$(".serror").html("Please Enter Email Address.");
				return false;
			}			
			return true;
		}else if($("#userType").val() == "Scrutiny User"){
			return true;
		}else if($("#userType").val() == "Audit User"){
			return true;
		}else if($("#userType").val() == "Flc User"){
			return true;
		}	
	}
</script>

<style type="text/css">
#savaForm label.error {
	margin-left: -2px;
	margin-top: 10px;
	width: 180px;
	display: inline;
	color: red;
	font-size: 12px;
}
</style>

<div id="wrapper">
		<div id="int-container">
			<div class="cbox-cont">				
				<div class="content">
					<div id="form"
						style="width: 55%; height: auto; border: 1px solid; padding-left: 17px;">
						<h3><label style="font-size:12px;">New User</label></h3>
						
						<form:form id="savaForm" name="saveform" modelAttribute="userVo">
							<table>
								<tr>
									<td width="150px"><label class="textlabel" style="font-size:12px;"><b>User
											Type :</b></label>
									</td>
									<td width="170px"><form:select id="userType"
											path="userType" style="font-size:12px;">
											<option value="" style="font-size:11px;">Select</option>
											<option value="Scan User" style="font-size:12px;">Scan User</option>
											<option value="Scrutiny User" style="font-size:12px;">Scrutiny User</option>
											<option value="Flc User" style="font-size:12px;">Flc User</option>
											<option value="Audit User" style="font-size:12px;">Audit User</option>
										</form:select></td>
								</tr>
								<tr>
									<td nowrap="nowrap" colspan="4"><c:if
											test="${msg=='USER_CREATED_STATUS'}">
											<p>
												<label class="textlabel">${msg1}</label>
											</p>
										</c:if></td>
								</tr>
								<tr>
									<td><label style="font-size:12px;"><b>User Name :</b></label>
									</td>
									<td><span class="textbox-bg" style="margin-right: 20px;"><form:input
												type="text" path="username" id="username" name="username" 
												class="textbox" style="font-size:12px;"/>
									</span>
									</td>
									<td width="150px" style="font-size:12px;"><label><b>Password :</b></label>
									</td>
									<td width="170px"><span><form:input type="password" style="font-size:12px;"
												path="pwd" id="pwd" minlength="4" maxlength="20" class="textbox" />
									</span>
									</td>
								</tr>
								<tr>
									<td><label style="font-size:12px;"><b>First Name :</b></label>
									</td>
									<td><span><form:input type="text" path="firstName" style="font-size:12px;"
												id="firstName" name="firstName"  class="textbox" />
									</span>
									</td>
									<td><label style="font-size:12px;"><b>Middle Name :</b></label>
									</td>
									<td><span><form:input type="text" path="middleName" style="font-size:12px;"
												id="middleName" name="middleName"  class="textbox" />
									</span>
									</td>
								</tr>
								<tr>
									<td><label style="font-size:12px;"><b>Last Name :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="lastName" id="lastName" name="lastName" 
												class="textbox" style="font-size:12px;"/>
									</span>
									</td>
									<td><label style="font-size:12px;"><b>Address :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="address" id="address" name="address" 
												class="textbox" style="font-size:12px;"/>
									</span>
									</td>
								</tr>
								<tr>
									<td><label style="font-size:12px;"><b>City :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="city" id="city" name="city"
												class="textbox" style="font-size:12px;"/>
									</span>
									</td>
									<td><label style="font-size:12px;"><b>State :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="state" id="state" name="state"
												class="textbox" style="font-size:12px;"/>
									</span>
									</td>
								</tr>
								<tr>
									<td><label style="font-size:12px;"><b>Mobile Number :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="mobileNumber" id="mobileNumber" name="mobileNumber"
												class="textbox" maxlength="10" style="font-size:12px;"/>
									</span>
									</td>
									<td><label style="font-size:12px;"><b>Email Address :</b></label>
									</td>
									<td><span class="textbox-bg"><form:input
												type="text" path="emailAddress" id="emailAddress" style="font-size:12px;"
									placeholder="only for Scan User" name="emailAddress"  class="textbox" />
									<label class="serror" style="color: red; font-size: 14px;"></label>
									</span>
									</td>
								</tr>
								<tr>
									<td style="display: none;"><label class="textlabel" style="font-size:12px;">Status
											:</label>
									</td>
									<td style="display: none;"><span class="selectbox2">
											<form:select id="status" path="status" itemLabel="SELECT"
												class="select2">
												<form:option value="ACTIVE" label="ACTIVE" />
												<form:option value="INACTIVE" label="INACTIVE" />
											</form:select>
									</span>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
<!-- 										<button type="submit"  id="btnSave">Save</button> -->
										<button type="submit"  id="btnSave">Save</button>
										<input type="reset" id="btnReset" value="Reset " onClick="this.form.reset()"/>
										
									</td>
									<td colspan="2"></td>
								</tr>
							</table>
						</form:form>
						<label id="saveerror" style="color: green"></label>
					</div>
					<div id="displayGrid" style="padding-top: 30px; font-size:12px;"></div>
				</div>
				<!-- End: Container -->
			</div>
		</div>
</div>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div id="editUserDiv">
<c:url var="url" value="/userController/saveuser" />
	<form:form id="editUserForm" action="${url}" method="POST" modelAttribute="userVo">
		<fieldset>
			<legend style="margin-left: 10px;"> Update User</legend>
			<table cellspacing="5" cellpadding="5">
				<tr>					
					<td>
						<form:select id="userType" path="userType" hidden="true">
							<option value="">Select</option>
							<option value="Audit User">Audit User</option>
							<option value="Flc User">Flc User</option>
							<option value="Scan User">Scan User</option>
							<option value="Scrutiny User">Scrutiny User</option>
						</form:select>
					</td>
				</tr>
				<tr>					
					<td><label style="font-weight: bold; font-size: 12px;">First Name :</label></td>
					<td><input type="text" name="firstName" id="firstNameTxt" size="25" value="${userVo.firstName}" style="font-size: 12px;"/></td>
					<td><label style="font-weight: bold; font-size: 12px;">Last Name :</label></td>
					<td><input type="text" id="lastNameTxt" name="lastName" size="25" value="${userVo.lastName}" style="font-size: 12px;"/></td>
				</tr>
				<tr>
					<td><label style="font-weight: bold; font-size: 12px;">Middle Name :</label></td>
					<td><input type="text" name="middleName" id="middleNameTxt" size="25" value="${userVo.middleName}" style="font-size: 12px;"/></td>
					<td><label style="font-weight: bold; font-size: 12px;">Address :</label></td>
					<td><input type="text" id="addressTxt" name="address" size="25" value="${userVo.address}" style="font-size: 12px;"/></td>
				</tr>
				<tr>
					<td><label style="font-weight: bold; font-size: 12px;">UserName :</label></td>
					<td><input type="text" id="usernameTxt" name="username" size="25" value="${userVo.username}" readonly="readonly" style="font-size: 12px;"/></td>
					<td><label style="font-weight: bold; font-size: 12px;">Mobile Number :</label></td>
					<td><input id="mobileNumberTxt" type="text" name="mobileNumber" size="25" maxlength="10" value="${userVo.mobileNumber}" style="font-size: 12px;"/></td>
				</tr>
				<tr>
					<td><label style="font-weight: bold; font-size: 12px;">Status :</label></td>					
					<td><select id="statusTxt" name="status" style="font-size: 12px;">
						<option value="${userVo.status}" style="font-size: 12px;">${userVo.status}</option>
						<c:choose>
							<c:when test="${userVo.status == 'ACTIVE'}">
								<option value="DEACTIVE" style="font-size: 12px;">DEACTIVE</option>
							</c:when>
							<c:when test="${userVo.status == 'DEACTIVE'}">
								<option value="ACTIVE" style="font-size: 12px;">ACTIVE</option>
							</c:when>
						</c:choose>										
						</select>	
					</td>									
					<td><input id="userId" type="hidden" name="id" size="25" maxlength="10" value="${userVo.id}"/></td>
				</tr>
			</table>
		</fieldset>
	</form:form>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ page import="org.springframework.security.core.AuthenticationException"%>
<%@ page import="org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter"%>
<%@ page import="org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Speedoc</title>


<link href="<c:url value="/resources/styles/uninor/style.css" />" rel="stylesheet" type="text/css" />
<c:url var="imageUrl" value="/resources/images/uninor/" />

</head>
<body onload='document.f.j_username.focus();'>
	
	<div id="wrapper">
	<div id="top">
		<div class="logo"><a href="#"><img src="${imageUrl}logo.jpg" alt="Speedoc" title="Speedoc" width="150" height="37" /></a></div><!-- End: Logo -->
        <div id="nav">
            
        </div><!-- End: Top Navigation -->
        <div class="uninor-logo"><img src="${imageUrl}uninor-logo.jpg" alt="Uninor" width="180" height="52" /></div>
    </div><!-- End: Top -->
    <div id="container">
    	<div id="login">
            <div class="logo2"><img src="${imageUrl}logo2.png" alt="Speedoc" width="257" height="80" /></div>
        	<div class="form">
            	<h2>
            	Portal <strong>Login</strong>
            	<c:if test="${not empty param.login_error}">
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 Login Unsuccessful
				 </c:if>
            	
            	</h2>
            	
                <c:url var="url" value="/j_spring_security_check" />

                
                 <form name='f' action='${url}' method='POST'>
                <p><label><span>Username</span></label><span class="logtexbox"><span><input type="text" name="j_username" class="textbox1" id="j_username"/></span></span></p>
                <p><label><span>Password</span></label><span class="logtexbox2"><span><input type="password" name="j_password" value="" class="textbox2" id="j_password"/></span></span><span class="for-pass"></span></p>
                <p class="submit"><input type="image" src="${imageUrl}submit.jpg" name="" value="" /><input type="image" src="${imageUrl}reset.jpg" name="" value="" onclick="document.f.reset();return false;"/></p>
                </form>
                
                
            </div>
        </div>    
    </div><!-- End: Container -->
    
    
    <div id="footer">
<%--     <div class="datamatic"><img src="${imageUrl}datamatic-logo.jpg" alt="Datamatic System Pvt. Ltd." width="140" height="46" /></div> --%>
    	<div class="datamatic"><img src="${imageUrl}cresent-logo2.png" width="300" height="50" /></div>
        <div id="fbox">
            <div class="fbox-top"><span></span></div>
            <div class="fbox-cont">
                <div class="left" style="display: none;">
                    <h2>Branch Office</h2>
<!--                     <p>Shop No. 10, Shanti Complex, Near Lokseva Bank, Narpatgeeri Chowk<br />Mangalwat Peth, Pune 411 011 (Contact: 0 9370929370)</p> -->
                </div>
                <div style="text-align: center;">
                    <h2>Registered Office</h2>
<!--                     <p>3/4, Beside NGDA Service Station, Buty Compound, Mount Road Extn.<br />Sadar, Nagpur, 400 001 (Contact: 0 9370929370) </p> -->
                	<p> Office no.11/19, A-wing, 3rd floor, Sagar complex, Opp Kasarwadi<br/> Railway station, Kasarwadi, Pune Maharashtra 411034.</p>
                   	<p>Website :- <a href="http://www.crmpl.in/" style="color: #7185B0;">www.crmpl.in</a></p>
                    <p>Version U.V.3.0</p>
                    <p style="display: none;" >Release Date 14 October 2013 01.30 PM </p> 
                </div>
                <div class="clear"></div>             
            </div>
            <div class="fbox-bottom"><span></span></div>
        </div>
    </div><!-- End: Footer -->
</div><!-- End Wrapper -->
<div id="int-bottom"><span></span></div><!-- End: Content Bottom -->
	
</body>
</html>
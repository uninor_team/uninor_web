<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login success</title>

<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-1.10.0.js" />"></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.1.0.js" />"></script> --%>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.2.1.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.2.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-ui-1.9.2.custom.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/json.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/JqueryConfirm.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery.validate.js" />"></script>
<script type="text/javascript" src='<c:url value="/resources/scripts/js/jquery.alerts.js"/>'></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/grid.locale-en.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery.jqGrid.min.js" />"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/moment-with-langs.js'/>"></script>
<link href="<c:url value="/resources/styles/css/ui.jqgrid.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery-ui.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery-ui.min.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.ui.theme.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/style.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.alerts.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.confirm.css" />" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/jquery.maskedinput-1.3.js'/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#tabs").tabs();	
		calendarDigitalClock();
	});

	function calendarDigitalClock(){
		var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
		var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year    
		$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

		setInterval( function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
			},1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		    },1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
		    }, 1000);
	}
	
	//Function returns current date
	function getCurrentDate() {
		var date = new Date();
		var futDate = date.getDate() + "/" + (parseInt(date.getMonth()) + 1)
				+ "/" + date.getFullYear();
		return futDate.toString();
	}
</script>
</head>
<body style=" margin: 0;">
<c:url value="/resources/images/uninor/" var="imageUrl"></c:url>
<div>
	<div class="ui-widget ui-widget-header" style="height:70px;" >
		<img class="ui-image" src="${imageUrl}/logo.jpg" />
					
			<div align="right" style="margin-right: 10px;">
						Welcome <b>${username}</b>|<a href="<c:url value = "/j_spring_security_logout"></c:url>" id="logout"
						style="background-color: #f0f0f0; width: 121px; font-size: 15px;">Logout</a>
					<div id="current_date_div" align="right">
						<div class="clock" align="right" style="font:bold 12px Arial, Helvetica, sans-serif; color:#3383BB;">
							<div id="Date"></div>
						</div>
					</div>
			</div>				
	</div>	
  	<div id="tabs">
		<ul>		
			<sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_VENDOR_ADMIN,ROLE_AVAM_USER,ROLE_VENDOR_OPERATOR,ROLE_AUDIT_USER,ROLE_SLC_USER,ROLE_FLC_OPERATOR">
				<sec:authorize ifAnyGranted="ROLE_ADMIN">
					<li id="userTabId"><a href="<c:url value="/userController/getuserform"/>">Add User</a></li>	
					<li id="activationTabId"><a href="<c:url value="/activationController/getactivationform"/>">Add Activation Officer</a></li>	
					<li id="avamTabId"><a href="<c:url value="/avamController/getuploadavamform"/>">Avam Upload</a></li>	 												
					<li id="flcTabId"><a href="<c:url value="/flc/getflcpage"/>">Flc</a></li>
					<li id="slcTabId"><a href="<c:url value="/slc/getslcpage"/>">Scrutiny</a></li>
					<li id="searchTabId"><a href="<c:url value="/searchController/getsearchpage"/>">Search</a></li>
					<li id="misTabId"><a href="<c:url value="/misController/getreportpage"/>">Mis</a></li>
					<li id="statusViewerTabId"><a href="<c:url value="/statusController/getstatuspage"/>">Status Viewer</a></li>
					<li id="auditTabId"><a href="<c:url value="/audit/getauditform"/>">Audit</a></li>
					<li id="slcUploadTabId"><a href="<c:url value="/slcupload/getslcuploadpage"/>">Slc Upload</a></li>
					<li id="slcDoneTabId"><a href="<c:url value="/slcdone/getslcdoneuploadform"/>">Slc Done</a></li>
					<li id="slcSearchTabId"><a href="<c:url value="/slc/getslcsearchpage"/>">SLC Search</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_VENDOR_ADMIN">
					<li id="userTabId"><a href="<c:url value="/userController/getuserform"/>">Add User</a></li>	
					<li id="activationTabId"><a href="<c:url value="/activationController/getactivationform"/>">Add Activation Officer</a></li>	
					<li id="searchTabId"><a href="<c:url value="/searchController/getsearchpage"/>">Search</a></li>
					<li id="misTabId"><a href="<c:url value="/misController/getreportpage"/>">Mis</a></li>
					<li id="statusViewerTabId"><a href="<c:url value="/statusController/getstatuspage"/>">Status Viewer</a></li>
					<li id="slcUploadTabId"><a href="<c:url value="/slcupload/getslcuploadpage"/>">Slc Upload</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_AVAM_USER">
					<li id="avamTabId"><a href="<c:url value="/avamController/getuploadavamform"/>">Avam Upload</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_VENDOR_OPERATOR">
					<li id="slcSearchTabId"><a href="<c:url value="/slc/getslcsearchpage"/>">SLC Search</a></li>
					<li id="slcTabId"><a href="<c:url value="/slc/getslcpage"/>">Scrutiny</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_FLC_OPERATOR">
					<li id="flcTabId"><a href="<c:url value="/flc/getflcpage"/>">Flc</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_AUDIT_USER">
					<li id="auditTabId"><a href="<c:url value="/audit/getauditform"/>">Audit</a></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_SLC_USER">
					<li id="slcDoneTabId"><a href="<c:url value="/slcdone/getslcdoneuploadform"/>">Slc Done</a></li>
				</sec:authorize>
			</sec:authorize>
		</ul>		
	</div>  
</div>           						
</body>
</html>							
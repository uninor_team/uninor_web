<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:url value="/avamController/getavammonthwiselist" var="gridUrl" />

<script type="text/javascript">
$(document).ready(function() {
	 
	$("#grid").jqGrid({
     	url:'${gridUrl}',
		datatype : 'json',
		mtype : 'GET',
		colNames : ['Id', 'File Name', 'Total Record','Record Inserted','Duplicate Record','Status','Created Date','Username'],
		colModel : [ {
			name : 'id',
			index : 'id',
			width : 6,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			},
			hidden : true
		}, {
			name : 'fileName',
			index : 'fileName',
			width : 25,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'totalRecord',
			index : 'totalRecord',
			width : 13,
			editable : false,
			hidden : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'recordInserted',
			index : 'recordInserted',
			width : 13,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'duplicateRecord' ,
			index : 'duplicateRecord' ,
			width : 13,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'status' ,
			index : 'status' ,
			width : 13,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'createdDate' ,
			index : 'createdDate' ,
			formatter: function (newFormat, opts) {
			    var date = new Date(newFormat);
			    opts = $.extend({}, $.jgrid.formatter.date, opts);
			    return $.fmatter.util.DateFormat("", date, 'd-M-Y', opts);
			},
			width : 13,
			editable : false,
			hidden : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
								
		}, {
			name : 'username' ,
			index : 'username' ,
			width : 13,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,	
			}						
	     }],
		postData :{month:$("#month").val()},
		rowNum :25,
		rowList : [25, 50, 75, 100,150,200,250,300,350,400,600,800,1000 ],
		height : 'auto',
		autowidth : true,
		rownumbers : true,
// 		toppager : true,
// 		pager: '#grid_pager',
		viewrecords : true,
// 		refresh : true,
// 		sortname : 'id',
		sortorder : "desc",
		emptyrecords : "Empty records",
		loadonce : false,
		subGrid: false,
		hidegrid: false,
		footerrow: true,
		userDataOnFooter: true,		
		subGridOptions: { 
              "plusicon" : "ui-icon-info",
			  "minusicon" : "ui-icon-triangle-1-s", 
			  "openicon" : "ui-icon-arrowreturn-1-e" 
			},				
          jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			cell : "cell",
			id : "id"
		},gridComplete: function(){				
			var ids = jQuery("#grid").jqGrid('getDataIDs');
			for(var i=0;i < ids.length;i++){
				var cl = ids[i]; 
				var rowData=jQuery('#grid').jqGrid ('getRowData', cl);
				var avamId=rowData.id;
				jQuery("#grid").jqGrid('setRowData',ids[i]);
			}
		},
		loadComplete: function(){
				//alert("loadComplete");
		 }
	 });

	$("#grid").jqGrid('navGrid',{
		edit : false,
		add : false,
		del : false,
		search : false
	}, {}, {}, {}, { // search
		sopt : [ 'cn', 'eq', 'ne', 'lt', 'gt', 'bw', 'ew' ],
		closeOnEscape : true,
		multipleSearch : true,
		closeAfterSearch : true
	});			
});

</script>
<div>
	<div id='jqgrid'>
		<table id='grid' style="text-align: center; font-size: 12px;"></table>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:url var="url" value="/avamController/uploadFile" /> 
	
<script type="text/javascript">	
$(document).ready(function(){
	$("#uploadBtn").button();
	$.get("<c:url value='/avamController/getavampage' />",function(data){
		 $('#resultData').html(data);   
	});
	
});

function uploadFormData(){
 
    var url = "<c:url value='/avamController/uploadFile' />" ;
  	var oMyForm = new FormData();
  	oMyForm.append("file", files.files[0]);

		$.ajax({
	    	url: url,
	    	data: oMyForm,
	   	 	dataType: 'text',
	    	processData: false,
	    	contentType: false,
	    	type: 'POST',
	    	success: function(data){
	    		$("#up").show();
	       	 $('#resultData').html(data);   
	       	$('#files').val("");
	    	}
	  	});	
}

</script>
			
<div class="content">
<div id="mainAvam" style="padding-top: 30px; width: 400px;">
	<table>
		<tbody>
			<tr>
				<td >
					<label style="display: block; width:133px;margin-bottom: 18px; font-size: 12px;">
					<b>Select Avam dump file:</b> </label></td>
				<td>
					<form enctype="multipart/form-data" method="post" action="<c:url value='/avamController/uploadFile' />" style="width: 230px;" id="form">
						<input type="file" name="files" id="files" style="font-size: 12px;">
					</form>
				</td>
				<td>
					<button id="uploadBtn" onclick="uploadFormData()" value="Upload" class="ui-button-text" style="margin-bottom:12px;">
					Upload</button>		
				</td>
			</tr>
			<tr>
				<td></td>
					
				<td colspan="2">
					<label style="display: none;color: red; font-size: 15px;" id="up">File Upload Successfully. </br>Status of AVAM file will be shown after 10 to 15 minutes.</label>
				</td>	
			</tr>
		</tbody>
	</table>
</div>	
		
<div id="resultData" style="padding-top: 15px;"></div>

</div>
		
		
	
	


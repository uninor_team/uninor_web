<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(document).ready(function(){
		$("#btnSearch").button();
		$("#resetButton").button();
		
		$("#btnSearch").click(function(){
			if(validateForm()){
				$.post("<c:url value='/searchController/getsearchdata' />",{searchTxt:$("#txtSearch").val(),searchOption:$("#searchForm").val()},function(data){					
					var serviceResponse = data;
					var searchVos = serviceResponse.result;
					if(serviceResponse.successful){
						displayResult(searchVos);						
					}else{						
						$("#resultDiv").css("color","red");
						$("#resultDiv").html("No Record Found");					
					}															
				});
			}			
		});
		
		function validateForm(){
			var textSearch =$.trim($("#txtSearch").val());
			
			if($("#searchForm").val() == "Select" ){					
				jAlert("Please select any option.");
				return false;
			}
			if(textSearch == "" || textSearch == null){
				jAlert("Please fill search box.");
				return false;
			}
			if($("#searchForm").val() == "Mobile Number"){
				if(textSearch.length < 10 || textSearch.length > 10){
					jAlert("Mobile Number should be 10 digits. ");
					return false;
				}
			}
			if($("#searchForm").val() == "Speedoc Id"){
				if(textSearch.length < 9 || textSearch.length == 10 || textSearch.length == 11){
					jAlert("Speedoc Id should be 9 OR 12 OR 13 digits.");
					return false;
				}
			}
			return true;
		}
		
		$("#resetButton").click(function(){
			$("#txtSearch").val("");
			$("#searchForm").val("Select");
			$("#resultDiv").html("");
		});
	});

	function displayResult(searchVos){
		var htmlStr = "<table width='100%' cellspacing='0' cellpadding='0' border='1' align='center' id='displayResult' class='gridtable1' style='font-size: 12px;'>";
		htmlStr += 	"<tr align='center' valign='middle' class='tbl-head' style='background:#EEEEEE;'>";

		htmlStr +=	"<th class='tbl-top'>Sr No.</th>";
		htmlStr +=	"<th class='tbl-top'>Speedoc Id</th>";
		htmlStr +=	"<th class='tbl-top'>Mobile Number</th>";
		htmlStr +=	"<th class='tbl-top'>Caf Number</th>";
		htmlStr +=	"<th class='tbl-top'>First Name</th>";
		htmlStr +=	"<th class='tbl-top'>Last Name</th>";
		htmlStr +=	"<th class='tbl-top'>Status</th>";
		htmlStr +=	"<th class='tbl-top'>Date</th>";
		htmlStr += "</tr>";
		for ( var i = 0; i < searchVos.length; i++) {
			if(i%2==0) {
				htmlStr +="<tr align='center' valign='middle' class='row-alter'>";
			} else {
				htmlStr +="<tr align='center' valign='middle' class='row'>";
			}			
			htmlStr +="<td>"+(i+1)+"</td>";
						
			if(searchVos[i].speedocId != null){
				htmlStr +="<td>"+searchVos[i].speedocId+"</td>";
			}else{
				htmlStr +="<td>NA</td>";				
			}
			if(searchVos[i].mobileNumber != null){
				htmlStr +="<td>"+searchVos[i].mobileNumber+"</td>";
			}else{
				htmlStr +="<td>NA</td>";		
			}
			if(searchVos[i].cafNumber != null){
				htmlStr +="<td>"+searchVos[i].cafNumber+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(searchVos[i].firstName != null){
				htmlStr +="<td>"+searchVos[i].firstName+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(searchVos[i].lastName != null){
				htmlStr +="<td>"+searchVos[i].lastName+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(searchVos[i].statusName != null){
				htmlStr +="<td>"+searchVos[i].statusName+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			if(searchVos[i].createdDate != null){
				htmlStr +="<td>"+searchVos[i].createdDate+"</td>";
			}else{
				htmlStr +="<td>NA</td>";
			}
			htmlStr +="</tr>";
		}
		htmlStr += "</table>"; 	 	

		$("#resultDiv").html("");
		$("#resultDiv").html(htmlStr);
	}
</script>
<style>
	#displayResult{
		margin-left: 2px;
		border:1px double #999999;
	}
	
	#form{
		margin-top: 20px;
	}
	
	table.gridtable1 {
		font-family: verdana,arial,sans-serif;
		font-size:12px;
		color:#3383BB;
		border-width: 1px;
		border-color: #666666;
		border-collapse: collapse;
	}
	
	table.gridtable1 th {
		border-width: 1px;
		padding: 8px;
		border-style: solid;
		border-color: #666666;
		background-color: #EEEEEE;
	}
	
	table.gridtable1 td {
		border-width: 1px;
		padding: 2px;
		border-color: #666666;
		background-color: #DDDDDD;
		color:#000000;
	}
</style>
<div id="container">
	<div id="form">
		<form:form id="frmSearchOption" modelAttribute="searchVo">		
			<table>
				<tr>
					<td>
						<select id="searchForm" name="columnName" style="font-size: 12px;">
							<option style="font-size: 12px;">Select</option>
							<option value="Mobile Number" style="font-size: 12px;">Mobile Number</option>
							<option value="Speedoc Id" style="font-size: 12px;">Speedoc Id</option>						
						</select>
					</td>
					<td>
						<input id="txtSearch" type="text" name="searchText" maxlength="13" tabindex="1" style="font-size: 12px;"/>									
					</td>
					<td><button type="button" id="btnSearch" tabindex="2">Search</button>
						<button type="reset" id="resetButton" tabindex="3">Reset</button>
					</td>
				</tr>
			</table>
		</form:form>
	</div>
	<div id='resultDiv'></div>
</div>


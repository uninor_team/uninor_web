<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div style="width: 100%; height: auto;">

	<form name="slcfrmRadio" id="slcfrmRadio">
		<table border="0">
			<c:forEach var="remarkVo" items="${remarkVoList}">
				<tr>
					<td align="right" valign="top"><input type="checkbox"
						value="${remarkVo.id}" name="slcremak" onclick="addRejectReasons(${remarkVo.id}, this)"
						id="${remarkVo.rejectReason}" />
					</td>
					<td>
						<label style="width: 100%; margin-left: 4px; font-size: 12px;">${remarkVo.rejectReason}</label>
					</td>
				</tr>
			</c:forEach>
<!-- 			<tr> -->
<!-- 				<td></td> -->
<!-- 				<td><button id="rejectDone" type="button">OK</button></td> -->
<!-- 			</tr> -->
		</table>
	</form>
</div>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta http-equiv="Content-path" content="text/html; charset=ISO-8859-1">
<title>Slc</title>
<%-- <script type="text/javascript" src="<c:url value='/resources/scripts/slc/moment-with-langs.js'/>"></script>--%>
 <script src="<c:url value="/resources/scripts/slc/SLCPipe.js?v=1.2" />" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/slc.js?v=1.2'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/cafElementListHorizontal.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/scrollintoview.jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/uninor/PageRotation.js' />" ></script>
<%-- <script type="text/javascript" src="<c:url value='/resources/scripts/js/json.min.js'/>"></script>--%>

<c:url var="imageUrl" value="/resources/images/uninor/" />
<script type="text/javascript">
	$(document).ajaxStop($.unblockUI);
	$(document).ready(function() {
		$("#cancelBut").button();
		$("#slcsave").button();
		$("#rejectDone").button();
		$("#photoComplianceBtn").button();
		$("#documentComBtn").button();
		$("#cafComBtn").button();
		$("#customerComBtn").button();
		$("#caoCombtn").button();
		$("#retailerComBtn").button();
		$("#distributorComBtn").button();
		$("#overwritingBtn").button();
		$("#outstationCombtn").button();
		$("#mnpCombtn").button();
		$("#databaseCombtn").button();
// 		$("#otherbtn").button();
		$("#slcImageRotateBtn").button();
		
// 		$("#rejectDone").hide();
		$("#rejectDone").button();
	});

	function bigImg(x) {
	    x.style.width = "180%";
	}
	function normalImg(x) {
	    x.style.width = "100%";
	}
	
</script>

<style>
#bg_image_photo {
	width: 250px;
	height: 300px;
	margin: 5px 5px 5px 5px;
}

#bg_image_signature {
	width: 350px;
	height: 150px;
	margin: 5px 5px 5px 5px;
}

#basicDetails td {
	width: 7%;
	text-align: left;
}

#basicDetails input {
	width: 120px;
}

#basicDetails label {
	font-size: 12px;
}

#addDetails td {
	width: 7%;
	text-align: left;
}

#addDetails input {
	width: 120px;
	font-size: 13px;
}

#addDetails label {
	font-size: 12px;
}

#passportDetails td {
	width: 10%;
	text-align: left;
}

#passportDetails input {
	width: 100px;
}

#passportDetails label {
	font-size: 12px;
}

#ui-tabs-5 #scrutinyDiv {
	margin: 0;
	padding: 0;
	width: 100%;
}

.ui-tabs .ui-tabs-panel {
	padding: 0 !important;
}

#cafImages {
	overflow-y: scroll;
	overflow-x: hidden;
	float: left;
	width: 100%;
	height: 50%;
	border-bottom: 4px solid black;
}

rotateAuditHiddendiv
</style>
</head>
<body>
	
	<!-- <div id="scrutinyDiv">
		<div id="idletimeout" style="color: red;">
			You will be logged off in <span> countdown place holder
			</span>&nbsp;seconds due to inactivity. <a id="idletimeout-resume" href="#">Click
				here to continue using this web page</a>.
	</div> -->
		
		<div>
			<form id="rotatecafslc" name="rotatecafslc" style=" margin-left: 2%;">
				
				<input id="fileName2" type="hidden" name="fileName"/>
				<input type="hidden" id="lblNoOfPage2" /> 
				<label >Page :</label> <select id="pageNo2" name="pageNo"></select>	
			   	
			</form>
			<button title="rotate" style=" margin-left: 10%; position:relative; bottom: 34px; float: left;" id="slcImageRotateBtn" align="top">Rotate</button>
		</div>
		<div id="" style="width: 100%; height: 100%">
			<div id="" style="position: relative; height: 100%">
				<div style="position: relative; width: 100%; margin-bottom: 5px">
					<div id="cafImages"></div>
					<div id="hiddenImageDiv" style="display: none;"></div>
					<div id="rotateSlcHiddendiv" style="display: none; height: 0px"></div>
				</div>
				<div
					style="position: relative; width: 100%; height: 50%; float: right; vertical-align: top; overflow-y: scroll; overflow-x: hidden;">
					<div id="smbox">
						<div class="smbox-cont">
							<div id="form">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td>
											<form id="myForm" name="myForm">
												<input type="hidden"  id="emailAddress" name="email"/>
												<input type="hidden"  id="vasApplied" name="vasApplied"/>
												<input type="hidden"  id="tarrifPlanApplied" name="tarrifPlanApplied"/>
												<input type="hidden"  id="existingNumber" name="existingNumber"/>
												<input type="hidden"  id="prevServiceProviderName" name="prevServiceProviderName"/>
												<input type="hidden"  id="existingConnection" name="existingConnection"/>
												<input type="hidden"  id="callerBackRingTone" name="callerBackRingTone"/>
												<input type="hidden"  id="friendsAndFamily" name="friendsAndFamily"/>

												<table width="100%" cellpadding="0" cellspacing="0"
													style="margin-top: 10px">
													<tr>
														<td>
															<table width="92%" id="basicDetails">
																<tr>
																	<td><label>Speedoc Id</label></td>
																	<td><input class="focusH" id="speedocId1"
																		name="speedocId" maxlength="9" tabindex="1"
																		readonly="readonly"></td>
																	
																	<td><label>Mobile Number</label></td>
																	<td><input class="focusH" id="mobileNumber1"
																		name="mobileNumber" maxlength="10" tabindex="2"
																		readonly="readonly" /></td>
																	
																	<td><label>Caf Number</label></td>
																	<td><input class="focusH" id="cafNumber"
																		name="cafNumber" readonly="readonly" maxlength="20"
																		tabindex="3"></td>
																	
																	<td><label>DOB</label></td>
																	<td><div><input class="focusH" id="dob" name="dob"
																				maxlength="10" readonly="readonly"
																				class="validateDateOfBirth" tabindex="4" />
																		</div>
																		<div>
																			<label>(dd-mm-yyyy)</label>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td id="fn"><label>First Name</label>
																	</td>
																	<td><input tabindex="5" class="focusH"
																		id="firstName1" name="firstName" maxlength="50"
																		readonly="readonly" /></td>
																	<td><label>Middle Name</label>
																	</td>
																	<td><input tabindex="6" class="focusH"
																		id="middleName1" name="middleName" readonly="readonly"
																		maxlength="50" /></td>
																	<td><label>Last Name</label>
																	</td>
																	<td><input tabindex="7" class="focusH"
																		id="lastName1" name="lastName" readonly="readonly"
																		onkeyup="fillLastName()" maxlength="50" /></td>
																	<td><label class="generalLable">Nationality</label>
																	</td>
																	<td><input class="focusH" id="nationality"
																		name="nationality" tabindex="9" readonly="readonly">
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table width="110%" id="addDetails">
																<tr>
																	<td align="right" colspan="2"><label>Father's/Husband's:</label></td>
																</tr>
																<tr>
																	<td><label>First Name</label></td>
																	<td><input class="focusH" id="fatherFirstName"
																		name="fatherFirstName" readonly="readonly"
																		tabindex="10" maxlength="50" /></td>
																	
																	<td><label class="generalLable">Middle Name</label>
																	</td>
																	<td><input class="focusH" id="fatherMiddleName"
																		name="fatherMiddleName" readonly="readonly"
																		tabindex="11" maxlength="50" /></td>
																	
																	<td><label>Last Name</label></td>
																	<td><input class="focusH" id="fatherLastName"
																		name="fatherLastName" readonly="readonly"
																		tabindex="12" maxlength="50" /></td>
																		
																	<td><label class="generalLable">Landmark</label>
																	</td>
																	<td><input class="focusH"
																		id="localAddressLandmark" name="localAddressLandmark"
																		maxlength="30" readonly="readonly" tabindex="14" /></td>
																</tr>
																<tr>
																	<td><label>Permanent Address</label></td>
																	<td colspan="3"><textarea class="focusH"
																			id="permanentAddress" name="permanentAddress"
																			style="width: 386px; height: 85px;"
																			readonly="readonly" tabindex="13"></textarea></td>
																			
																	<td><label>Local Address</label></td>
																	<td colspan="3"><textarea class="focusH"
																			id="localAddress" name="localAddress"
																			style="width: 386px; height: 85px;"
																			readonly="readonly" tabindex="13"></textarea></td>
																</tr>
																<tr>
																	<td><label>State</label></td>
																	<td><input class="focusH" id="localState"
																		name="localState" class="generalLable"
																		readonly="readonly" tabindex="15" readonly="readonly">
																	</td>
																	
																	<td><label>City</label></td>
																	<td><input class="focusH" id="localCity"
																		name="localCity" class="generalLable" tabindex="16"
																		readonly="readonly"></td>
																		
																	<td><label>District</label></td>
																	<td><input class="focusH" id="localDistrict"
																		name="localDistrict" class="generalLable"
																		tabindex="17" readonly="readonly"></td>
																	
																	<td><label>Pin Code</label></td>
																	<td><input class="focusH" id="localPincode"
																		name="localPincode" maxlength="6" readonly="readonly"
																		tabindex="18" /></td>
																</tr>
																<tr>
																	<td><label>Alternate No</label></td>
																	<td><input class="focusH"
																		id="alternateMobileNumber"
																		name="alternateMobileNumber" maxlength="10"
																		readonly="readonly" tabindex="19" /></td>
																
																	<td><label class="generalLable">PAN/GIR No.</label>
																	</td>
																	<td><input class="focusH" id="panGirNumber"
																		name="panGirNumber" readonly="readonly" tabindex="27" />
																	</td>
																</tr>
																<tr>
																	<td><label>POI</label>
																	</td>
																	<td><input class="focusH" id="poi" name="poi"
																		style="width: 155px;"
																		readonly="readonly" tabindex="29"></td>
																	<td><label class="generalLable">POI Ref#</label>
																	</td>
																	<td><input class="focusH" id="poiref"
																		name="poiref" maxlength="30" tabindex="30"
																		readonly="readonly" style="width: 155px;"/></td>
																	
																	<td><label>POA</label></td>
																	<td><input class="focusH" id="poa" name="poa"
																		style="width: 155px;"
																		tabindex="31" readonly="readonly"></td>
																	
																	<td><label class="generalLable">POA Ref#</label></td>
																	<td><input class="focusH" id="poaref"
																		name="poaref" maxlength="30" tabindex="32"
																		readonly="readonly" style="width: 155px;"/></td>
																</tr>
																<tr>
																	<td><label class="generalLable">POI Issue
																			Place</label>
																	</td>
																	<td><input id="poiIssuePlace" class="focusH"
																		tabindex="33" maxlength="50" readonly="readonly"
																		style="width: 155px;"
																		name="poiIssuePlace"></td>
																	<td><label class="generalLable">POI
																			Issuing Authority</label>
																	</td>
																	<td><input id="poiIssuingAuthority" class="focusH"
																		tabindex="34" readonly="readonly"
																		style="width: 155px;"
																		name="poiIssuingAuthority"></td>
																	<td><label class="generalLable">POI Issue
																			Date</label>
																	</td>
																	<td><input id="poiIssueDate" class="focusH"
																		maxlength="19" tabindex="35" readonly="readonly"
																		style="width: 155px;"
																		name="poiIssueDate"></td>
																	<td><label class="generalLable">POA Issue
																			Place</label>
																	</td>
																	<td><input id="poaIssuePlace" class="focusH"
																		style="width: 155px;"
																		readonly="readonly" maxlength="50"
																		name="poaIssuePlace"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">POA
																			Issuing Authority</label>
																	</td>
																	<td><input id="poaIssuingAuthority" class="focusH"
																		style="width: 155px;"
																		readonly="readonly" name="poaIssuingAuthority">
																	</td>
																	<td><label class="generalLable">POA Issue
																			Date</label>
																	</td>
																	<td><input id="poaIssueDate" class="focusH"
																		style="width: 155px;"
																		maxlength="19" readonly="readonly" name="poaIssueDate">
																	</td>
																	<td><label>Status Of Subscriber</label>
																	</td>
																	<td><input id="statusOfSubscriber" class="focusH"
																		style="width: 155px;"
																		tabindex="44" readonly="readonly"
																		name="statusOfSubscriber"></td>
																	<td><label>Profession of the Subscriber</label>
																	</td>
																	<td><input id="subscriberProfession"
																		class="focusH" tabindex="45" readonly="readonly"
																		style="width: 155px;"
																		name="subscriberProfession"></td>
																</tr>
																<tr>
																	<td><label>IMSI No.</label></td>
																	<td><input id="imsiNumber" class="focusH"
																		maxlength="10" tabindex="46" readonly="readonly"
																		style="width: 155px;"
																		name="imsiNumber"></td>
																	
																	<td><label class="generalLable">UID No. </label></td>
																	<td><input id="uidNumber" class="focusH"
																		tabindex="50" maxlength="50" readonly="readonly"
																		style="width: 155px;"
																		name="uidNumber"></td>
																	
																	<td><label>MNP</label></td>
																	<td><div id="mnpCheckBox">
																			<input type="checkbox" style="width: 20px;"
																				tabindex="51" name="mnp" id="mnp" class="focusH"
																				readOnly="readOnly">
																			</div>
																	</td>
																</tr>
																
																<tr>
																	<td><input type="hidden" id="connectionType"
																		name="connectionType">
																	</td>

																	<td><input type="hidden" id="id" name="id" /> <input
																		type="hidden" id="onhold" name="onhold" /></td>
																	<td><input type="hidden" id="cafStatusId1"
																		name="cafStatusId" /></td>
																	<td><input type="hidden" id="images1"
																		name="images1" /></td>
																	<td><input type="hidden" id="cafType"
																		name="cafType" />
																	</td>
																	<td><input type="hidden" id="cafFileLocation"
																		name="cafFileLocation" />
																	</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
																<!-- <tr id="portingReason1">
																	<td nowrap="nowrap"><label>Porting Reason:</label>
																	</td>
																</tr> -->
																<tr id="mnpManditory2">
																	<td><label class="generalLable">UPC Code </label></td>
																	<td><input id="upcCode" class="focusH"
																		style="width: 155px;"
																		tabindex="2" maxlength="10" readonly="readonly"
																		name="upcCode"></td>
																	
																	<td><label>CAF tracking Status</label></td>
																	<td><input class="focusH" id="cafTrackingStatus" name="cafTrackingStatus"
																		style="width:155px;"
																		readonly="readonly" tabindex="57"></td>
																	
																	<td><label>RTR Code</label></td>
																	<td><input class="focusH" id="wap" name="wap"
																		style="width: 155px;" 
																		readonly="readonly" tabindex="58"></td>
																	
																	<td><label>RTR Name</label></td>
																	<td><input class="focusH" id="mms" name="mms"
																		style="width: 200px;"
																		readonly="readonly" tabindex="59"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">Foreign
																			National</label>
																	</td>
																	<td><input type="checkbox" class="focusH"
																		style="width: 20px;" id="foreignNational"
																		name="foreignNational" value="REQUIRED" tabindex="60" />
																	</td>
																	<td><label class="generalLable">Outstation</label>
																	</td>
																	<td><input type="checkbox" class="focusH"
																		style="width: 20px;" id="outstation" name="outstation"
																		value="REQUIRED" tabindex="61" /></td>
																	<td colspan="2" nowrap="nowrap">
																		<div id="copyCommonFieldsCheckboxTr">
																			<label class="generalLable">Copy common
																				fields from above</label>
																		</div></td>
																	<td>
																		<div id="copyCommonFieldsCheckboxTr1" align="left">
																			<input type="checkbox" style="width: 20px;"
																				id="copyCommonFieldsCheckbox" tabindex="62">
																		</div></td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
															</table></td>
													</tr>
													<tr>
														<td>
															<table width="75%">
																<!-- PASSPORT & VISA DETAILS OF FOREIGN NATIONALS -->
																<tr style="height:9px"></tr>
																<tr id="foreignNationalTr1">
																	<td><label style="font-size: 12px;"
																		class="generalLable"><b><u>Passport & Visa Details</u> :</b></label>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table width="98%" id="passportDetails">
																<tr id="foreignNationalTr2">
																	<td><label class="generalLable">PASSPORT
																			No.</label>
																	</td>
																	<td><input class="focusH" id="passportNumber1"
																		class="generalLable" name="passportNumber"
																		tabindex="63" readonly="readonly" /></td>
																	<td><label class="generalLable">Type of
																			Visa</label>
																	</td>
																	<td><input class="focusH" id="typeOfVisa"
																		name="typeOfVisa" readonly="readonly" tabindex="64" />
																	</td>
																	<td><label class="generalLable">Visa valid
																			till</label>
																	</td>
																	<td>
																		<div>
																			<input class="focusH" id="visaValidTill"
																				name="visaValidTill" class="validateVisaValidTill"
																				readonly="readonly" maxlength="10" tabindex="65">
																		</div>
																		<div>
																			<label>(dd/mm/yy)</label>
																		</div></td>
																	<td><label class="generalLable">Visa No.</label>
																	</td>
																	<td colspan="5" align="left"><input class="focusH"
																		id="visaNumber" name="visaNumber" readonly="readonly"
																		tabindex="66"></td>
																	<td width=100%><div>&nbsp;</div>
																	</td>
																	<td>&nbsp;</td>
																</tr>
																<!-- IN CASE OF OUTSTATION APPLICANT / FOREIGN NATIONALS	 -->
																<tr id="outstationTr1">
																	<td align="left" colspan="10"><label
																		style="width: 150px;" class="generalLable">
																			<b><u>Local Reference</u> :</b></label></td>
																</tr>
																<tr id="outstationTr2">
																	<td><label class="generalLable">First Name</label>
																	</td>
																	<td><input class="focusH" id="localReferenceName"
																		name="localReferenceName" maxlength="50"
																		readonly="readonly" tabindex="67"></td>
																	<td><label class="generalLable">Address</label>
																	</td>
																	<td><input class="focusH"
																		id="localReferenceAddress"
																		name="localReferenceAddress" maxlength="200"
																		readonly="readonly" tabindex="68"></td>
																	
																	<td><label class="generalLable">State</label></td>
																	<td><input class="focusH" id="permanentState"
																		class="generalLable" name="permanentState"
																		tabindex="69" readonly="readonly"></td>
																	
																	<td><label class="generalLable">City/Town</label></td>
																	<td><input class="focusH" id="localReferenceCity"
																		name="localReferenceCity" class="generalLable"
																		tabindex="70" readonly="readonly" /></td>
																	
																	<td><label class="generalLable">Pin Code</label></td>
																	<td><input class="focusH"
																		id="localReferencePincode"
																		name="localReferencePincode" maxlength="6"
																		readonly="readonly" tabindex="71" /></td>
																</tr>
																<tr id="outstationTr3">
																	<td><label class="generalLable">Local
																			Contact No.</label>
																	</td>
																	<td><input class="focusH"
																		id="localReferenceContactNumber"
																		name="localReferenceContactNumber" maxlength="10"
																		tabindex="72" readonly="readonly" /></td>
																	<td><label class="generalLable">Calling
																			Party No.</label>
																	</td>
																	<td><input id="partyNumber" class="focusH"
																		tabindex="73" maxlength="10" readonly="readonly"
																		name="partyNumber"></td>
																	<td><label class="generalLable">Activation
																			Date</label>
																	</td>
																	<td><input id="activationDateTxt" class="focusH"
																		maxlength="19" tabindex="74" readonly="readonly"
																		name="activationDate"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">Cao Name</label>
																	</td>
																	<td><input class="focusH" id="caoName"
																		name="caoName" maxlength="6" readonly="readonly"
																		style="width: 190px;"
																		tabindex="71" /></td>
																	<td><label class="generalLable">Cao Code</label>
																	</td>
																	<td><input class="focusH" id="caoCode"
																		style="width: 150px;"
																		name="caoCode" maxlength="6" readonly="readonly"
																		tabindex="72" /></td>
																	<td><label class="generalLable">Cao
																			Signature</label>
																	</td>
																	<td><div id="imagediv"></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
<!-- 													election commission  25-05-2015-->
													<tr>
														<td>
															<table width="75%">
																<!-- Election Commission DETAILS -->
																<tr style="height:9px"></tr>
																<tr id="electionCommissionTr1">
																	<td><label style="font-size: 12px;"
																		class="generalLable"><b><u>Election Commission Details</u> :</b></label>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table width="69%" id="electionDetails">
																<tr>
																	<td style="width: 10%;"><label style="font-size: 12px;">Name</label></td>
																	<td><input class="focusH" id="voterName"
																		name="voterName" readonly="readonly" maxlength="20"
																		tabindex="74"></td>
																	<td><label style="font-size: 12px;">Father's Name</label></td>
																	<td><input class="focusH"
																			id="voterFatherName" name="voterFatherName"
																			readonly="readonly" tabindex="75"></td>
																	<td><label style="font-size: 12px;">District</label></td>
																	<td><input class="focusH"
																			id="voterCity" name="voterCity"
																			readonly="readonly" tabindex="76"></td>
																	<td><label style="font-size: 12px;">State</label></td>
																	<td><input class="focusH"
																			id="voterState" name="voterState"
																			readonly="readonly" tabindex="77"></td>																																
																</tr>																													
															</table>
														</td>
													</tr>
												</table>
											</form></td>
									</tr>
									<tr>
										<td align="left">
											<table cellpadding="0" cellspacing="0" width="92%" border="0">
												<tr>
													<td align="left">
														<ul>
															<li style="display: inline; float: left; padding-right: 10px;">
																<button type="button" id="slcsave" tabindex="5">Accept</button>
															</li>
														</ul></td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="photoComplianceBtn" value="Photo_Compliance" tabindex="7">Photo Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="overwritingBtn" tabindex="14" value="Overwriting">Overwriting</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="cafComBtn" tabindex="9" value="CAF_Compliance">CAF Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="mnpCombtn" tabindex="16" value="MNP_Compliance">MNP Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="caoCombtn" tabindex="11" value="CAO_Compliance">CAO Comp</button>
															</li>
														</ul>
													</td>
												</tr>
												<tr>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="retailerComBtn" tabindex="12" value="Retailer_Compliance">Retailer Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="distributorComBtn" tabindex="13" value="Distributor_Compliance">Distributor Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="documentComBtn" tabindex="8" value="Document_Compliance">Document Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="outstationCombtn" tabindex="15" value="Outstation_Compliance">Outstation Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="customerComBtn" tabindex="10" value="Customer_Compliance">Customer Comp</button>
															</li>
														</ul>
													</td>
													
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="databaseCombtn" tabindex="17" value="Database_Compliance">Database Comp</button>
															</li>
														</ul>
													</td>
												<!-- 	<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="otherbtn" tabindex="18" value="Other">Other</button>
															</li>
														</ul>
													</td> -->
<!-- 													<td align="left"> -->
<!-- 														<ul> -->
<!-- 															<li style="display: inline; float: left;"> -->
<!-- 																<button type="button" id="cancelBut" tabindex="6">Cancel</button> -->
<!-- 															</li> -->
<!-- 														</ul> -->
<!-- 													</td> -->
												</tr>
											</table></td>
									</tr>
									<tr>
										<td style="float: left; margin-left: 34px;">
											<div id="rejectOpt1" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt2" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt3" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt4" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt5" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt6" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt7" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt8" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt9" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt10" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt11" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
											<div id="rejectOpt12" style="width: 100%; height: auto;">
													<jsp:include page="reject-reason-page.jsp"></jsp:include>
											</div>
										</td>
									</tr>
									<tr>
										<td style="float: left; margin-left: 40px;"><button id="rejectDone" type="button">OK</button></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- div class="smbox-bottom" ends -->
					</div>
				</div>
			</div>
		</div>
		<!--wrapper ends -->
	</div>
	<div id="int-bottom">
		<span></span>
	</div>
	<!-- End: Content Bottom -->
	<c:url var="homeLoadUrl" value="/loginController/loginSuccess" />
	<form id="loadHome" action="${homeLoadUrl}"></form>

</body>
</html>
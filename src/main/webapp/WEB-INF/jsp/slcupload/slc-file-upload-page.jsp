<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:url var="url" value="/slcupload/uploadFile" /> 
	
<script type="text/javascript">	
$(document).ready(function(){
	$("#slcUploadBtn").button();
	$.get("<c:url value='/slcupload/getslcpage' />",function(data){
		 $('#resultSlcData').html(data);   
	});	 
});

function uploadSlcFormData(){
 
    var url = "<c:url value='/slcupload/uploadFile' />" ;
  	var oMyForm = new FormData();
  	oMyForm.append("file", files.files[0]);
		$.ajax({
	    	url: url,
	    	data: oMyForm,
	   	 	dataType: 'text',
	    	processData: false,
	    	contentType: false,
	    	type: 'POST',
	    	success: function(data){
	    		$("#up1").show();
	       	 $('#resultSlcData').html(data);   
	       	$('#files').val("");   
	    	}
	  	});	
}
</script>
			
<div class="content">
<div id="mainSlc" style="padding-top: 30px; width: 400px;">
	<table>
		<tbody>
			<tr>
				<td >
					<label style="display: block; width:89px; margin-bottom: 18px; font-size: 12px;">
					<b>Select SLC file:</b> </label></td>
				<td>
					<form enctype="multipart/form-data" method="post" action="<c:url value='/slcupload/uploadFile' />" style="width: 230px;" id="form1">
						<input type="file" name="files" id="files" style="font-size: 12px;">
					</form>
				</td>
				<td>
					<button id="slcUploadBtn" onclick="uploadSlcFormData()" value="Upload" class="ui-button-text" style="margin-bottom: 14px;">Upload</button>		
				</td>	
			</tr>
			<tr>
				<td colspan="2">
					<label style="display: none;color: red; font-size: 15px;" id="up1">File Upload Successfully. </br>Status of SLC file will be shown after 10 to 15 minutes.</label>
				</td>
			</tr>
		</tbody>
	</table>
</div>	
		
<div id="resultSlcData" style="padding-top: 0px;"></div>

</div>
		
		
	
	


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
$(document).ready(function(){
	$("#goSlcBtn").button();
	
	$('#month1').datepicker({dateFormat:"MM yy"}).datepicker("setDate",new Date());
	var month=$("#month1").val().trim();
	month = month.split(' ').join(' ') ;

	$.get("../slcdone/getgridpage/"+month,function(data){			 
		$("#resultSlcDoneGrid").html(data);			 
 	});
 		
	$('#month1').datepicker({
	     changeMonth: true,
	     changeYear: true,
	     dateFormat: 'MM yy',
	     onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	     },
	       
	     beforeShow: function() {
	       if ((selDate = $(this).val()).length > 0) 
	       {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
	           $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	       }
	    }
	  });

	 	$("#goSlcBtn").click(function(){	    		    		 	    
	     	$.get("../slcdone/getgridpage/"+month,function(data){			 
				$("#resultSlcDoneGrid").html(data);			 
		 	});	 	    		    		    	    			    		   		    		
	 	});	   
});


</script>

	<div>		
		<form id="slcDoneForm">
			<label>Month :</label>
			<input type="text" id="month1" name="month"/>
			<button type="button" id="goSlcBtn">Go</button>
		</form>
		<div id="resultSlcDoneGrid" style="padding-top: 15px; text-align: center;"> </div>
	</div>
		

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript">
$(document).ready(function(){
	var sum = 0;
	var remSum = 0;
	var verRej = 0;
	var scrutiny = 0;
	var scrutinyReject = 0;
	var rows = $("#tbl tr:gt(0)");
	rows.children("td:nth-child(3)").each(function() {
		if (!isNaN(parseInt($(this).html()))) {
			sum += parseInt($(this).html());
		}
	});
	rows.children("td:nth-child(4)").each(function() {
		if (!isNaN(parseInt($(this).html()))) {
			remSum += parseInt($(this).html());
		}
	});
	rows.children("td:nth-child(5)").each(function() {
		if (!isNaN(parseInt($(this).html()))) {
			verRej += parseInt($(this).html());
		}
	});
	rows.children("td:nth-child(6)").each(function() {
		if (!isNaN(parseInt($(this).html()))) {
			scrutiny += parseInt($(this).html());
		}
	});
	rows.children("td:nth-child(7)").each(function() {
		if (!isNaN(parseInt($(this).html()))) {
			scrutinyReject += parseInt($(this).html());
		}
	});
	if (sum != 0) {
		$("#sum").html("DE Complete <b>&nbsp;<u>" + sum + "</u></b>");
	}
	if (remSum != 0) {
		$("#remSum").html("	FLC Accept <b>&nbsp;<u>" + remSum + "</u></b>");
	}
	if (verRej != 0) {
		$("#verRej").html("	FLC Reject <b>&nbsp;<u>" + verRej + "</u></b>");
	}
	if (scrutiny != 0) {
		$("#scrutiny").html("Scrutiny Complete <b>&nbsp;<u>" + scrutiny + "</u></b>");
	}
	if (scrutinyReject != 0) {
		$("#scrutinyReject").html("Scrutiny Reject <b>&nbsp;<u>" + scrutinyReject + "</u></b>");
	}

});
</script>
<div id="mainDiv">
	<table width="90%" border="1" cellspacing='0' cellpadding='0' align="center" id="tbl" style="margin-left: 20px; font-size: 12px;">

			<tr align="center" valign="middle" class="tbl-head" style="background:#EEEEEE;">
				<th class="tbl-top" width="7%" style="font-size: 12px;">Sr No</th>
				<th class="tbl-top" width="13%" style="font-size: 12px;">User</th>
				<th class="tbl-top" class="hd" width="17%" id="sum" style="font-size: 12px;">DE Complete</th>
				<th class="tbl-top" class="hd" width="17%" id="remSum" style="font-size: 12px;">FLC Accept</th>
				<th class="tbl-top" class="hd" width="17%" id="verRej" style="font-size: 12px;">FLC Reject</th>
				<th class="tbl-top" class="hd" width="17%" id="scrutiny" style="font-size: 12px;">Scrutiny Complete</th>
				<th class="tbl-top" class="hd" width="18%" id="scrutinyReject" style="font-size: 12px;">Scrutiny Reject</th> 
			</tr>
		<c:if test="${not empty lists}">
			<c:forEach items="${lists}" var="list" varStatus="len">
				<c:choose>
					<c:when test="${len.count%2==0}">
						<tr align="center" valign="middle" class="row-alter" style="background-color: #EDEDED;">
					</c:when>
					<c:otherwise>
						<tr align="center" valign="middle" class="row">
					</c:otherwise>
				</c:choose>
					<td style="font-size: 12px;">${len.count}</td>
					<td style="font-size: 12px;">${list.username}</td>
					<td id="deComp" style="font-size: 12px;">${list.deCompleteCount}</td>
					<td style="font-size: 12px;">${list.flcAcceptCount}</td>
					<td style="font-size: 12px;">${list.flcRejectCount}</td>
					<td style="font-size: 12px;">${list.scrutinyCompleteCount}</td>
					<td style="font-size: 12px;">${list.scrutinyRejectCount}</td>					
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${empty lists}">
			<tr>
				<td colspan="8" align="center">
				  <label style="font-size:12px; color: red;"><b> No Record Found. </b></label>
				</td>
			</tr>
		</c:if>
				<tr>
					<td></td>
					<td></td>
					<td id="deComp"></td>
					<td id="remComp"></td>
				</tr>
	</table>
</div>
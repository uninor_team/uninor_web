<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div style="width: 100%; height: auto;">

	<form name="auditfrmRadio" id="auditfrmRadio">
		<table border="0">
			<c:forEach var="remarkVo" items="${remarkVoList}">
				<tr>
					<td align="right" valign="top"><input type="checkbox"
						value="${remarkVo.id}" name="auditremak" onclick="addRejectReasons(${remarkVo.id}, this)"
						id="${remarkVo.rejectReason}" />
					</td>
					<td>
						<label style="width: 100%; margin-left: 4px; font-size: 12px;">${remarkVo.rejectReason}</label>
					</td>
				</tr>
			</c:forEach>
<!-- 			<tr> -->
<!-- 				<td></td> -->
<!-- 				<td><button id="rejectDone" type="button">OK</button></td> -->
<!-- 			</tr> -->
		</table>
	</form>
</div>
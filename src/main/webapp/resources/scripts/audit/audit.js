//var auditremak = "NOT_SELECTED"; 

var states;
var cities;
var districts;
var cityresponse = 0;
var distresponse = 0;
var noOfPages = 0;
var mdnbeforeChange = null;
var activationdate = null;
var oldDataEntryVO = null;
var placeholder = "" ;

$(document).ready(function(){
	
	$("#dob1").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#visaValidTill1").mask("99/99/9999", {
		placeholder : "_"
	});

	$("#existingConnection1").mask("9999999999", {
		placeholder : "_"
	});
	$("#localReferenceContactNumber1").mask("9999999999", {
		placeholder : "_"
	});

	// / $("#simNumber").mask("9999999999999999999",{placeholder:"_"});
	$("#mobileNumber1").mask("9999999999", {
		placeholder : "_"
	});
	$("#localPincode1").mask("999999", {
		placeholder : "_"
	});
	// $("#permanentPincode").mask("999999",{placeholder:"_"});
	$("#localReferencePincode1").mask("999999", {
		placeholder : "_"
	});
	$("#poiIssueDate1").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#cafNumber1").mask("999999999999", {
		placeholder : "_"
	});
	$("#poaIssueDate1").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#activationDateTxt1").mask("99/99/9999", {
		placeholder : "_"
	});
	
	$("#auditRejectOpt1").hide();
	$("#auditRejectOpt2").hide();
	$("#auditRejectOpt3").hide();
	$("#auditRejectOpt4").hide();
	$("#auditRejectOpt5").hide();
	$("#auditRejectOpt6").hide();
	$("#auditRejectOpt7").hide();
	$("#auditRejectOpt8").hide();
	$("#auditRejectOpt9").hide();
	$("#auditRejectOpt10").hide();
	$("#auditRejectOpt11").hide();
	$("#auditRejectOpt12").hide();
	
	$("#speedocId").focus();
	
	$("#okRejectBtn").hide();
	$("#okRejectBtn").button();
//	$("#rejectBtn").click(function(){
//		$("#rejectOpt2").slideToggle();
//	});
	
	
//	$('#auditfrmRadio input:checkbox[name=auditremak]').click(function() {
//		auditremak="";
//	    var i = 1;
//	   $('#auditfrmRadio input:checkbox[name=auditremak]:checked').each(
//			   function() {
//			    	  if(i== 1){
//			    		  auditremak =  $(this).val()+"\n ";
//			    	  }else{
//			    		  auditremak = auditremak+","+ $(this).val()+"\n "; 
//			    	  }
//			    	  i++;
//			  	    }
//		);
//	  if(auditremak.length == 0) {
//		  auditremak="NOT_SELECTED";
//	  }
//	 
//	});
	
	$("#photoComplianceBtn1").click(function(){
		var photoCom = $("#photoComplianceBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt1").html(data);
			setSelected("#auditRejectOpt1");
			$("#auditRejectOpt1").show();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#documentComBtn1").click(function(){
		var docu = $("#documentComBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+docu,function(data){
			$("#auditRejectOpt2").html(data);
			setSelected("#auditRejectOpt2");
			$("#auditRejectOpt2").show();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#cafComBtn1").click(function(){
		var photoCom = $("#cafComBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt3").html(data);
			setSelected("#auditRejectOpt3");
			$("#auditRejectOpt3").show();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#customerComBtn1").click(function(){
		var photoCom = $("#customerComBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt4").html(data);
			setSelected("#auditRejectOpt4");
			$("#auditRejectOpt4").show();		
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#caoCombtn1").click(function(){
		var photoCom = $("#caoCombtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt5").html(data);
			setSelected("#auditRejectOpt5");
			$("#auditRejectOpt5").show();	
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#retailerComBtn1").click(function(){
		var photoCom = $("#retailerComBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt6").html(data);
			setSelected("#auditRejectOpt6");
			$("#auditRejectOpt6").show();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});	
	});
	
	$("#distributorComBtn1").click(function(){
		var photoCom = $("#distributorComBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt7").html(data);
			setSelected("#auditRejectOpt7");
			$("#auditRejectOpt7").show();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});	
	});
	
	$("#overwritingBtn1").click(function(){
		var photoCom = $("#overwritingBtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt8").html(data);
			setSelected("#auditRejectOpt8");
			$("#auditRejectOpt8").show();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#outstationCombtn1").click(function(){
		var photoCom = $("#outstationCombtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt9").html(data);
			setSelected("#auditRejectOpt9");
			$("#auditRejectOpt9").show();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#mnpCombtn1").click(function(){
		var photoCom = $("#mnpCombtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt10").html(data);
			setSelected("#auditRejectOpt10");
			$("#auditRejectOpt10").show();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#databaseCombtn1").click(function(){
		var photoCom = $("#databaseCombtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){		
			$("#auditRejectOpt11").html(data);
			setSelected("#auditRejectOpt11");
			$("#auditRejectOpt11").show();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#auditRejectOpt12").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#otherbtn1").click(function(){
		var photoCom = $("#otherbtn1").val();
		$.post("../audit/getrejectreasonaudit?header="+photoCom,function(data){
			$("#auditRejectOpt12").html(data);
			setSelected("#auditRejectOpt12");
			$("#auditRejectOpt12").show();
			$("#auditRejectOpt11").hide();
			$("#auditRejectOpt10").hide();
			$("#auditRejectOpt9").hide();
			$("#auditRejectOpt8").hide();
			$("#auditRejectOpt7").hide();
			$("#auditRejectOpt6").hide();
			$("#auditRejectOpt5").hide();
			$("#auditRejectOpt4").hide();
			$("#auditRejectOpt3").hide();
			$("#auditRejectOpt2").hide();
			$("#auditRejectOpt1").hide();
			$("#okRejectBtn").show();
		});
	});
	
	$("#auditsave").click(function(){
		var formObject = $("#auditForm").serializeObject();
		formObject.dob = saveDateFormate(formObject.dob);
		formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
		formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);	
		formObject.activationDate = saveDateFormate(formObject.activationDate);	
		formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);		
		formObject.createdDate = saveDateFormate(formObject.createdDate);	
			$.post("../audit/save",formObject,function(data) {
				var sr = data; 
				if(sr.successful){
					reset();
					rePipeNextCaf();				
				}else{
					reset();
					rePipeNextCaf();
				}
			}) ;
	});
	
	 $("#okRejectBtn").click(function(){		
			if( auditRejectReasons.length != 0){
				var formObject = $("#auditForm").serializeObject();
				formObject.dob = saveDateFormate(formObject.dob);
				formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
				formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);
				formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);
				formObject.activationDate = saveDateFormate(formObject.activationDate);
				formObject.createdDate = saveDateFormate(formObject.createdDate);
				 $.post("../audit/renamereject?auditremak="+auditRejectReasons, formObject , function(data){
				var response = data;
				if(response.successful){
					reset();
					$("#auditRejectOpt1").hide();
					$("#auditRejectOpt2").hide();
					$("#auditRejectOpt3").hide();
					$("#auditRejectOpt4").hide();
					$("#auditRejectOpt5").hide();
					$("#auditRejectOpt6").hide();
					$("#auditRejectOpt7").hide();
					$("#auditRejectOpt8").hide();
					$("#auditRejectOpt9").hide();
					$("#auditRejectOpt10").hide();
					$("#auditRejectOpt11").hide();
					$("#auditRejectOpt12").hide();
					auditRejectReasons.length=0;
					$("#okRejectBtn").hide();
					rePipeNextCaf();
				}else{
					jAlert("Rejection Failed.","Data Entry");
					reset();
					$("#auditRejectOpt1").hide();
					$("#auditRejectOpt2").hide();
					$("#auditRejectOpt3").hide();
					$("#auditRejectOpt4").hide();
					$("#auditRejectOpt5").hide();
					$("#auditRejectOpt6").hide();
					$("#auditRejectOpt7").hide();
					$("#auditRejectOpt8").hide();
					$("#auditRejectOpt9").hide();
					$("#auditRejectOpt10").hide();
					$("#auditRejectOpt11").hide();
					$("#auditRejectOpt12").hide();
					auditRejectReasons.length=0;
					$("#okRejectBtn").hide();
					rePipeNextCaf();
				}
			 });
		
			}else{
				$("#okRejectBtn").blur();
				jAlert("Reject reason mandatory. Please select whichever is applicable.","Data Entry",function(){
					$("#okRejectBtn").focus();
				});
			}
			
		 });
   rePipeNextCaf();
}) ;

	var auditRejectReasons=[];
	function addRejectReasons(value, elem){ 
		 if (elem.checked) {
			auditRejectReasons.push(value);
		 }else{
			 var reasons = [];
			 for(var i=0; i < auditRejectReasons.length; i++){
				 if(auditRejectReasons[i] != value){
					 reasons.push(auditRejectReasons[i]);
				 } 
			 }
			 auditRejectReasons = reasons;
		 }
	}	

	function setSelected(id){ 
		$(id + " input[type=checkbox]").each(function() {
			for(var i=0; i < auditRejectReasons.length; i++){
				if(auditRejectReasons[i] == this.value){
					this.checked = true;
				} 
			}
		});
	} 
	
function reset(){
	$("#auditForm :input").val("");
//	$("#rejectOpt2").hide();
	$("#imagediv1").html("");
	
	$("#auditRejectOpt1").html("");
	$("#auditRejectOpt2").html("");
	$("#auditRejectOpt3").html("");
	$("#auditRejectOpt4").html("");
	$("#auditRejectOpt5").html("");
	$("#auditRejectOpt6").html("");
	$("#auditRejectOpt7").html("");
	$("#auditRejectOpt8").html("");
	$("#auditRejectOpt9").html("");
	$("#auditRejectOpt10").html("");
	$("#auditRejectOpt11").html("");
	$("#auditRejectOpt12").html("");

	auditRejectReasons.length=0;
	 
	 $("#connectionOptingFor1").val('Personal');
		// $("#localReferenceCity").val('1');
		$("#cafNumber1").val("");
		$("#cafNumber1").focus();
		$("#cafImages1").html("");

		$("#monthlyExpenditure1").val(0);
	//	$("#localState").change();

		if (document.getElementById("foreignNational1").checked) {
			$("#foreignNational1").attr('checked', false);
			$("#outstation1").attr('checked', false);
			$("#copyCommonFieldsCheckbox1").attr('checked', false);
//			foreignNationalDisabled(true);
//			displayForeignNationalRow(false);
//			outstationDisabled(true);
//			displayOutStationRow(false);
			$('#copyCommonFieldsCheckboxTr').hide();
			$('#copyCommonFieldsCheckboxTr1').hide();
			$("#foreignNational1").val("N");
		}

		if (document.getElementById("outstation1").checked) {
			$('#copyCommonFieldsCheckboxTr').hide();
			$('#copyCommonFieldsCheckboxTr1').hide();
			$('#outstationTr2').hide();
			$("#outstation").attr('checked', false);
			$("#copyCommonFieldsCheckbox").attr('checked', false);
//			outstationDisabled(true);
//			displayOutStationRow(false);
			$("#outstation").val("N")
		}

		if (document.getElementById("mnp1").checked) {
			$("#mnp1").attr('checked', false);
			$("#mnp1").val("N");
			$("#monthlyExpenditure1").val(0);
			$("#existingConnection1").val("");
			$("#mnpCheckBox1").hide();
//			mnpHide();
		}

		$("#mnpCheckBox1").hide();
		$("#pageNo").html("");		
}

$("#auditImageRotateBtn").click(function(){	// added on 15-01-2015		
	rotateAuditCafImage();
});

function fillForm1(dataEntryVO) {
	// checkedCheckBoxes(true);
	jQuery.ajaxSetup({
		async : false
	});
	oldDataEntryVO = dataEntryVO;
	$("#speedocId2").val(dataEntryVO.speedocId);	
	$("#mobileNumber1").val(dataEntryVO.mobileNumber);
	if (dataEntryVO.cafNumber == 'null') {
		$("#cafNumber1").val("");
	} else {
		$("#cafNumber1").val(dataEntryVO.cafNumber);
	}
	$("#id1").val(dataEntryVO.id);
	$("#cafStatusId2").val(dataEntryVO.cafStatusId);
	$("#maritalStatus1").val(dataEntryVO.maritalStatus);	
	$("#images2").val(dataEntryVO.images);
	$("#cafFileLocation1").val(dataEntryVO.cafFileLocation);
	$("#cafType1").val(dataEntryVO.cafType);
	$("#onhold1").val(dataEntryVO.onhold);
	// for rotate Page
	$("#fileName3").val(dataEntryVO.cafFileLocation);
	$("#pageNo3").html("");
	noOfPages = 0;
	noOfPages = dataEntryVO.pages;
	for (var i = 0; i < noOfPages; i++) {
		$("#pageNo3").append(
				"<option  value='" + (i + 1) + "'>" + (i + 1) + "</option>");
	}
	$.trim($('#firstName1').val(dataEntryVO.firstName));
	$.trim($('#middleName1').val(dataEntryVO.middleName));
	$.trim($('#lastName1').val(dataEntryVO.lastName));
	$.trim($('#fatherFirstName1').val(dataEntryVO.fatherFirstName));
	$.trim($('#fatherMiddleName1').val(dataEntryVO.fatherMiddleName));
	$.trim($('#fatherLastName1').val(dataEntryVO.fatherLastName));
	$("#localAddress1").val(dataEntryVO.localAddress);
	$("#permanentAddress1").val(dataEntryVO.permanentAddress);
	$("#localAddressLandmark1").val(dataEntryVO.localAddressLandmark);

	if (!(dataEntryVO.localPincode == null)) {
		$("#localPincode1").val(dataEntryVO.localPincode);
	}

	$("#alternateMobileNumber1").val(dataEntryVO.alternateMobileNumber);

	$("#panGirNumber1").val(dataEntryVO.panGirNumber);
	$("#poiref1").val(dataEntryVO.poiref);
	$("#poaref1").val(dataEntryVO.poaref);

	$("#photograph1").val(dataEntryVO.photograph);
	$("#gender1").val(dataEntryVO.gender);
	$("#title1").val(dataEntryVO.title);

	$("#nationality1").val(dataEntryVO.nationality);

	$("#localState1").val(dataEntryVO.localState);

	$("#localCity1").val(dataEntryVO.localCity);

	$("#localDistrict1").val(dataEntryVO.localDistrict);

	$("#form6061").val(dataEntryVO.form6061);
	$("#poi1").val(dataEntryVO.poi);
	$("#poa1").val(dataEntryVO.poa);
	$("#poiIssuingAuthority1").val(dataEntryVO.poiIssuingAuthority);
	$("#poaIssuingAuthority1").val(dataEntryVO.poaIssuingAuthority);
	$("#category").val(dataEntryVO.category);

	// new
	$("#poiIssuePlace1").val(dataEntryVO.poiIssuePlace);

	$("#poaIssuePlace1").val(dataEntryVO.poaIssuePlace);

	$("#partyNumber1").val(dataEntryVO.callingPartyNumber);

	$("#permanentState1").val(dataEntryVO.permanentState);
	$("#caoName1").val(dataEntryVO.activationOfficerName);
	$("#caoCode1").val(dataEntryVO.activationOfficerDesignation);
	$("#statusOfSubscriber1").val(dataEntryVO.statusOfSubscriber);
	$("#subscriberProfession1").val(dataEntryVO.subscriberProfession);
	$("#imsiNumber1").val(dataEntryVO.imsiNumber);
	$("#vasApplied1").val(dataEntryVO.vasApplied);
	$("#category1").val(dataEntryVO.category);
	$("#emailAddress1").val(dataEntryVO.email);
	$("#uidNumber1").val(dataEntryVO.uidNumber);
	$("#tarrifPlanApplied1").val(dataEntryVO.tarrifPlanApplied);
	$("#existingNumber1").val(dataEntryVO.existingNumber);
	$("#upcCode1").val(dataEntryVO.upcCode);
	$("#prevServiceProviderName1").val(dataEntryVO.prevServiceProviderName);
	$("#cafTrackingStatus1").val(dataEntryVO.cafTrackingStatus);
	$("#wap1").val(dataEntryVO.rtrCode);
	$("#mms1").val(dataEntryVO.rtrName);
	if (dataEntryVO.dob != null) {		
		var date = dateformate(dataEntryVO.dob);
		$("#dob1").val(date);
	}else{
		$("#dob1").val("");
	}

	if (dataEntryVO.poiIssueDate != null) {
		var date = dateformate(dataEntryVO.poiIssueDate);
		$("#poiIssueDate1").val(date);
	}else{
		$("#poiIssueDate1").val("");
	}

	if (dataEntryVO.poaIssueDate != null) {
		var date = dateformate(dataEntryVO.poaIssueDate);
		$("#poaIssueDate1").val(date);
	}else{
		$("#poaIssueDate1").val("");
	}
	
	if (dataEntryVO.activationDate != null) {
		var date = dateformate(dataEntryVO.activationDate);
		$("#activationDateTxt1").val(date);
	}else{
		$("#activationDateTxt1").val("");
	}
	
	localState: {
		if (dataEntryVO.foreignNational != null) {
			if (dataEntryVO.foreignNational.toUpperCase() == 'Y') {

				$("#foreignNational1").attr('checked', true);

				showForeignNational();

				fillPassportVistDetailsFields(dataEntryVO);

				// fillCommonFieldsCheckbox();
				$("#copyCommonFieldsCheckbox1").attr('checked', true);
				$('#localReferenceName1').val(dataEntryVO.localReferenceName);
				$('#localReferenceAddress1').val(dataEntryVO.localAddress);
				$("#localReferenceCity1").val(dataEntryVO.localReferenceCity);
				$('#localReferenceContactNumber1').val(dataEntryVO.mobileNumber);
				$('#localReferencePincode1').val(dataEntryVO.localPincode);
			}
		}

		if (dataEntryVO.outstation != null) {
			if (dataEntryVO.outstation.toUpperCase() == "Y") {

				$("#outstation1").attr('checked', true);
				showOutstation();
				// fillCommonFieldsCheckbox();
				$("#copyCommonFieldsCheckbox1").attr('checked', true);
				$('#localReferenceName1').val(dataEntryVO.localReferenceName);
				$('#localReferenceAddress1').val(dataEntryVO.localAddress);
				$("#localReferenceCity1").val(dataEntryVO.localReferenceCity);
				$('#localReferenceContactNumber1').val(dataEntryVO.mobileNumber);
				$('#localReferencePincode1').val(dataEntryVO.localPincode);
			}
		}

		if (dataEntryVO.outstation != null
				&& dataEntryVO.foreignNational != null) {
			if (dataEntryVO.outstation.toUpperCase() != "Y"
					&& dataEntryVO.foreignNational.toUpperCase() != "Y") {
				$('#copyCommonFieldsCheckboxTr').hide();
				$('#copyCommonFieldsCheckboxTr1').hide();
			}
		}

		if (dataEntryVO.connectionType != 'null') {
			$("#connectionType1").val(dataEntryVO.connectionType);
		}
		
		
		$("#rejectSpan").val(dataEntryVO.rejectReason);
		$("#usernameSpan").val(dataEntryVO.username);
//		$("#createdDateSpan").val(dataEntryVO.createdDate);
		if (dataEntryVO.createdDate != null) {		
			var date = dateformate(dataEntryVO.createdDate);
			$("#createdDateSpan").val(date);
		}else{
			$("#createdDateSpan").val("");
		}
		jQuery.ajaxSetup({
			async : true
		});
	}
	
	// Cao Image 
	$.post("../audit/getcaoimage", $.param({caoCode:dataEntryVO.caoCode}), function(data){
		var response = data;
		if(response.successful)
		{
			var caoVo =  response.result;
			var htmlImage = "";
			var timestamp = new Date().getTime();
			htmlImage += "<img  onmouseover=\"bigImg(this)\" onmouseout=\"normalImg(this)\" id=\"ImageDiv1\" alt=\"\" src=\"../audit/getcaoimage?fileName="+ caoVo.imageName+ "&timestamp="+ timestamp+ "\" style='width:100%'  > <br>";
			htmlImage += "</div>";
						
			$("#caoName1").val(caoVo.caoName);
			$("#caoCode1").val(caoVo.caoCode);
			$("#imagediv1").html(htmlImage);
		}
	 });
	
	// election commission 26-05-2015
	$("#voterId1").val(dataEntryVO.voterId);
	$("#voterName1").val(dataEntryVO.voterName);
	$("#pollingStation1").val(dataEntryVO.pollingStation);
}

function dateformate(timestamp){
	if(timestamp != ""){
		var strDate = timestamp.split(" ");
		var date = strDate[0].split("-");
		return date[2] + "-" + date[1] + "-"+ date[0];
	}
}

function saveDateFormate(date){
	if(date != null && date != ""){ 
		var d = date.split("-") ;
		var formateDate = d[2]+"-"+d[1]+"-"+d[0]+" "+"00:"+"00:"+"00";
		return formateDate ;
	}
}

function fillPassportVistDetailsFields(dataEntryVO) {
	$("#passportNumber1").val(dataEntryVO.passportNumber);

	$("#typeOfVisa1").val(dataEntryVO.typeOfVisa);

	if (dataEntryVO.visaValidTill != null) {
		var visaValidTill = dataEntryVO.visaValidTill.split('-');
		$("#visaValidTill1").val(visaValidTill[2] + "/" + visaValidTill[1] + "/"+ visaValidTill[0]);
	}
	$("#visaNumber1").val(dataEntryVO.visaNumber);
}



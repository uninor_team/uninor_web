var remark = "NOT_SELECTED"; 

$(document).ready(function(){
	$("#rename").button();
	$("#Reject").button();
	$("#cancel").button();
	$("#rejectClose").button();
	$("#flcImageRotateBtn").button();
	$("#skip").button();
	
	$("#rejectOpt").hide();
	$("#speedocId").focus();

	$("#Reject").click(function(){
		$("#rejectOpt").slideToggle();
	});
	
		if ($.browser.mozilla) {
			$("#speedocId").keydown(checkForEnter);
		} else {
			$("#speedocId").keydown(checkForEnter);
		}
	
		function checkForEnter(event) {
			if (event.keyCode == 13) {				
				submitFLCForm();								
			}
		}
		
	$("#speedocId").keydown(function(event) {		
	    // Allow only backspace and delete
		if ((event.keyCode < 48 || event.keyCode > 57 || event.shiftKey ) ) {
	        // let it happen, don't do anything
	    	if(!(event.keyCode == 9 || event.keyCode == 8 || (event.keyCode >= 96 && event.keyCode <= 105) 
	    			|| event.keyCode ==37 || event.keyCode == 39 || event.keyCode == 36 || 
	    			event.keyCode == 35 || event.keyCode == 46) ){
	    		if(!(event.keyCode == 86 &&  event.keyCode==17)){
	    			event.preventDefault(); 
	    		}		   
	    	}
		}		
	});
	
	$("#rename").click(function(){
		submitFLCForm();
	});
	
	function submitFLCForm(){	
		if($("#speedocId").val() == ""){
			jAlert("Please enter Speedoc Id");
		}else if($("#speedocId").val().length < 9){
			jAlert("Please enter 9 digit Speedoc Id");
		}else{
			$.post("../flc/save", $("#renameform").serialize(),function(data) {
				var sr = data; 
				if(sr.successful){
//					jAlert("Save Successfully.","Data Entry");
					$("#popup_content").css("background","white");
					reset();
//					alert("load next caf");
					rePipeNextCaf();						
				}else{
					jAlert("Save Failed.","FLC");
//					reset();
//					rePipeNextCaf();
				}
			}) ;
		}
	}
	
	$('#frmRadio input:checkbox[name=remark]').click(function() {
		    remark="";
		    var i = 1;
		   $('#frmRadio input:checkbox[name=remark]:checked').each(
				   function() {
				    	  if(i== 1){
				    	  remark =  $(this).val()+"\n ";
				    	  }else{
				    		  remark = remark+", "+ $(this).val()+"\n "; 
				    	  }
				    	  i++;
				  	    }
			);
		  if(remark.length == 0)
			  {
			  remark="NOT_SELECTED";
			  }	 
		});
	
//	 $("#cancel").click(function(){
//		 $("#speedocId").focus();
//			$.post("/console/flc/cancel", $("#renameform").serialize(),function(data) {
//				var sr = data; 
//				if(sr.successful){
//					reset();
//					rePipeNextCaf();
//					
//				}
//			}) ;
//	 }) ;
	
	 
 $("#rejectClose").click(function(){
		if( remark != "NOT_SELECTED"){
			var formObject = $("#renameform").serializeObject();
			$.post("../flc/renamereject?remark="+remark, formObject , function(data){
			var response = data;
			if(response.successful){
				$("#speedocId").attr("readonly", false);
				reset();
				$("#rejectOpt").slideToggle();
				rePipeNextCaf();
			}else{
				jAlert("Rejection Failed.","Data Entry");
				reset();
				$("#rejectOpt").slideToggle();
				$("#speedocId").attr("readonly", false);
//				rePipeNextCaf();
			}
		 });
		}else{
			$("#rejectOk").blur();
			jAlert("Reject reason mandatory. Please select whichever is applicable.","Data Entry",function(){
				$("#rejectOk").focus();
			});
		}
	});
   
  $("#skip").click(function(){
	  		var formObject = $("#renameform").serializeObject();
			$.post("../flc/skip", formObject , function(data){
			var response = data;
			if(response.successful){
				$("#speedocId").attr("readonly", false);
				reset();
				rePipeNextCaf();
			}else{
				jAlert("Skip Failed. Onhold count greater than 5.","Data Entry");
				reset();
				$("#speedocId").attr("readonly", false);
				rePipeNextCaf();
			}
		 });
	  }); 
	 rePipeNextCaf();
}) ;

function reset(){
	$("#speedocId").val("");
	$("#cafId").val("");
	$("#userId").val("");
	$("#cafStatusId").val("");
	$("#flcPoolId").val("");
	$("#currentStatus").val("");
	$("#fileLocation").val("");
	
	for (var index=0; index < document.frmRadio.remark.length; index++) {
		if (document.frmRadio.remark[index].checked){
			document.frmRadio.remark[index].checked=false;			
		}
	}
	 remark="NOT_SELECTED";
}

$("#flcImageRotateBtn").click(function(){	// added on 15-01-2015		
	rotateFlcCafImage();
});

function fillForm(cafData){
	$("#cafId").val(cafData.cafId);
	$("#userId").val(cafData.userId);
	$("#cafStatusId").val(cafData.cafStatusId);
	$("#flcPoolId").val(cafData.flcPoolId);
	$("#currentStatus").val(cafData.currentStatus);
	$("#fileLocation").val(cafData.fileLocation);
	$("#images").val(cafData.images);
	
	// for rotate Page
	$("#fileName1").val(cafData.fileLocation);
	$("#pageNo1").html("");
	noOfPages = 0;
	noOfPages = cafData.pages;
//	alert("pages : "+cafData.pages);
	for (var i = 0; i < noOfPages; i++) {
		$("#pageNo1").append(
				"<option  value='" + (i + 1) + "'>" + (i + 1) + "</option>");	

	}	
}
